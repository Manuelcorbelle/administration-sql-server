

USE Tempdb
go


CREATE TABLE Sales  
    (  
    OrderID int,  
    SalesRep sysname,  
    Product varchar(10),  
    Qty int  
    );  
GO
INSERT Sales VALUES   
(1, 'Sales1', 'Valve', 5),   
(2, 'Sales1', 'Wheel', 2),   
(3, 'Sales1', 'Valve', 4),  
(4, 'Sales2', 'Bracket', 2),   
(5, 'Sales2', 'Wheel', 5),   
(6, 'Sales2', 'Seat', 5);  
-- View the 6 rows in the table  
SELECT * FROM Sales;
Go

CREATE USER Manager WITHOUT LOGIN;  
CREATE USER Sales1 WITHOUT LOGIN;  
CREATE USER Sales2 WITHOUT LOGIN;  


GRANT SELECT ON Sales TO Manager;  
GRANT SELECT ON Sales TO Sales1;  
GRANT SELECT ON Sales TO Sales2;
GO

CREATE SCHEMA Security;  
GO  
  
CREATE FUNCTION Security.fn_securitypredicate(@SalesRep AS sysname)  
    RETURNS TABLE  
WITH SCHEMABINDING  
AS  
    RETURN SELECT 1 AS fn_securitypredicate_result   
WHERE @SalesRep = USER_NAME() OR USER_NAME() = 'Manager';  
Go

CREATE SECURITY POLICY SalesFilter  
ADD FILTER PREDICATE Security.fn_securitypredicate(SalesRep)   
ON dbo.Sales  
WITH (STATE = ON); 
GO

EXECUTE AS USER = 'Sales1';  
SELECT * FROM Sales;   
REVERT;  
  
EXECUTE AS USER = 'Sales2';  
SELECT * FROM Sales;   
REVERT;  
  
EXECUTE AS USER = 'Manager';  
SELECT * FROM Sales;   
REVERT;  

ALTER SECURITY POLICY SalesFilter  
WITH (STATE = OFF);
GO






--------------------------


-- -- http://www.sqlshack.com/filter-block-data-access-using-sql-server-2016-row-level-security/

-- How to filter and block the data access using SQL Server 2016 Row-Level Security

/*

SQL Server 2016 came with many new features and enhancements for existing ones, that concentrate on the aspect of SQL Server security. One of the new security features introduced in SQL Server 2016 is Row-Level Security. This feature allows us to control access deeply into the rows level in the database table, based on the user executing the query. This is done within the database layer, in complete transparency to the application process, without the need to manage it with complex coding at the application layer.

Row-Level Security (RLS) provides us with a way to restrict users to allow them to work only with the data they have access to. For example, each courier in a shipping company can access only the data related to the shipments he is requested to deliver, without being able to access other courier�s data. Each time data access is performed from any tier, data access will be restricted by the SQL Server Engine, reducing the security system surface area.

There are two types of security predicates that are supported in Row-Level Security; the Filter predicate that filters row silently for read operations. The Silent filter predicate means that the application will not be made aware that the data is filtered, null values will be returned if all rows are filtered, without raising an error message. The Block predicate prevents any write operation that violates any defined predicate with an error message returned as a result of the block. This policy can be turned ON and OFF with four main blocking types; AFTER INSERT and AFTER UPDATE blocking will prevent the users from updating the rows to values that will violate the predicate. BEFORE UPDATE will prevent the users from updating the rows that are violating the predicate currently. BEFORE DELETE will prevent the users from deleting the rows. Trying to add a predicate on a table that already has a predicate defined will result with error.

Data access restriction using Row-Level Security is accomplished by defining a Security predicate as an inline-table-valued function that will restrict the rows based on filtering logic, which is invoked and enforced by a Security Policy created using the CREATE SECURITY POLICY T-SQL statement and working as predicates container. SQL Server allows you to define multiple active security policies but without overlapping predicates. Altering a table with a schema bound security policy defined on it will fail with error.

*/
-- 
DROP DATABASE If Exists RLS
GO
CREATE DATABASE RLS
GO
USE RLS
GO

CREATE TABLE [dbo].[Courier_Shipments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Courier_Name] [varchar](10) NULL,
	[NumOfShipmentsInPackage] [int] NULL,
	[Package_Date] [datetime] NULL,
	[Package_Cost] [int] NULL,
	[Package_City] [varchar](10) NULL,
 CONSTRAINT [PK_Courier_Shipments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)) ON [PRIMARY]
 
GO

INSERT INTO [dbo].[Courier_Shipments]
           ([Courier_Name]
           ,[NumOfShipmentsInPackage]
           ,[Package_Date]
           ,[Package_Cost]
           ,[Package_City])
     VALUES
           ('John',5,'2016-09-22 01:35:00:000',580,'LON'),
		   ('Ghezil',3,'2016-09-22 03:45:00:000',470,'LAX'),
		   ('Mark',2,'2016-09-22 04:32:00:000',118,'JFK'),
		   ('Ghezil',5,'2016-09-22 02:38:00:000',358,'LAX'),
		   ('Mark',4,'2016-09-22 06:12:00:000',774,'JFK'),
		   ('John',7,'2016-09-22 08:07:00:000',941,'LON')
GO


--Also we will create a three users, one for each courier who will retrieve his shipments data:


CREATE USER John WITHOUT LOGIN;  
CREATE USER Ghezil WITHOUT LOGIN;  
CREATE USER Mark WITHOUT LOGIN;   
GO
--  Without Scema

GRANT SELECT ON [Courier_Shipments] TO John;  
GRANT SELECT ON [Courier_Shipments] TO Ghezil;  
GRANT SELECT ON [Courier_Shipments] TO Mark;
GO

-- No ejecutar
CREATE SCHEMA RLS;  
GO

--ALTER SCHEMA RLS TRANSFER dbo.[Courier_Shipments]  
--GO
--REVERT
--GRANT SELECT ON SCHEMA::RLS TO John,Ghezil,Mark;
--GO

-- First we will create the Filter Predicate function that depends on the logged in user name to filter the users and check their access on the data as follows:

CREATE FUNCTION RLS.fn_SecureCourierData(@CourierName AS sysname)  
    RETURNS TABLE  
WITH SCHEMABINDING  
AS  
    RETURN SELECT 1 AS 'SecureCourierShipments'   
WHERE @CourierName = USER_NAME(); 
GO

-- And create the Security Policy on the Courier_Shipments table using the previously created predicate function, then turn it on:

CREATE SECURITY POLICY CourierShipments  
ADD FILTER PREDICATE RLS.fn_SecureCourierData(Courier_Name)   
ON RLS.Courier_Shipments  
WITH (STATE = ON);  
GO

-- Now, Row-Level Security is fully configured and ready to start filtering any new data access on the Courier_Shipments table. Expanding the Security node of the SQLShackDemo database, the newly created Security Policy can be found as in the following image using the SQL Server Management Studio:

-- If we try to run the below query using John user:


EXECUTE AS USER = 'John'
GO  
SELECT * FROM RLS.Courier_Shipments; 
GO  

-- Only see
/*
ID	Courier_Name	NumOfShipmentsInPackage	Package_Date	Package_Cost	Package_City
1	John	5	2016-09-22 01:35:00.000	580	LON
6	John	7	2016-09-22 08:07:00.000	941	LON
*/

REVERT;
GO

EXECUTE AS USER = 'Ghezil'
GO  
SELECT * FROM RLS.Courier_Shipments; 
GO  

/*

ID	Courier_Name	NumOfShipmentsInPackage	Package_Date	Package_Cost	Package_City
2	Ghezil	3	2016-09-22 03:45:00.000	470	LAX
4	Ghezil	5	2016-09-22 02:38:00.000	358	LAX

*/

REVERT;
GO

EXECUTE AS USER = 'Mark'
GO  
SELECT * FROM RLS.Courier_Shipments; 
GO  

/*
ID	Courier_Name	NumOfShipmentsInPackage	Package_Date	Package_Cost	Package_City
3	Mark	2	2016-09-22 04:32:00.000	118	JFK
5	Mark	4	2016-09-22 06:12:00.000	774	JFK
*/

/*
It is clear from the previous results that the Row-Level Security feature can be used to filter the data that each user can see depending on the filtering criteria defined in the predicate function.

If you manage to stop using the Row-Level Security feature that we configured, you need to disable the Security Policy using the ALTER SECURITY POLICY statement below:
*/
REVERT
GO
Alter Security Policy CourierShipments  with (State = off)
GO

Drop Security Policy CourierShipments
GO
Drop Function RLS.fn_SecureCourierData
GO

-- Now the Row-Level Security is removed completely from the Courier_Shipments table.

-- there is another type of predicates that can be used in the Row-level Security feature which is the Block Predicate.

--  Let�s have a second demo to know how the block predicate can be configured and work.

-- Again, we will create a predicate function that will filter the data access depends on the users connecting to that table as follows:


CREATE FUNCTION RLS.fn_SecureCourierData (@Courier_Name sysname)
	RETURNS TABLE
	WITH SCHEMABINDING
AS
	RETURN SELECT 1 AS fn_SecureCourierData_result
	WHERE @Courier_Name= user_name()
GO

-- Also we will create the security policy, but this time it will contain an AFTER INSERT block predicate condition in addition to the filter predicate

CREATE SECURITY POLICY RLS.fn_Courier_Shipments
	ADD FILTER PREDICATE RLS.fn_SecureCourierData(Courier_Name)  ON  RLS.Courier_Shipments,
	ADD BLOCK PREDICATE RLS.fn_SecureCourierData(Courier_Name)  ON  RLS.Courier_Shipments AFTER INSERT 
GO

-- And enable the Security policy as follows:


Alter Security Policy RLS.fn_Courier_Shipments  with (State = ON)
GO

-- As the blocking will be on the INSERT operation, we will grant Mark user access to do so:


GRANT SELECT, INSERT, UPDATE, DELETE ON RLS.Courier_Shipments TO Mark;
GO

--If Mark tries to apply the below SELECT statement with his user:


EXECUTE AS USER = 'Mark'; 
GO 
SELECT * FROM RLS.Courier_Shipments; 
GO  

/*
ID	Courier_Name	NumOfShipmentsInPackage	Package_Date	Package_Cost	Package_City
3	Mark	2	2016-09-22 04:32:00.000	118	JFK
5	Mark	4	2016-09-22 06:12:00.000	774	JFK
*/

-- Mark sees the outcome

REVERT
GO

-- But if he tries to insert a new record with his user, but with the courier name different from his name, let�s say John:

EXECUTE AS USER = 'Mark';
GO  
INSERT INTO [RLS].[Courier_Shipments]
           ([Courier_Name]
           ,[NumOfShipmentsInPackage]
           ,[Package_Date]
           ,[Package_Cost]
           ,[Package_City])
     VALUES
           ('John',5,'2016-09-22 01:35:00:000',580,'LON')
GO

--Msg 33504, Level 16, State 1, Line 217
--The attempted operation failed because the target object 'RLS.RLS.Courier_Shipments' has a block predicate that conflicts with this operation. If the operation is performed on a view, the block predicate might be enforced on the underlying table. Modify the operation to target only the rows that are allowed by the block predicate.
--The statement has been terminated.



SELECT * FROM RLS.Courier_Shipments; 
GO 
-- Works


-- NO ejecutar
DELETE RLS.Courier_Shipments; 
GO
-- (2 row(s) affected)

SELECT * FROM RLS.Courier_Shipments; 
GO 

-- (0 row(s) affected)

--Again, if the previous insert statement is modified by replacing John name with Mark name and execute it with Mark�s user:

EXECUTE AS USER = 'Mark'; 
GO 
INSERT INTO [RLS].[Courier_Shipments]
           ([Courier_Name]
           ,[NumOfShipmentsInPackage]
           ,[Package_Date]
           ,[Package_Cost]
           ,[Package_City])
     VALUES
           ('Mark',5,'2016-09-22 01:35:00:000',580,'LON')
GO


-- (1 row(s) affected)


-- Which is clear from running the SELECT statement with his user, showing the below result:

-- As you can conclude from the previous result, the Row-level Security feature can be also used to block the user from applying specific operations on the rows that he has no access on it.

-- New system objects have also been introduced in SQL Server 2016 to query the Row-Level Security feature�s information. The sys.security_policies can be used to retrieve all information about the defined security policy on your database as below:


SELECT name,type_desc ,create_date ,modify_date,is_enabled,is_schema_bound  FROM sys.security_policies
GO

SELECT * FROM sys.security_predicates
GO

/*

A common question that you may ask or may be asked, is if there any performance impact or side effect when using the Row-Level Security feature? The suitable answer here is that it depends. Yes, it depends on the complexity of the predicate logic you define, as it will be checked for each data access in your table. If you define a simple direct predicate filter, you will not notice any overhead or performance degradation in your database.

*/



DROP DATABASE RLS
GO
