
-- Transparent Data Encryption 


-- *** Beginning of setup code ***
-- *******************************

-- Make a copy of AdventureWorks2014 database
-- Change the path to the appropriate backup directory
BACKUP DATABASE AdventureWorks2014
	TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\Backup\AdventureWorks2014.bak'
	WITH NOFORMAT, INIT, NAME = 'AdventureWorks2014 Full Database Backup',
	SKIP, NOREWIND, NOUNLOAD, STATS = 10;
GO

RESTORE DATABASE AdventureWorks2014Copy
	FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\Backup\AdventureWorks2014.bak'
	WITH 
		FILE = 1, NOUNLOAD, REPLACE, STATS = 10,
		MOVE 'AdventureWorks2014_Data' TO 'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\DATA\AdventureWorks2014Copy.mdf',
		MOVE 'AdventureWorks2014_Log' TO 'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\DATA\AdventureWorks2014Copy.ldf';
GO
-- Look at Object Explorer AdventureWorks2014Copy
-- *** End of setup code ***
-- *************************

-- *** Configure AdventureWorks2014Copy
-- ************************************

-- Four steps to use TDE:
--		Create a master key in the master database
--		Create/use a certificate protected by the master key
--		Create database master key and protect using certificate
--		Enable TDE for the database

-- Create a certificate in master to use with TDE
USE master;
GO

-- TDE hooks into encryption key hierarchy in SQL Server
CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'abcd1234.';
GO
--Msg 15578, Level 16, State 1, Line 41
--There is already a master key in the database. Please drop it before performing this statement.
DROP MASTER KEY;
GO

--Msg 15580, Level 16, State 1, Line 45
--Cannot drop master key because certificate 'Pubs_DBBackupEncryptCert' is encrypted by it.

DROP CERTIFICATE Pubs_DBBackupEncryptCert
GO

CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'abcd1234.';
GO

-----------------------------
--Msg 15578, Level 16, State 1, Line 54
--There is already a master key in the database. Please drop it before performing this statement.

ALTER DATABASE AdventureWorks2014;
SET ENCRYPTION OFF;
GO
/* Wait for decryption operation to complete, look for a 
value of  1 in the query below. */
SELECT encryption_state
FROM sys.dm_database_encryption_keys;
GO
USE AdventureWorks2014;
GO
DROP DATABASE ENCRYPTION KEY;
GO
---------------------------

-- Create the certificate used to protect the database encryption key
CREATE CERTIFICATE AdventureWorks2014TDECert 
WITH SUBJECT = 'Certificate to implement TDE on AdventureWorks2014Copy';
GO

-- Backup the certificate
-- Either create the C:\Data folder or change it in the code below
BACKUP CERTIFICATE AdventureWorks2014TDECert TO FILE = 'C:\BD\AdventureWorks2014TDECert'
    WITH PRIVATE KEY ( FILE = 'C:\BD\AdventureWorks2014TDECertPrivateKey' , 
    ENCRYPTION BY PASSWORD = 'abcd1234.' );
GO
-- Must backup private key as well

-- List the encrypted databases on this server, and the encryption status
-- Should be empty on a pristine SQL Server instance
SELECT DB_NAME(database_id) AS DatabaseName,
	key_algorithm AS [Algorithm],
	key_length AS KeyLength,
	CASE encryption_state
		WHEN 0 THEN 'No database encryption key present, no encryption'
		WHEN 1 THEN 'Unencrypted'
		WHEN 2 THEN 'Encryption in progress'
		WHEN 3 THEN 'Encrypted'
		WHEN 4 THEN 'Key change in progress'
		WHEN 5 THEN 'Decryption in progress'
	END AS EncryptionStateDesc,
	percent_complete AS PercentComplete
FROM sys.dm_database_encryption_keys
GO
SELECT * FROM sys.dm_database_encryption_keys
GO
USE AdventureWorks2014Copy;
GO

-- Create the database encryption key for TDE. Analogous to database master key for data encryption.
CREATE DATABASE ENCRYPTION KEY 
	WITH ALGORITHM = TRIPLE_DES_3KEY
	ENCRYPTION BY SERVER CERTIFICATE AdventureWorks2014TDECert;
GO
-- Get a warning about backing up the key, if you haven't already
-- ...take the advice and back it up!

-- Now need to turn TDE on. 
ALTER DATABASE AdventureWorks2014Copy SET ENCRYPTION ON;
GO
-- Encryption/decryption operations scheduled on background threads
-- Will fail if any database files are marked read only
-- Zeros out remaining space in transaction log to force next virtual transaction log. Prevents
-- clear text from being in the log after TDE enabled.

-- Immediately run following to get progress info about encryption.
-- Repeat until completed.
SELECT DB_NAME(database_id) AS DatabaseName,
	key_algorithm AS [Algorithm],
	key_length AS KeyLength,
	CASE encryption_state
		WHEN 0 THEN 'No database encryption key present, no encryption'
		WHEN 1 THEN 'Unencrypted'
		WHEN 2 THEN 'Encryption in progress'
		WHEN 3 THEN 'Encrypted'
		WHEN 4 THEN 'Key change in progress'
		WHEN 5 THEN 'Decryption in progress'
	END AS EncryptionStateDesc,
	percent_complete AS PercentComplete
FROM sys.dm_database_encryption_keys;
GO	

--DatabaseName	Algorithm	KeyLength	EncryptionStateDesc	PercentComplete
--tempdb			AES				256		Encrypted				0
--AdventureWorks2014Copy	TRIPLE_DES_3KEY	192	Encryption in progress	100
-- tempdb is always encrypted with AES 256.

-- From an application standpoint, nothing seems different
SELECT TOP 500 * FROM Production.Product;
-- Anyone who had permission to see data before TDE, still can
-- But data is encrypted within the file

-- If want to try again, turn encryption off
ALTER DATABASE AdventureWorks2014Copy
	SET ENCRYPTION OFF;
GO

-- *** Test TDE
-- ************
-- Backup the database, drop it, accidentally drop the server certificate
-- to simulate using the database on another instance, then restore it
-- Change paths as necessary!
BACKUP DATABASE AdventureWorks2014Copy
	TO DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\Backup\AdventureWorks2014Copy.bak'
	WITH NOFORMAT, INIT, NAME = N'AdventureWorks2014Copy Full Database Backup',
	SKIP, NOREWIND, NOUNLOAD, STATS = 10;
GO

USE master
GO
DROP DATABASE AdventureWorks2014Copy;
GO

-- Oops! We lost the certificate and don't have a copy!
-- Or, going to restore the database to another server instance
DROP CERTIFICATE AdventureWorks2014TDECert; 
GO

-- Certificate used to encrypt must be available to restore or attach database
RESTORE DATABASE AdventureWorks2014Copy
	FROM DISK = 'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\Backup\AdventureWorks2014Copy.bak'
	WITH 
		FILE = 1, NOUNLOAD, REPLACE, STATS = 10;
GO

--Msg 33111, Level 16, State 3, Line 179
--Cannot find server certificate with thumbprint '0x92B92CD774B5133A3F3EFA8BD89C48C10DE9C99E'.
--Msg 3013, Level 16, State 1, Line 179
--RESTORE DATABASE is terminating abnormally.
-- Error that can't find server certificate

-- Recover from the problem
-- Restore the certificate
CREATE CERTIFICATE AdventureWorks2014TDECert
	FROM FILE = 'C:\BD\AdventureWorks2014TDECert'
    WITH PRIVATE KEY ( FILE = 'C:\BD\AdventureWorks2014TDECertPrivateKey', 
    DECRYPTION BY PASSWORD = 'RISiS9Ul%CByEk6');
GO

--Msg 15465, Level 16, State 6, Line 193
--The private key password is invalid.	

CREATE CERTIFICATE AdventureWorks2014TDECert
	FROM FILE = 'C:\BD\AdventureWorks2014TDECert'
    WITH PRIVATE KEY ( FILE = 'C:\BD\AdventureWorks2014TDECertPrivateKey', 
    DECRYPTION BY PASSWORD = 'abcd1234.');
GO
-- Now try to restore the database
RESTORE DATABASE AdventureWorks2014Copy
	FROM DISK = N'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\Backup\AdventureWorks2014Copy.bak'
	WITH 
		FILE = 1, NOUNLOAD, REPLACE, STATS = 10;
GO

-- *** Clean up ***
-- ****************
USE master;
GO
DROP DATABASE AdventureWorks2014Copy;
GO
-- Can't turn off TDE in tempdb once it is on
DROP CERTIFICATE AdventureWorks2014TDECert;
GO
DROP MASTER KEY;
GO