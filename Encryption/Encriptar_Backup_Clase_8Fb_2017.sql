

-- Create a Database Master Key for the master database on the instance
USE master
GO
----------------------------------------------------------------------------
-- *This Script will give you result output for which database TDE is enabled.
---------------------------------------------------------------------------------------------------------
USE master;
GO

SELECT
    db.name,
    db.is_encrypted,
    dm.encryption_state,
    dm.percent_complete,
    dm.key_algorithm,
    dm.key_length
FROM
    sys.databases db
    LEFT OUTER JOIN sys.dm_database_encryption_keys dm
        ON db.database_id = dm.database_id;

GO
--
-- Sometimes
-- Msg 15578, Level 16, State 1, Line 6
-- There is already a master key in the database. Please drop it before performing this statement.
-- http://kushagrarakesh.blogspot.com.es/2015/09/there-is-already-master-key-in-database.html

ALTER DATABASE database_name SET ENCRYPTION OFF --> user database.
DROP DATABASE ENCRYPTION KEY --> user database.
DROP CERTIFICATE certificate_name --> master database 
DROP MASTER KEY --> master database.
-----------------------------------------


-- Backup Encrypted


CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'Abcd1234.'
Go
-----
--GRANT VIEW DEFINITION ON CERTIFICATE::DBBackupEncryptCert  TO [WIN-776Q3S6FO22\cmm]
-- GRANT VIEW DEFINITION ON SYMMETRIC KEY::DBBackupEncryptKey  TO [WIN-776Q3S6FO22\cmm]


-- Create an encryption certificate
CREATE CERTIFICATE DBBackupEncryptCert
WITH SUBJECT = 'Backup Encryption Certificate'
GO
-- Backup Master key & encryption certificate
BACKUP CERTIFICATE DBBackupEncryptCert
TO FILE = 'C:\DataBases\BackupDBBackupEncryptCert.cert'
WITH PRIVATE KEY
(
FILE = 'C:\DataBasesCert\BackupDBBackupEncryptCert.cert',
ENCRYPTION BY PASSWORD = 'Abcd1234.'
)
GO
BACKUP MASTER KEY TO FILE = 'C:\Databases\BackupMasterKey.key'
ENCRYPTION BY PASSWORD = 'Abcd1234.'
GO
-- Backup database with encryption option & required encryption algorithm
BACKUP DATABASE Pubs
TO DISK = 'C:\Databases\BackupUserDB1_Encrypt.bak'
WITH
ENCRYPTION
(
ALGORITHM = AES_256,
SERVER CERTIFICATE = DBBackupEncryptCert
),
STATS = 10 , INIT
GO
-- BACKUP DATABASE successfully processed 378 pages in 0.554 seconds (5.317 MB/sec).


-- RESTORE
DROP DATABASE Pubs
GO
-- Steps to restore encrypted backup 
-- 1 � Server on which Encrypted backup going to restore has Master key & Encryption certificate. No Change is restore steps either from script or GUI required.
RESTORE DATABASE Pubs 
FROM DISK = 'C:\Databases\BackupUserDB1_Encrypt.bak'
GO

-- RESTORE DATABASE successfully processed 378 pages in 0.782 seconds (3.767 MB/sec).


-- 2 - Server on which Encrypted backup going to restore, Master key & Encryption certificate does not exists
RESTORE DATABASE Pubs 
FROM DISK = 'C:\Databases\BackupUserDB1_Encrypt.bak'
GO

-- Provocara ERROR
-- Mensaje
--Msg 33111, Level 16, State 3, Line 1
--Cannot find server certificate with thumbprint ......
--Msg 3013, Level 16, State 1, Line 1
--RESTORE DATABASE is terminating abnormally.