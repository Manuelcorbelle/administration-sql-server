-- Not  No ejecutar el primero . Hacer el de Clinical data

-- Encryption Column

-- 2 Examples

-- Credit Card
-- Clinical Data

----------------------------------------
-- Encryption Card Number

-- Para cifrar una columna de datos usando un cifrado simétrico simple y Certificado

USE AdventureWorks2014;
GO
SELECT * 
INTO Sales.TarjetadeCredito
FROM [Sales].[CreditCard]
GO

--If there is no master key, create one now. 
IF NOT EXISTS 
    (SELECT * FROM sys.symmetric_keys WHERE symmetric_key_id = 101)
    CREATE MASTER KEY ENCRYPTION BY 
    PASSWORD = 'Abcd1234.'
GO

CREATE CERTIFICATE Cert_Xan_2017
   WITH SUBJECT = 'Customer Credit Card Numbers';
GO

CREATE SYMMETRIC KEY SK_CreditCards_Xan_2017
    WITH ALGORITHM = AES_256
    ENCRYPTION BY CERTIFICATE Cert_Xan_2017;
GO

-- Create a column in which to store the encrypted data.
ALTER TABLE Sales.TarjetadeCredito
    ADD CardNumber_Encrypted varbinary(128); 
GO

-- Open the symmetric key with which to encrypt the data.
OPEN SYMMETRIC KEY SK_CreditCards_Xan_2017
   DECRYPTION BY CERTIFICATE Cert_Xan_2017;
GO
SELECT TOP 3 CardNumber, CardNumber_Encrypted FROM Sales.TarjetadeCredito
GO

--CardNumber		CardNumber_Encrypted
--33332664695310		NULL
--55552127249722		NULL
--77778344838353		NULL


-- Encrypt the value in column CardNumber using the
-- symmetric key CreditCards_Key11.
-- Save the result in column CardNumber_Encrypted.

  
UPDATE Sales.TarjetadeCredito
SET CardNumber_Encrypted = EncryptByKey(Key_GUID('SK_CreditCards_Xan_2017')
    , CardNumber, 1, HashBytes('SHA1', CONVERT( varbinary, CreditCardID)));
GO

-- (19118 row(s) affected)

SELECT TOP 3 *  FROM Sales.TarjetadeCredito
GO

--CreditCardID	CardType	CardNumber	ExpMonth	ExpYear	ModifiedDate	CardNumber_Encrypted
--1	SuperiorCard	33332664695310	11	2006	2013-07-29 00:00:00.000					0x00D799758AF7FF439A3F465F5D21471201000000B7090F42D4F6CEF0AD721506B8F59542B98F2A12CD31DD43CBC40F7674FA1DE1C5AFD2E0EC496A05974E8FF27160B62A76B8FDD188EF7602F3B015815C001B924022ECF10DF4A4323E6E81FE5E77190D
--2	Distinguish	55552127249722	8	2005	2013-12-05 00:00:00.000					0x00D799758AF7FF439A3F465F5D21471201000000464522E43E6EBA55D211F93892B7B75E804356BA0FB7875EEB7D7317D3BE060E6D694B3643FA667CF10D85D89F965399AAB3606338D1C0DBB7EA863B083AAF1048C56987A6E929A6A75081EEED429CE8
--3	ColonialVoice	77778344838353	7	2005	2014-01-14 00:00:00.000				0x00D799758AF7FF439A3F465F5D214712010000006178BFDBFF582E6BE7B941B3EE3796FB3F8D2A023B412D45473EEE8177B182116C00293A8C144B87B3F616733354AE7A448D751519DDB6A16A9F4525FAB8E3BEB00438B33263335231A397B0534041DF


-- Verify the encryption.
-- First, open the symmetric key with which to decrypt the data.

OPEN SYMMETRIC KEY SK_CreditCards_Xan_2017
   DECRYPTION BY CERTIFICATE Cert_Xan_2017
GO

-- Now list the original card number, the encrypted card number,
-- and the decrypted ciphertext. If the decryption worked,
-- the original number will match the decrypted number.

SELECT TOP 3 CardNumber, CardNumber_Encrypted 
    AS 'Encrypted card number', CONVERT(nvarchar,  DecryptByKey(CardNumber_Encrypted, 1 , 
    HashBytes('SHA1', CONVERT(varbinary, CreditCardID))))
    AS 'Decrypted card number' FROM Sales.TarjetadeCredito
GO

--CardNumber	   Encrypted card number																											Decrypted card number
--33332664695310	0x00C00F283656D646AA88A83F8ED73AAB01000000AE7090F32DE7B8A6E660C5ABA25132AD78B38B3368C4B2E7AD7AEB21C02BE3C472CB35F0A533C459518471BAA2F3FD96DFC510A32FC4FB76619ABD3F1F6BAE951315A75390CAF37F1B4B68AAFDCCCE4C	33332664695310
--55552127249722	0x00C00F283656D646AA88A83F8ED73AAB01000000DDDB21C8E836EDAFDA4607085876C2901A3025C9AE76C851DA3425D78F9B495941DBB725A70922818CD7403B7DE90C717EB6DD6B974BC51EC8DCD061995DADA4361D1FF5DBDB0EC84B97C788828C0752	55552127249722
--77778344838353	0x00C00F283656D646AA88A83F8ED73AAB01000000F561C4B01DBD43EC62D98B7C8CA66F6C77A1F5A73185DDF60B8B5040DA601F01884ABD3DA5037745391D78A09AAE7F250DF5E393886C3FE854DAD5E85F7CFBE92FF566FF10EFA0B53DCABF0154D8602A	77778344838353

---------------------------------------------
-- Encryption Clinic Data

-- One such scenario I talk to customers who have requirements of security is 
--around column level encryption. 
-- It is one of the most simplest implementation and yet difficult to visualize.
--  The scenario is simple where-in most of the HRMS (Hospital Management Systems) 
--come with a simple requirement that data of one person must be masked to other.

USE master
GO
DROP DATABASE hospitaldb
go
REVERT
GO
--   Creating the Logins for demo

CREATE LOGIN doctor1 WITH password = 'Abcd1234.'
go
CREATE LOGIN doctor2 WITH password = 'Abcd1234.'
go
---

IF DB_ID('[AdventureWorks2014]') IS NOT NULL
BEGIN
    PRINT 'Database Exists'
END
--2
IF EXISTS(SELECT * FROM master.sys.databases 
          WHERE name='[AdventureWorks2014]')
BEGIN
    PRINT 'Database Exists'
END

-- 3
IF EXISTS(SELECT * FROM master.sys.sysdatabases 
          WHERE name='[AdventureWorks2014]')
BEGIN
    PRINT 'Database Exists'
END

--

-- Old block of code

IF EXISTS (SELECT name FROM sys.databases WHERE name = '[AdventureWorks2014]')
    DROP DATABASE 
GO
 
-- New block of code in		SQL SERVER 2016
DROP DATABASE IF EXISTS [AdventureWorks2014]
GO


-----------


IF EXISTS (SELECT name FROM sys.databases WHERE name = 'HospitalDB')
    DROP DATABASE 
GO




DROP DATABASE IF EXISTS [Hospitaldb]
GO


CREATE DATABASE HospitalDB
GO

USE Hospitaldb
go

-- Creamos 2 Usuarios de BD Hospital

CREATE USER doctor1 without login
go
CREATE USER doctor2 without login
go

-- For our example we have two doctors, we want to build a mechanism where Doctor1 patients details
--  must not be visible to Doctor2. Let us create our simple table to keep values.

--------
-- Crear Tabla Pacientes . Comprobar su existencia

 
-- Old block of code before SQL SERVER 2016

-- 2 Formas


IF  EXISTS (SELECT * FROM sys.objects 
    WHERE object_id = OBJECT_ID('patientdata') AND type in ('U'))
DROP TABLE patientdata
GO

IF OBJECT_ID('patientdata','u') is not null
	DROP TABLE patientdata
GO


-- New block of code SQL SERVER 2016

DROP TABLE IF EXISTS patientdata
GO




-----------------
-- Let's encrypt 2 fields :  uid y symptom

-- Create table patientdata

CREATE TABLE patientdata
(
	id         INT,
	name       NVARCHAR(30),
	doctorname VARCHAR(25),
	uid        VARBINARY(1000),
	symptom    VARBINARY(4000)
)
go




-- Grant access to the table to both doctors
GRANT SELECT, INSERT ON patientdata TO doctor1;
GRANT SELECT, INSERT ON patientdata TO doctor2;
Go

-- Basic Encryption steps

-- Next step is to create our keys. 

-- To start with, we need to create our Master Key first. 
-- Then we will create the Certificates we will use. 
-- Create Symmetric key which will be encrypted by the certificates 


-- Create the Master Key

CREATE master KEY encryption BY password = 'Abcd1234.'
GO

SELECT name KeyName,
  symmetric_key_id KeyID,
  key_length KeyLength,
  algorithm_desc KeyAlgorithm
FROM sys.symmetric_keys;
GO

--KeyName					KeyID	KeyLength	KeyAlgorithm
--##MS_DatabaseMasterKey##	101			256			AES_256

-- Create a self-signed certificate


CREATE CERTIFICATE doctor1cert AUTHORIZATION doctor1 
WITH subject = 'Abcd1234.', start_date = '01/02/2017' 
GO

CREATE CERTIFICATE doctor2cert AUTHORIZATION doctor2 
WITH subject = 'Abcd1234.', start_date = '01/02/2017'
GO

SELECT name CertName,
  certificate_id CertID,
  pvt_key_encryption_type_desc EncryptType,
  issuer_name Issuer
FROM sys.certificates;
GO


--CertName	CertID	EncryptType	             Issuer
--doctor1cert	 256	ENCRYPTED_BY_MASTER_KEY	 Abcd1234.
--doctor2cert	 257	ENCRYPTED_BY_MASTER_KEY	 Abcd1234.



-- CREATE SYMMETRIC KEY

CREATE SYMMETRIC KEY doctor1key
WITH ALGORITHM = AES_256
ENCRYPTION BY CERTIFICATE doctor1cert 
GO

CREATE SYMMETRIC KEY doctor2key
WITH ALGORITHM = AES_256
ENCRYPTION BY CERTIFICATE doctor2cert 
GO
-- ERROR algorithm = triple_des
CREATE symmetric KEY doctor1key AUTHORIZATION doctor1 
WITH algorithm =triple_des encryption BY certificate doctor1cert 
GO
CREATE symmetric KEY doctor2key AUTHORIZATION doctor2 
WITH algorithm =triple_des encryption BY certificate doctor2cert 
GO

-- Let us next look at the Keys we just created using the DMVs.

SELECT *
FROM   sys.symmetric_keys 
GO 
--name	principal_id	symmetric_key_id	key_length	key_algorithm	algorithm_desc	create_date	modify_date	key_guid	key_thumbprint	provider_type	cryptographic_provider_guid	cryptographic_provider_algid
--##MS_DatabaseMasterKey##	1	101	256	A3	AES_256	2017-01-30 18:47:17.020	2017-01-30 18:47:17.020	02505A00-A89D-4C1A-97B7-A7ED6EF388D0	NULL	NULL	NULL	NULL
--doctor1key	1	256	256	A3	AES_256	2017-01-30 18:51:43.670	2017-01-30 18:51:43.670	BDF48900-C7CD-403E-B83D-686C59572DB0	NULL	NULL	NULL	NULL
--doctor2key	1	257	256	A3	AES_256	2017-01-30 18:52:01.397	2017-01-30 18:52:01.397	DBD5B600-FA58-4082-97E7-9954B5A20B08	NULL	NULL	NULL	NULL

REVERT 
GO
-----------------
-- NO
-- doctor1
--Grant View

PRINT user
GO
--GRANT VIEW DEFINITION ON CERTIFICATE::doctor1cert  TO doctor1
GRANT VIEW DEFINITION ON SYMMETRIC KEY::doctor1key TO doctor1

    --Grant Control
--GRANT CONTROL ON CERTIFICATE::doctor1cert TO doctor1
-- doctor2
--Grant View

--GRANT VIEW DEFINITION ON CERTIFICATE::doctor2cert  TO doctor2
GRANT VIEW DEFINITION ON SYMMETRIC KEY::doctor2key TO doctor2

    --Grant Control
-- GRANT CONTROL ON CERTIFICATE::doctor2cert TO doctor2
-------------------


-- Adding Data into table

-- Next is to simulate as two different users and enter some data into our tables. 

-- Let us first impersonate as Doctor1 and enter values, next we will do it for Doctor2.


EXECUTE AS User = 'doctor1'

GO
OPEN SYMMETRIC KEY doctor1key
    DECRYPTION BY CERTIFICATE doctor1cert
GO

-- Hint Print USER ?????
-- Error
--Msg 15151, Level 16, State 1, Line 130
--Cannot find the symmetric key 'doctor1key', because it does not exist or you do not have permission.



--  View the list of open keys in the session
SELECT *
FROM   sys.openkeys 
GO


--database_id	database_name	key_id	key_name	key_guid	opened_date	status
--12	hospitaldb	256	doctor1key	D57B5A00-B512-433E-A3D8-5027BEADAEA2	2017-01-30 19:13:30.147	1


-- Insert into our table.

INSERT INTO patientdata
VALUES      (1,
'Jack',
'Doctor1',
Encryptbykey(Key_guid('Doctor1Key'), '1111111111'),
Encryptbykey(Key_guid('Doctor1Key'), 'Cut'))
GO
INSERT INTO patientdata
VALUES      (2,
'Jill',
'Doctor1',
Encryptbykey(Key_guid('Doctor1Key'), '2222222222'),
Encryptbykey(Key_guid('Doctor1Key'), 'Bruise'))
GO
INSERT INTO patientdata
VALUES      (3,
'Jim',
'Doctor1',
Encryptbykey(Key_guid('Doctor1Key'), '3333333333'),
Encryptbykey(Key_guid('Doctor1Key'), 'Head ache'))
GO

SELECT * FROM patientdata
GO

--id	name	doctorname	uid																symptom
--1		Jack	Doctor1		0x005A7BD512B53E43A3D85027BEADAEA201000000D33772B7CFEA49C4EB854E184DF7803D2D02B6A4C2C5949FAD1A2903E8E0558115ED9FF942D9A0630E9B2555B5DC70D0	0x005A7BD512B53E43A3D85027BEADAEA2010000000C52F94AFF6CE976419E67EAA1E074050DAD406813DE5347A48C94CCEA0A3182
--2		Jill	Doctor1		0x005A7BD512B53E43A3D85027BEADAEA2010000000CEE8DAEDA92EAF6EB097563213EAFD7B3E054F448F5A1BCA1D0EB5F5D0D0757496D838505604DE19D7918FEB7EFB8FE	0x005A7BD512B53E43A3D85027BEADAEA2010000001EF36F1A4FA4661428653E125FA0A514F9C48A4FD8CC84AABBF58A85C0A7A7F8
--3		Jim	    Doctor1		0x005A7BD512B53E43A3D85027BEADAEA20100000022ED2CE3A2A5AF6DA666B64ED03A9B5B98B48ADE46BDD2BB9BC4641912DF1C9786FAAFA9A60E61E354CEB1A0D17EE90D	0x005A7BD512B53E43A3D85027BEADAEA201000000B340D3D95AD7776F26F2DD36B0BA90996A5ADED4041DD0005757E5242304C7535478D4CF8AC12C277205420BC9A8C25E

SELECT [name],[symptom] FROM patientdata
GO

--name	symptom
--Jack	0x005A7BD512B53E43A3D85027BEADAEA2010000000C52F94AFF6CE976419E67EAA1E074050DAD406813DE5347A48C94CCEA0A3182
--Jill	0x005A7BD512B53E43A3D85027BEADAEA2010000001EF36F1A4FA4661428653E125FA0A514F9C48A4FD8CC84AABBF58A85C0A7A7F8
--Jim	0x005A7BD512B53E43A3D85027BEADAEA201000000B340D3D95AD7776F26F2DD36B0BA90996A5ADED4041DD0005757E5242304C7535478D4CF8AC12C277205420BC9A8C25E


-- In this example the Doc1 has 3 records for him.
 
--Next is to revert back to Doctor2 and do the same set of operations.

--  Close all opened keys
CLOSE ALL symmetric keys
GO
REVERT 
GO

-- Impersonate as Doctor2 and do the same steps.

EXECUTE AS user = 'Doctor2'
GO
OPEN symmetric KEY doctor2key decryption BY certificate doctor2cert
GO
-- view the list of open keys in the session
SELECT *
FROM   sys.openkeys
GO

--database_id	database_name	key_id	key_name	key_guid	opened_date	status
--12	hospitaldb	257	doctor2key	B2DD2A00-343C-489C-8568-1DF5D80CB901	2017-01-30 19:19:53.203	1


INSERT INTO patientdata
VALUES      (4,
'Rick',
'Doctor2',
Encryptbykey(Key_guid('Doctor2Key'), '4444444444'),
Encryptbykey(Key_guid('Doctor2Key'), 'Cough'))
GO
INSERT INTO patientdata
VALUES      (5,
'Joe',
'Doctor2',
Encryptbykey(Key_guid('Doctor2Key'), '5555555555'),
Encryptbykey(Key_guid('Doctor2Key'), 'Asthma'))
GO
INSERT INTO patientdata
VALUES      (6,
'Pro',
'Doctor2',
Encryptbykey(Key_guid('Doctor2Key'), '6666666666'),
Encryptbykey(Key_guid('Doctor2Key'), 'Cold'))
GO

SELECT [name],[symptom] FROM patientdata
GO


CLOSE ALL symmetric keys
GO
-- View the list of open keys in the session
SELECT *
FROM   sys.openkeys
GO
REVERT
-- Select data and see values encrypted
SELECT *
FROM   patientdata
GO

SELECT [name],[doctorname],[symptom] 
FROM patientdata
GO


--name	doctorname	symptom
--Jack	Doctor1	0x005A7BD512B53E43A3D85027BEADAEA2010000000C52F94AFF6CE976419E67EAA1E074050DAD406813DE5347A48C94CCEA0A3182
--Jill	Doctor1	0x005A7BD512B53E43A3D85027BEADAEA2010000001EF36F1A4FA4661428653E125FA0A514F9C48A4FD8CC84AABBF58A85C0A7A7F8
--Jim	Doctor1	0x005A7BD512B53E43A3D85027BEADAEA201000000B340D3D95AD7776F26F2DD36B0BA90996A5ADED4041DD0005757E5242304C7535478D4CF8AC12C277205420BC9A8C25E
--Rick	Doctor2	0x002ADDB23C349C4885681DF5D80CB901010000002ADB2CFF507A9DD38ED3FBCDFF007372EAE973EB00147C25D708A6E93AF3BAD7
--Joe	Doctor2	0x002ADDB23C349C4885681DF5D80CB901010000002B1BBF2C2F92D2144552AE4691C17E342496FD604AD5E144EA2A083A1524A28F
--Pro	Doctor2	0x002ADDB23C349C4885681DF5D80CB90101000000E37BECCB15AFB987D089FD5CC60977B633CF3B264F0B9E4048C555F3A288BD39


-- As you can see the values are not visible as-is but has some garbage.

-- Impersonate as Doctor and show Values

-- The next step is to show that Doctor1 can see his data and Doctor2 can see his data alone. The steps are simple:
REVERT
GO

EXECUTE AS user = 'Doctor1'
GO
OPEN SYMMETRIC KEY doctor1key decryption  BY CERTIFICATE doctor1cert
GO
SELECT id,
name,
doctorname,
CONVERT(VARCHAR, Decryptbykey(uid))      AS UID,
CONVERT (VARCHAR, Decryptbykey(symptom)) AS Symptom
FROM   patientdata
GO

--id	name	doctorname	UID	Symptom
--1	Jack	Doctor1	1111111111	Cut
--2	Jill	Doctor1	2222222222	Bruise
--3	Jim	Doctor1	3333333333	Head ache
--4	Rick	Doctor2	NULL	NULL
--5	Joe	Doctor2	NULL	NULL
--6	Pro	Doctor2	NULL	NULL


-- Attention NULL
CLOSE ALL SYMMETRIC keys
GO
REVERT 
GO

-- Now let us impersonate as Doctor2 and check for values.

EXECUTE AS user = 'Doctor2'
GO
OPEN SYMMETRIC KEY doctor2key decryption  BY CERTIFICATE doctor2cert
GO
SELECT id,
name,
doctorname,
CONVERT(VARCHAR, Decryptbykey(uid))      AS UID,
CONVERT (VARCHAR, Decryptbykey(symptom)) AS Symptom
FROM   patientdata
GO

--id	name	doctorname	UID	Symptom
--1	Jack	Doctor1	NULL	NULL
--2	Jill	Doctor1	NULL	NULL
--3	Jim	Doctor1	NULL	NULL
--4	Rick	Doctor2	4444444444	Cough
--5	Joe	Doctor2	5555555555	Asthma
--6	Pro	Doctor2	6666666666	Cold


-- Null id Patient 1 2 3 
CLOSE ALL SYMMETRIC keys
GO


REVERT
GO 

-- Conclusion

-- As you can see this is a very simple implementation of column level encryption 
-- inside SQL Server and can be quite effective to mask data from each others in a multi-tenant 
-- environment. There are a number of reasons one can go for this solution. I thought this will 
-- be worth a shout even though the implementation has been in industry for close to a decade now.
DROP LOGIN doctor1 
DROP LOGIN doctor2
USE master
DROP DATABASE hospitaldb
GO