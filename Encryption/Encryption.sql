

-- https://www.simple-talk.com/sql/sql-development/encrypting-sql-server-using-encryption-hierarchy-protect-column-data/

-- Encrypting SQL Server: Using an Encryption Hierarchy to Protect Column Data


-- Preparing the environment

USE master;
GO
CREATE DATABASE EmpData;
GO
 
USE EmpData;
GO
 
CREATE TABLE EmpInfo(
  EmpID INT PRIMARY KEY,
  NatID NVARCHAR(15) NOT NULL,
  EncryptedID VARBINARY(128) NULL);
GO
 
INSERT INTO EmpInfo(EmpID, NatID) 
SELECT BusinessEntityID, NationalIDNumber
FROM AdventureWorks2014.HumanResources.Employee
WHERE NationalIDNumber IS NOT NULL;
GO

SELECT * FROM EmpInfo
GO

/*
The following steps outline the process we�ll follow to complete these tasks:

Create the database master key (DMK)
Create a self-signed certificate
Create a symmetric key
Encrypt the column data
Query the encrypted data

This process takes advantage of SQL Server�s encryption hierarchy and key management infrastructure to deliver a layered architecture made up of a public key certificate and several symmetric keys. Each layer encrypts the layer below it, using the security mechanisms built into SQL Server.

*/

-- Create the database master key

-- To create the DMK in the EmpData database

USE EmpData;
GO
 
CREATE MASTER KEY 
ENCRYPTION BY PASSWORD = 'abcd1234.'
GO


-- We can verify that the DMK key has been created by querying the sys.symmetric_keys catalog view in the EmpData database

SELECT name KeyName,
  symmetric_key_id KeyID,
  key_length KeyLength,
  algorithm_desc KeyAlgorithm
FROM sys.symmetric_keys;
GO

-- Create a self-signed certificate

CREATE CERTIFICATE Cert1
WITH SUBJECT = 'Employee national IDs';
GO

-- We can now verify that the certificate has been created by querying the sys.certificates catalog view:

SELECT name CertName,
  certificate_id CertID,
  pvt_key_encryption_type_desc EncryptType,
  issuer_name Issuer
FROM sys.certificates;
GO


-- Create a symmetric key

CREATE SYMMETRIC KEY SymKey1
WITH ALGORITHM = AES_256
ENCRYPTION BY CERTIFICATE Cert1;
GO

/*
The statement names the symmetric key SymKey1 and specifies that the 256-bit AES algorithm be used to encrypt the column data. SQL Server supports numerous other algorithms; however, starting with SQL Server 2016, all algorithms have been deprecated except 128-bit AES, 192-bit AES, and 256-bit AES, the one we�re using and the strongest of the three. (The higher the number of bits, the stronger the algorithm.)
*/

SELECT name KeyName,
  symmetric_key_id KeyID,
  key_length KeyLength,
  algorithm_desc KeyAlgorithm
FROM sys.symmetric_keys;
GO

-- Encrypt the data

/*

Encrypting the data is a relatively straightforward process that requires only three simple T-SQL statements:

An OPEN SYMMETRIC KEY statement to decrypt the symmetric key and make it available to our session
An UPDATE statement that uses the ENCRYPTBYKEY system function to encrypt the data
A CLOSE SYMMETRIC KEY statement to close the symmetric key so it�s no longer available to the session


*/

OPEN SYMMETRIC KEY SymKey1
DECRYPTION BY CERTIFICATE Cert1;
GO
UPDATE EmpInfo
SET EncryptedID = ENCRYPTBYKEY(KEY_GUID('SymKey1'), NatID);
GO
CLOSE SYMMETRIC KEY SymKey1;
GO

-- Query the encrypted data

SELECT TOP 5 * FROM EmpInfo;
GO

/*
As you can see, the EncryptedID values are returned as binary values that represent the encrypted data. If we want to see the actual values, we must again open our symmetric key and then use the DECRYPTBYKEY system function to decrypt the data when we retrieve it, as shown in the following T-SQL script:

*/

OPEN SYMMETRIC KEY SymKey1
DECRYPTION BY CERTIFICATE Cert1;
GO
SELECT TOP 5 EmpID, NatID,
  CONVERT(NVARCHAR(15), DECRYPTBYKEY(EncryptedID)) DecryptedID
FROM EmpInfo;
GO

/*

EmpID	NatID	DecryptedID
1	295847284	295847284
2	245797967	245797967
3	509647174	509647174
4	112457891	112457891
5	695256908	695256908
*/


CLOSE SYMMETRIC KEY SymKey1;
GO


-----------------------------------------------

-- https://www.simple-talk.com/sql/sql-development/encrypting-sql-server-transparent-data-encryption-tde/

-- Encrypting SQL Server: Transparent Data Encryption (TDE)

/*
Home SQL  Development  Encrypting SQL Server: Transparent Data Encryption (TDE)
12 JANUARY 2017
Encrypting SQL Server: Transparent Data Encryption (TDE)

Transparent Data Encryption (TDE) encrypts the data within the physical files of the database, the 'data at rest'. Without the original encryption certificate and master key, the data cannot be read when the drive is accessed or the physical media is stolen. The data in unencrypted data files can be read by restoring the files to another server. TDE requires planning but can be implemented without changing the database. Robert Sheldon explains how to implement TDE.

With the release of SQL Server 2008, Microsoft expanded the database engine�s security capabilities by adding Transparent Data Encryption (TDE), a built-in feature for encrypting data at rest. TDE protects the physical media that hold the data associated with a user database, including the data and log files and any backups or snapshots. Encrypting data at rest can help prevent those with malicious intent from being able to read the data should they manage to access the files.

SQL Server TDE takes an all-or-nothing approach to protecting data. When enabled, TDE encrypts all data in the database, as well as some outside the database. You cannot pick-and-choose like you can with column-level encryption.
*/


/*
to implement TDE on a user database, we must take the following steps:

Create the DMK in the master database, if it doesn�t already exist.
Create a certificate in the master database for securing the DEK.
Create the DEK in the user database to be encrypted.
Enable TDE on the user database.
*/

USE master;
GO
 
CREATE DATABASE EmpData2;
GO
 
USE EmpData2;
GO
 
CREATE TABLE EmpInfo(
  EmpID INT PRIMARY KEY,
  NatID NVARCHAR(15) NOT NULL,
  LoginID NVARCHAR(256) NOT NULL);
GO
 
INSERT INTO EmpInfo(EmpID, NatID, LoginID) 
SELECT BusinessEntityID, NationalIDNumber, LoginID
FROM AdventureWorks2014.HumanResources.Employee
WHERE NationalIDNumber IS NOT NULL;
GO

-- Create the DMK

USE master;
GO
 
CREATE MASTER KEY
ENCRYPTION BY PASSWORD = 'abcd1234.';
GO

SELECT name KeyName,
  symmetric_key_id KeyID,
  key_length KeyLength,
  algorithm_desc KeyAlgorithm
FROM sys.symmetric_keys;
GO

/*
KeyName	KeyID	KeyLength	KeyAlgorithm
##MS_DatabaseMasterKey##	101	256	AES_256
##MS_ServiceMasterKey##	102	256	AES_256
*/

-- Create the certificate

CREATE CERTIFICATE TdeCert
WITH SUBJECT = 'TDE certificate';
GO

SELECT name CertName,
  certificate_id CertID,
  pvt_key_encryption_type_desc EncryptType,
  issuer_name Issuer
FROM sys.certificates
WHERE issuer_name = 'TDE certificate';
GO


-- Create the DEK

/*
Now we switch over to our EmpData2 database to create the DEK, the next level down our hierarchy. When we create the DEK, we must specify the algorithm to use for the encryption key and the certificate to use to encrypt the DEK. Starting with SQL Server 2016, all algorithms have been deprecated except 128-bit AES, 192-bit AES, and 256-bit AES. (The higher the number of bits, the stronger the algorithm.)
*/

USE EmpData2;
GO
 
CREATE DATABASE ENCRYPTION KEY
WITH ALGORITHM = AES_256
ENCRYPTION BY SERVER CERTIFICATE TdeCert;
GO

-- Warning: The certificate used for encrypting the database encryption key has not been backed up. You should immediately back up the certificate and the private key associated with the certificate. If the certificate ever becomes unavailable or if you must restore or attach the database on another server, you must have backups of both the certificate and the private key or you will not be able to open the database.

SELECT DB_NAME(database_id) DbName,
  encryption_state EncryptState,
  key_algorithm KeyAlgorithm,
  key_length KeyLength,
  encryptor_type EncryptType
FROM sys.dm_database_encryption_keys;
GO

/*

the EncryptState column shows a value of 1. This indicates that the database is in an unencrypted state. According to SQL Server documentation, the column can display any one of the values described in the following table.

Value
Description
0
No database encryption key present, no encryption
1
Unencrypted
2
Encryption in progress
3
Encrypted
4
Key change in progress
5
Decryption in progress
6
The certificate or asymmetric key encrypting the DEK is being changed
*/

-- Enable TDE on the user database

ALTER DATABASE EmpData2
SET ENCRYPTION ON;
GO

SELECT DB_NAME(database_id) DbName,
  encryption_state EncryptState,
  key_algorithm KeyAlgorithm,
  key_length KeyLength,
  encryptor_type EncryptType
FROM sys.dm_database_encryption_keys;
GO

/*
DbName	EncryptState	KeyAlgorithm	KeyLength	EncryptType
tempdb		3				AES				256	ASYMMETRIC KEY
EmpData2	3				AES				256	CERTIFICATE
*/

/*

The results also show something else that�s very important to note�the addition of a row for the tempdb database. When you implement TDE on any user table, SQL Server also encrypts the tempdb database.

If you consider the logic behind this, you can see why Microsoft has taken this step. The database contains such items as temporary user objects, internal objects, and row versions, any of which can expose sensitive data. The downside to this is that unencrypted databases can take a performance hit, although Microsoft claims that the impact is minimal.

This issue aside, as long as our certificate and keys are in place, we can query the TDE-encrypted database just like we did before we enabled TDE. For example, we can run the following SELECT statement against the EmpInfo table:

*/

-- This issue aside, as long as our certificate and keys are in place, we can query the TDE-encrypted database just like we did before we enabled TDE. For example, we can run the following SELECT statement against the EmpInfo table:

SELECT TOP 5 * FROM EmpInfo;
GO

-- Disable TDE on the user database

ALTER DATABASE EmpData2
SET ENCRYPTION OFF;
GO

-- Back up the certificate and keys

Use master;
GO
 
BACKUP SERVICE MASTER KEY 
TO FILE = 'C:\Backup\SvcMasterKey.key'
ENCRYPTION BY PASSWORD = 'abcd1234.';
GO

-- Backing up the DMK works much the same way, except that you use a BACKUP MASTER KEY statement:

BACKUP MASTER KEY 
TO FILE = 'C:\Backup\DbMasterKey.key'
ENCRYPTION BY PASSWORD = 'abcd1234.'
GO

-- Backing up a certificate is a little different because you want to be sure to explicitly back up the private key along with the certificate. For that, you must use a BACKUP CERTIFICATE statement that includes the WITH PRIVATE KEY clause, as shown in the following example:

BACKUP CERTIFICATE TdeCert 
TO FILE = 'C:\Backup\TdeCert.cer'
WITH PRIVATE KEY(
  FILE = 'C:\Backup\TdeCert.key',
  ENCRYPTION BY PASSWORD = 'abcd1234.'
);
GO

-- In this case, we�re generating a file for both the certificate and the private key, as well as providing a password for the private key.

-- That�s all there is to backing up the certificate and keys. Of course, you should store your backup files in a remote locate separate from the database files, ensuring that they�re protected from any sort of mischief or recklessness.











