
-- PARTITIONS
--Crear un grupo o grupos de archivos y los archivos correspondientes que contendr�n las particiones especificadas por el esquema de partici�n.
--Crear una funci�n de partici�n que asigna las filas de una tabla o un �ndice a particiones seg�n los valores de una columna especificada.
--Crear un esquema de partici�n que asigna las particiones de una tabla o �ndice con particiones a los nuevos grupos de archivos.
--Crear o modificar una tabla o un �ndice y especificar el esquema de partici�n como ubicaci�n de almacenamiento.


-- OPERATIONS
-- Operations SPLIT-MERGE-SWITCH
-- TRUNCATE PARTITION

--Create a database with multiple files and filegroups
--Create a Partition Function and a Partition Scheme based on date
--Create a Table on the Partition
--Insert Data into the Table
--Investigate how the data is stored according to partition
 
USE master
IF EXISTS(select name 
	from sys.databases where name='PartitionTest')
DROP DATABASE PartitionTest
GO
 
 
CREATE DATABASE [PartitionTest]
 ON  PRIMARY
( NAME = N'PartitionTest', FILENAME = N'c:\data\PartitionTest.mdf' , SIZE = 4096KB , FILEGROWTH = 1024KB ), 
FILEGROUP [FGARCHIVE] 
( NAME = N'FGARCHIVE', FILENAME = N'c:\data\FGARCHIVE.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
 FILEGROUP [FG2013] 
( NAME = N'FG2013', FILENAME = N'c:\data\FG2013.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
FILEGROUP [FG2014] 
( NAME = N'FG2014', FILENAME = N'c:\data\FG2014.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
FILEGROUP [FG2015] 
( NAME = N'FG2015', FILENAME = N'c:\data\FG2015.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
FILEGROUP [FG2016] 
( NAME = N'FG2016', FILENAME = N'c:\data\FG2016.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB )
 LOG ON
( NAME = N'PartitionTest_log', FILENAME = N'c:\data\PartitionTest_log.ldf' , SIZE = 1024KB , FILEGROWTH = 10%)
GO
 

USE PartitionTest
 GO

-- PARTITION FUNCTION
CREATE PARTITION FUNCTION partFn_Date (datetime)
AS RANGE RIGHT FOR VALUES ('2013-01-01','2014-01-01','2015-01-01')
-- AS RANGE RIGHT FOR VALUES ('2013-01-01','2014-01-01','2015-01-01','2016-01-01')
GO
GO
 -- PARTITION SCHEME
CREATE PARTITION SCHEME part_Date
AS PARTITION partFn_Date
TO (FGARCHIVE,FG2013,FG2014,FG2015,FG2016)
GO

-- Partition scheme 'part_Date' has been created successfully. 'FG2015' is marked as the next 
-- used filegroup in partition scheme 'part_Date'.


-- SCHEME part_Date
Create Table Orders
(
OrderID Int Identity (10000,1),
OrderDesc Varchar(50),
OrderDate datetime,
OrderAmount money
)
ON part_Date (OrderDate)
GO

---Insert 2011 Orders
--INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2011-01-01',RAND()*100)
--INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2011-02-01',RAND()*100)
--INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2011-03-01',RAND()*100)
 
 
---Insert 2012 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-03-01',RAND()*100)
 
---Insert 2013 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-03-01',RAND()*100)
 
---Insert 2014 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-03-01',RAND()*100)
 
---Insert 2015 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-03-01',RAND()*100)
GO

---Insert 2016 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2016-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2016-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2016-03-01',RAND()*100)
GO
---Find rows and corresponding partitions
SELECT *,$Partition.partFn_Date(ORDERDate)
FROM Orders
GO
---find ranges for partitions
select *
from sys.partition_functions f
inner join sys.partition_range_values rv on f.function_id=rv.function_id
where f.name = 'partFn_Date'
GO
----Find number of rows per partition
select p.*
from sys.partitions p
inner join sys.tables t on p.object_id=t.object_id
and t.name = 'orders'
GO 

--partition_id	object_id	index_id	partition_number	hobt_id	rows	filestream_filegroup_id	data_compression	data_compression_desc
--72057594040549376	245575913	0	1	72057594040549376	3	0	0	NONE
--72057594040614912	245575913	0	2	72057594040614912	3	0	0	NONE
--72057594040680448	245575913	0	3	72057594040680448	3	0	0	NONE
--72057594040745984	245575913	0	4	72057594040745984	6	0	0	NONE


----other useful system views with paritioning info
 
select *
from sys.partition_schemes
GO


--paritioned table and index details
SELECT
      OBJECT_NAME(p.object_id) AS ObjectName,
      i.name                   AS IndexName,
      p.index_id               AS IndexID,
      ds.name                  AS PartitionScheme,   
      p.partition_number       AS PartitionNumber,
      fg.name                  AS FileGroupName,
      prv_left.value           AS LowerBoundaryValue,
      prv_right.value          AS UpperBoundaryValue,
      CASE pf.boundary_value_on_right
            WHEN 1 THEN 'RIGHT'
            ELSE 'LEFT' END    AS Range,
      p.rows AS Rows
FROM sys.partitions                  AS p
JOIN sys.indexes                     AS i
      ON i.object_id = p.object_id
      AND i.index_id = p.index_id
JOIN sys.data_spaces                 AS ds
      ON ds.data_space_id = i.data_space_id
JOIN sys.partition_schemes           AS ps
      ON ps.data_space_id = ds.data_space_id
JOIN sys.partition_functions         AS pf
      ON pf.function_id = ps.function_id
JOIN sys.destination_data_spaces     AS dds2
      ON dds2.partition_scheme_id = ps.data_space_id 
      AND dds2.destination_id = p.partition_number
JOIN sys.filegroups                  AS fg
      ON fg.data_space_id = dds2.data_space_id
LEFT JOIN sys.partition_range_values AS prv_left
      ON ps.function_id = prv_left.function_id
      AND prv_left.boundary_id = p.partition_number - 1
LEFT JOIN sys.partition_range_values AS prv_right
      ON ps.function_id = prv_right.function_id
      AND prv_right.boundary_id = p.partition_number 
WHERE
      OBJECTPROPERTY(p.object_id, 'ISMSShipped') = 0
UNION ALL
--non-partitioned table/indexes
SELECT
      OBJECT_NAME(p.object_id)    AS ObjectName,
      i.name                      AS IndexName,
      p.index_id                  AS IndexID,
      NULL                        AS PartitionScheme,
      p.partition_number          AS PartitionNumber,
      fg.name                     AS FileGroupName,  
      NULL                        AS LowerBoundaryValue,
      NULL                        AS UpperBoundaryValue,
      NULL                        AS Boundary, 
      p.rows                      AS Rows
FROM sys.partitions     AS p
JOIN sys.indexes        AS i
      ON i.object_id = p.object_id
      AND i.index_id = p.index_id
JOIN sys.data_spaces    AS ds
      ON ds.data_space_id = i.data_space_id
JOIN sys.filegroups           AS fg
      ON fg.data_space_id = i.data_space_id
WHERE
      OBJECTPROPERTY(p.object_id, 'ISMSShipped') = 0
ORDER BY
      ObjectName,
      IndexID,
      PartitionNumber;
GO

-- Otro

SELECT     
     p.partition_id                             as PartitionID
    ,t.name                                     as [Table]
    ,ps.name                                    as PartitionScheme
    ,pf.name                                    as PartitionFunction
    ,p.partition_number                         as [Partition]
    ,p.rows                                     as [Rows]
    ,prv.value                                  as Boundary
    --,pf.type_desc                               as BoundaryType
    ,case when pf.boundary_value_on_right = 1 
        then 'Right'
        else 'Left'
        end                                     as BoundarySide
 
FROM 
    sys.tables t
    inner join sys.partitions p 
        on t.object_id = p.object_id 
        and p.index_id = 1
    inner join sys.indexes i on p.object_id = i.object_id and p.index_id = i.index_id
    inner join sys.data_spaces ds on i.data_space_id = ds.data_space_id
    inner join sys.partition_schemes ps on ds.data_space_id = ps.data_space_id
    inner join sys.partition_functions pf on ps.function_id = pf.function_id
    left outer join sys.partition_range_values prv 
        on pf.function_id = prv.function_id
        and p.partition_number = prv.boundary_id
WHERE
    is_ms_shipped = 0   
    and exists (select 1 from sys.partitions p2
                where t.object_id = p2.object_id
                and partition_number > 1)
order by
     t.name
    ,p.partition_number desc
GO

--object	   p#	filegroup	rows	pages	comparison	value	first_page
--dbo.Orders	1	FGARCHIVE	 3	     2	     less than	2013-01-01 00:00:00.000	3:8
--dbo.Orders	2	FG2013	     3	     2	     less than	2014-01-01 00:00:00.000	4:8
--dbo.Orders	3	FG2014	     3	     2	     less than	2015-01-01 00:00:00.000	5:8
--dbo.Orders	4	FG2015	     6	     2	     less than	NULL	                6:8


DECLARE @TableName NVARCHAR(200) = '[dbo].[Orders]'
 
SELECT SCHEMA_NAME(o.schema_id) + '.' + OBJECT_NAME(i.object_id) AS [object]
     , p.partition_number AS [p#]
     , fg.name AS [filegroup]
     , p.rows
     , au.total_pages AS pages
     , CASE boundary_value_on_right
       WHEN 1 THEN 'less than'
       ELSE 'less than or equal to' END as comparison
     , rv.value
     , CONVERT (VARCHAR(6), CONVERT (INT, SUBSTRING (au.first_page, 6, 1) +
       SUBSTRING (au.first_page, 5, 1))) + ':' + CONVERT (VARCHAR(20),
       CONVERT (INT, SUBSTRING (au.first_page, 4, 1) +
       SUBSTRING (au.first_page, 3, 1) + SUBSTRING (au.first_page, 2, 1) +
       SUBSTRING (au.first_page, 1, 1))) AS first_page
FROM sys.partitions p
INNER JOIN sys.indexes i
     ON p.object_id = i.object_id
AND p.index_id = i.index_id
INNER JOIN sys.objects o
     ON p.object_id = o.object_id
INNER JOIN sys.system_internals_allocation_units au
     ON p.partition_id = au.container_id
INNER JOIN sys.partition_schemes ps
     ON ps.data_space_id = i.data_space_id
INNER JOIN sys.partition_functions f
     ON f.function_id = ps.function_id
INNER JOIN sys.destination_data_spaces dds
     ON dds.partition_scheme_id = ps.data_space_id
     AND dds.destination_id = p.partition_number
INNER JOIN sys.filegroups fg
     ON dds.data_space_id = fg.data_space_id
LEFT OUTER JOIN sys.partition_range_values rv
     ON f.function_id = rv.function_id
     AND p.partition_number = rv.boundary_id
WHERE i.index_id < 2
     AND o.object_id = OBJECT_ID(@TableName);
--------------------------
-- SPLIT - MERGE - SWITCH

--Creating a new Range in our Partition for new data (SPLIT RANGE)
--Merging an older Range of data for archiving (MERGE RANGE)
--Reviewing detailed metadata on partitions
--Removing old Filegroups and Data files no longer needed after archiving
--Using the SWITCH PARTITION command to move data between partitions (Archiving, Moving data from Staging to 
--Production tables in DW environment)



--
--Let us put the 2015 data into a new partition
Use PartitionTest
GO 
-- SPLIT

ALTER PARTITION FUNCTION partFn_Date()
SPLIT RANGE ('2015-01-01');
GO

SELECT *,$Partition.partFn_Date(ORDERDate)
FROM Orders
GO

-- MERGE
--Remove 2013 Partition using a Merge
ALTER PARTITION FUNCTION partFn_Date ()
MERGE RANGE ('2013-01-01');
GO
 
--
--Query to determine table filegroup by index and partition
--Script provided by Jason Strate
--http://www.jasonstrate.com/2013/01/determining-file-group-for-a-table/
--
USE PartitionTest
Go
SELECT OBJECT_SCHEMA_NAME(t.object_id) AS schema_name
,t.name AS table_name
,i.index_id
,i.name AS index_name
,p.partition_number
,fg.name AS filegroup_name
,FORMAT(p.rows, '#,###') AS rows
FROM sys.tables t
INNER JOIN sys.indexes i ON t.object_id = i.object_id
INNER JOIN sys.partitions p ON i.object_id=p.object_id AND i.index_id=p.index_id
LEFT OUTER JOIN sys.partition_schemes ps ON i.data_space_id=ps.data_space_id
LEFT OUTER JOIN sys.destination_data_spaces dds ON ps.data_space_id=dds.partition_scheme_id AND 
p.partition_number=dds.destination_id
INNER JOIN sys.filegroups fg ON COALESCE(dds.data_space_id, i.data_space_id)=fg.data_space_id
 
---Remove the file and filegroup you no longer need
USE master
GO
ALTER DATABASE PartitionTest
REMOVE FILE FG2013
GO
 
-- The file 'FG2013' has been removed.

USE master
GO
ALTER DATABASE PartitionTest
REMOVE FILEGROUP FG2013
GO

-- The filegroup 'FG2013' has been removed.

-- SWITCH
USE PartitionTest
--Switch Data into another table then truncate
Create Table OrdersArchive
(
OrderID Int NOT NULL,
OrderDesc Varchar(50),
OrderDate datetime,
OrderAmount money
)
ON FGArchive
GO
 
ALTER TABLE Orders SWITCH Partition 1 to OrdersArchive
GO

SELECT *
FROM OrdersArchive
GO
SELECT *
FROM Orders
GO
