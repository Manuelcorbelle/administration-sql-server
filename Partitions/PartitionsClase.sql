http://dbafromthecold.wordpress.com/2014/06/04/partitioning-basics-part-1/

http://dbafromthecold.wordpress.com/2014/06/12/partitioning-basics-part-2-splittingmerging-partitions/

http://www.sqlservercentral.com/blogs/the-dba-who-came-in-from-the-cold/2014/06/


USE [master];
GO
-- EXAMPLE HANDLING PARTITIONS

-- Let�s create a demo database

IF EXISTS(SELECT 1 FROM sys.databases WHERE name = 'DEMO')
BEGIN
 DROP DATABASE [Demo];
END
GO

CREATE DATABASE [Demo]
	ON PRIMARY 
(NAME     = 'Demo', 
 FILENAME = 'C:\DataBases\Demo.MDF'), 
	FILEGROUP [DEMO] DEFAULT 
(NAME     = 'Demo_Data', 
 FILENAME = 'C:\DataBases\Demo_Data.NDF')
	LOG ON 
(NAME    = 'Demo_Log', 
FILENAME = 'C:\DataBases\Demo_log.ldf')
GO

-- First thing to do is create a partition function.
-- This function defines the number of partitions we will intiailly have. 
-- It also specifies the boundaries of each partition.

USE [Demo];
GO
-- Nota
-- Print DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 7)
-- Oct 21 2014 12:00AM
-- PRINT DATEDIFF(dd, 0, GETUTCDATE())
-- 41924
-- Print GETUTCDATE()
-- Fecha cuando probe ejercicio
-- Oct 14 2014  3:47PM


IF NOT EXISTS(SELECT 1 FROM sys.partition_functions WHERE name = 'DemoPartitionFunction')
CREATE PARTITION FUNCTION DemoPartitionFunction (DATE)
AS RANGE RIGHT 
FOR VALUES (DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 7),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 6),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 5),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 4),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 3),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 2),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 1),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), 0),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -1),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -2),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -3),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -4),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -5),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -6),
			DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -7));
GO

-- So this statement has said that my partition function is going to be using dates 
--as boundaries. What I�ve done is setup a bunch of partitions for one week in the past 
-- and one week in the future. 
-- The function sets the right �side� of the partition as the boundary, 
-- basically specifying which side of the boundary value the partition extends. 
-- The best way of thinking about it is like this�..

--  Today�s date is 2014-10-13. 
-- So the boundary values created by running the above script are

2014-10-08 00:00:00.000
2014-10-09 00:00:00.000
2014-10-10 00:00:00.000
2014-10-11 00:00:00.000
2014-10-12 00:00:00.000
2014-10-13 00:00:00.000
2014-10-14 00:00:00.000
2014-10-15 00:00:00.000
2014-10-16 00:00:00.000
2014-10-17 00:00:00.000
2014-10-18 00:00:00.000
2014-10-19 00:00:00.000
2014-10-20 00:00:00.000
2014-10-21 00:00:00.000
NULL

-- Say x is a value in the table then the partitions would evaluate as

1. 2014-10-08 > x
2. 2014-05-28 <= x < 2014-05-29
3. 2014-05-29 <= x < 2014-05-30
4. 2014-05-30 <= x < 2014-05-31
5. 2014-05-31 <= x < 2014-06-01
6. 2014-06-01 <= x < 2014-06-02
7. 2014-06-02 <= x < 2014-06-03
8. 2014-06-03 <= x < 2014-06-04
9. 2014-06-04 <= x < 2014-06-05
10. 2014-06-05 <= x < 2014-06-06
11. 2014-06-06 <= x < 2014-06-07
12. 2014-06-07 <= x < 2014-06-08
13. 2014-06-08 <= x < 2014-06-09
14. 2014-06-09 <= x < 2014-06-10
15. 2014-06-10 <= x < 2014-06-11
16. 2014-06-11 <= x

-- So in these partitions, the data in the partition is always less than the value 
-- of that partition�s boundary.

-- After the partition function, a partition scheme needs to be created


IF NOT EXISTS(SELECT 1 FROM sys.partition_schemes WHERE name = 'DemoPartitionScheme')
CREATE PARTITION SCHEME DemoPartitionScheme
AS PARTITION DemoPartitionFunction
ALL TO (DEMO);
Go


-- The partition scheme maps the data to a partition function and specifies where the data
-- is going to be stored. The [ALL TO (DEMO)] part of the statement says that 
-- all partitions are going to be mapped to the DEMO filegroup.

-- Now that the function and scheme have been created we can create a table 
-- on the partitions
CREATE TABLE dbo.[DemoPartitionedTable]
(DemoID			INT IDENTITY(1,1),
 SomeData		SYSNAME,
 CaptureDate	DATE,

 CONSTRAINT [PK_DemoPartitionedTable] PRIMARY KEY CLUSTERED 
	(DemoID ASC, CaptureDate ASC)

) ON DemoPartitionScheme(CaptureDate);
GO


-- Instead of specifying that the table be placed on a filegroup, 
--the statement has [ON DemoPartitionScheme(CaptureDate)] at the end. 
--This means that the table is on the partition scheme, using the column CaptureDate 
--to determine which rows go on which partition.

-- Let�s insert some data into the table

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -8));
GO 457

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -7));
GO 493

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -6));
GO 486

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -5));
GO 413

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -4));
GO 473

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -3));
GO 461

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -2));
GO 422

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()), -1));
GO 461

INSERT INTO dbo.[DemoPartitionedTable]
(SomeData,CaptureDate)
VALUES
('Demo',DATEADD(dd, DATEDIFF(dd, 0, GETUTCDATE()),0));
GO 273

-- This will insert data into today�s partition and the partitions for the last 8 days.
--  This can be checked by running the following script


SELECT 
	t.name AS TableName, i.name AS IndexName, p.partition_number, 
	r.value AS BoundaryValue, p.rows
FROM 
	sys.tables AS t
INNER JOIN
	sys.indexes AS i ON t.object_id = i.object_id
INNER JOIN
	sys.partitions AS p ON i.object_id = p.object_id AND i.index_id = p.index_id 
INNER JOIN
	sys.partition_schemes AS s ON i.data_space_id = s.data_space_id
INNER JOIN
	sys.partition_functions AS f ON s.function_id = f.function_id
LEFT OUTER JOIN 
	sys.partition_range_values AS r ON f.function_id = r.function_id AND r.boundary_id = p.partition_number
WHERE 
	t.name = 'DemoPartitionedTable'
AND 	i.type <= 1
ORDER BY p.partition_number;
GO

-- This will show each partition and the number of rows in it


-- Outcome

TableName	IndexName	partition_number	BoundaryValue	rows
DemoPartitionedTable	PK_DemoPartitionedTable	1	2014-10-06 00:00:00.000	457
DemoPartitionedTable	PK_DemoPartitionedTable	2	2014-10-07 00:00:00.000	493
DemoPartitionedTable	PK_DemoPartitionedTable	3	2014-10-08 00:00:00.000	486
DemoPartitionedTable	PK_DemoPartitionedTable	4	2014-10-09 00:00:00.000	413
DemoPartitionedTable	PK_DemoPartitionedTable	5	2014-10-10 00:00:00.000	473
DemoPartitionedTable	PK_DemoPartitionedTable	6	2014-10-11 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	7	2014-10-12 00:00:00.000	422
DemoPartitionedTable	PK_DemoPartitionedTable	8	2014-10-13 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	9	2014-10-14 00:00:00.000	273
DemoPartitionedTable	PK_DemoPartitionedTable	10	2014-10-15 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	11	2014-10-16 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	12	2014-10-17 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	13	2014-10-18 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	14	2014-10-19 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	15	2014-10-20 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	16	NULL	0

-- Remember that the boundary values are the upper limit, 
-- no value in the partitions will match that value due to the partition function
--  being specified as RANGE RIGHT.

-- Splitting/Merging Partitions

-- These partitions will hold data until the 2014-10-20 ,
-- after which all records in the table will be put into the end partition (number 16).
-- This means that the end partition will end up holding a lot of records. 
--In order to prevent this, a new partition needs to be created.

-- To do this, firstly the partition scheme needs to be told which filegroup 
-- the next partition will use:

ALTER PARTITION SCHEME DemoPartitionScheme
    NEXT USED [Demo];
GO

-- Then a SPLIT command is run against the partition function

ALTER PARTITION FUNCTION DemoPartitionFunction()
    SPLIT RANGE ('2014-10-20');
GO

-- Error
--Msg 7721, Level 16, State 1, Line 183
--Duplicate range boundary values are not allowed in partition function boundary values list.
-- The boundary value being added is already present at ordinal 15 of the boundary value list.

ALTER PARTITION FUNCTION DemoPartitionFunction()
    SPLIT RANGE ('2014-10-21');
GO
--Command(s) completed successfully.

-- And now when the data checking script is run, the results are

SELECT
    t.name AS TableName, i.name AS IndexName, p.partition_number, 
    r.value AS BoundaryValue, p.rows
FROM
    sys.tables AS t
INNER JOIN
    sys.indexes AS i ON t.object_id = i.object_id
INNER JOIN
    sys.partitions AS p ON i.object_id = p.object_id AND i.index_id = p.index_id 
INNER JOIN
    sys.partition_schemes AS s ON i.data_space_id = s.data_space_id
INNER JOIN
    sys.partition_functions AS f ON s.function_id = f.function_id
LEFT OUTER JOIN
    sys.partition_range_values AS r ON f.function_id = r.function_id AND r.boundary_id = p.partition_number
WHERE
    t.name = 'DemoPartitionedTable'
AND
    i.type <= 1
ORDER BY p.partition_number
GO

-- Outcome

TableName	IndexName	partition_number	BoundaryValue	rows
DemoPartitionedTable	PK_DemoPartitionedTable	1	2014-10-06 00:00:00.000	457
DemoPartitionedTable	PK_DemoPartitionedTable	2	2014-10-07 00:00:00.000	493
DemoPartitionedTable	PK_DemoPartitionedTable	3	2014-10-08 00:00:00.000	486
DemoPartitionedTable	PK_DemoPartitionedTable	4	2014-10-09 00:00:00.000	413
DemoPartitionedTable	PK_DemoPartitionedTable	5	2014-10-10 00:00:00.000	473
DemoPartitionedTable	PK_DemoPartitionedTable	6	2014-10-11 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	7	2014-10-12 00:00:00.000	422
DemoPartitionedTable	PK_DemoPartitionedTable	8	2014-10-13 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	9	2014-10-14 00:00:00.000	273
DemoPartitionedTable	PK_DemoPartitionedTable	10	2014-10-15 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	11	2014-10-16 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	12	2014-10-17 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	13	2014-10-18 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	14	2014-10-19 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	15	2014-10-20 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	16	2014-10-21 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	17	NULL	0

-- Partition 16 now has a boundary value of 2014-10-21 and a new partition (17) 
-- has been created.

-- But how can a partition be removed? There�s a really simple MERGE command that performs
--  this operation:-

ALTER PARTITION FUNCTION DemoPartitionFunction()
    MERGE RANGE ('2014-10-06');
GO

SELECT
    t.name AS TableName, i.name AS IndexName, p.partition_number, 
    r.value AS BoundaryValue, p.rows
FROM
    sys.tables AS t
INNER JOIN
    sys.indexes AS i ON t.object_id = i.object_id
INNER JOIN
    sys.partitions AS p ON i.object_id = p.object_id AND i.index_id = p.index_id 
INNER JOIN
    sys.partition_schemes AS s ON i.data_space_id = s.data_space_id
INNER JOIN
    sys.partition_functions AS f ON s.function_id = f.function_id
LEFT OUTER JOIN
    sys.partition_range_values AS r ON f.function_id = r.function_id AND r.boundary_id = p.partition_number
WHERE
    t.name = 'DemoPartitionedTable'
AND
    i.type <= 1
ORDER BY p.partition_number
GO

-- Outcome

--DemoPartitionedTable	PK_DemoPartitionedTable	1	2014-10-06 00:00:00.000	457
--DemoPartitionedTable	PK_DemoPartitionedTable	2	2014-10-07 00:00:00.000	493

--Borra la 1 y suma 457 + 493 =950

TableName	IndexName	partition_number	BoundaryValue	rows
DemoPartitionedTable	PK_DemoPartitionedTable	1	2014-10-07 00:00:00.000	950
DemoPartitionedTable	PK_DemoPartitionedTable	2	2014-10-08 00:00:00.000	486
DemoPartitionedTable	PK_DemoPartitionedTable	3	2014-10-09 00:00:00.000	413
DemoPartitionedTable	PK_DemoPartitionedTable	4	2014-10-10 00:00:00.000	473
DemoPartitionedTable	PK_DemoPartitionedTable	5	2014-10-11 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	6	2014-10-12 00:00:00.000	422
DemoPartitionedTable	PK_DemoPartitionedTable	7	2014-10-13 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	8	2014-10-14 00:00:00.000	273
DemoPartitionedTable	PK_DemoPartitionedTable	9	2014-10-15 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	10	2014-10-16 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	11	2014-10-17 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	12	2014-10-18 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	13	2014-10-19 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	14	2014-10-20 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	15	2014-10-21 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	16	NULL	0

-- So the data in the partition with the boundary value 2014-10-06 has been merged into 
--the partition with the boundary of 2014-10-07. 
-- The problem now is that the last partition has data from the partition that was just 
-- merged into it as well as the existing data that was already there.
--  In order to prevent this from happening, the data from the partition that is about
--  to be merged can be switched into another table before the MERGE command is executed.

-- Switching Data

-- How partitions can be used to archive data from a table. 
--Firstly a table to archive the data from the primary table needs to be created

CREATE TABLE dbo.[DemoPartitionedTable_Archive]
(DemoID         INT IDENTITY(1,1),
 SomeData       SYSNAME,
 CaptureDate    DATE,
 
 CONSTRAINT [PK_DemoPartitionedTable_Archive] PRIMARY KEY CLUSTERED 
    (DemoID ASC, CaptureDate ASC) 
 
) ON DEMO;
GO

-- I haven�t created this table on the partition scheme but it could be done because, 
-- according to Microsoft:

-- When a table and its indexes are aligned, then SQL Server can move partitions in and
--  out of partitioned tables more effectively, because all related data and indexes 
-- are divided with the same algorithm.�

-- So if the archive table was on the partition scheme it would be �Aligned�. 
-- This means that moving data in and out would be more efficient. 
-- The only reason I haven�t done this here is because this is a basic demo.

-- Before the switch is performed the data in the partitions needs to be checked:-

SELECT
    t.name AS TableName, i.name AS IndexName, p.partition_number, 
    r.value AS BoundaryValue, p.rows
FROM
    sys.tables AS t
INNER JOIN
    sys.indexes AS i ON t.object_id = i.object_id
INNER JOIN
    sys.partitions AS p ON i.object_id = p.object_id AND i.index_id = p.index_id 
INNER JOIN
    sys.partition_schemes AS s ON i.data_space_id = s.data_space_id
INNER JOIN
    sys.partition_functions AS f ON s.function_id = f.function_id
LEFT OUTER JOIN
    sys.partition_range_values AS r ON f.function_id = r.function_id AND r.boundary_id = p.partition_number
WHERE
    t.name = 'DemoPartitionedTable'
AND
    i.type <= 1
ORDER BY p.partition_number;
GO

-- Outcome

TableName	IndexName	partition_number	BoundaryValue	rows
DemoPartitionedTable	PK_DemoPartitionedTable	1	2014-10-07 00:00:00.000	950
DemoPartitionedTable	PK_DemoPartitionedTable	2	2014-10-08 00:00:00.000	486
DemoPartitionedTable	PK_DemoPartitionedTable	3	2014-10-09 00:00:00.000	413
DemoPartitionedTable	PK_DemoPartitionedTable	4	2014-10-10 00:00:00.000	473
DemoPartitionedTable	PK_DemoPartitionedTable	5	2014-10-11 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	6	2014-10-12 00:00:00.000	422
DemoPartitionedTable	PK_DemoPartitionedTable	7	2014-10-13 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	8	2014-10-14 00:00:00.000	273
DemoPartitionedTable	PK_DemoPartitionedTable	9	2014-10-15 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	10	2014-10-16 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	11	2014-10-17 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	12	2014-10-18 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	13	2014-10-19 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	14	2014-10-20 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	15	2014-10-21 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	16	NULL	0


/****** Script for SelectTopNRows command from SSMS  ******/

SELECT TOP 1000 [DemoID]
      ,[SomeData]
      ,[CaptureDate]
  FROM [Demo].[dbo].[DemoPartitionedTable_Archive]

  SELECT Count(*) as Registros
  FROM [Demo].[dbo].[DemoPartitionedTable_Archive]

  --950
  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [DemoID]
      ,[SomeData]
      ,[CaptureDate]
  FROM [Demo].[dbo].[DemoPartitionedTable]

  SELECT Count(*) as Registros
  FROM [Demo].[dbo].[DemoPartitionedTable]

  -- 2989


-- In order to switch the data from partition 1 in the primary table to the archive table,
--  the following script needs to be run

ALTER TABLE dbo.[DemoPartitionedTable]
SWITCH PARTITION 1
TO dbo.[DemoPartitionedTable_Archive];
GO

SELECT
    t.name AS TableName, i.name AS IndexName, p.partition_number, 
    r.value AS BoundaryValue, p.rows
FROM
    sys.tables AS t
INNER JOIN
    sys.indexes AS i ON t.object_id = i.object_id
INNER JOIN
    sys.partitions AS p ON i.object_id = p.object_id AND i.index_id = p.index_id 
INNER JOIN
    sys.partition_schemes AS s ON i.data_space_id = s.data_space_id
INNER JOIN
    sys.partition_functions AS f ON s.function_id = f.function_id
LEFT OUTER JOIN
    sys.partition_range_values AS r ON f.function_id = r.function_id AND r.boundary_id = p.partition_number
WHERE
    t.name = 'DemoPartitionedTable'
AND
    i.type <= 1
ORDER BY p.partition_number;
GO

-- Outcome
TableName	IndexName	partition_number	BoundaryValue	rows
DemoPartitionedTable	PK_DemoPartitionedTable	1	2014-10-07 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	2	2014-10-08 00:00:00.000	486
DemoPartitionedTable	PK_DemoPartitionedTable	3	2014-10-09 00:00:00.000	413
DemoPartitionedTable	PK_DemoPartitionedTable	4	2014-10-10 00:00:00.000	473
DemoPartitionedTable	PK_DemoPartitionedTable	5	2014-10-11 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	6	2014-10-12 00:00:00.000	422
DemoPartitionedTable	PK_DemoPartitionedTable	7	2014-10-13 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	8	2014-10-14 00:00:00.000	273
DemoPartitionedTable	PK_DemoPartitionedTable	9	2014-10-15 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	10	2014-10-16 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	11	2014-10-17 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	12	2014-10-18 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	13	2014-10-19 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	14	2014-10-20 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	15	2014-10-21 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	16	NULL	0

-- Partition 1 has 0 rows and could be merged. 
-- The following script will merge the partition

ALTER PARTITION FUNCTION DemoPartitionFunction()
    MERGE RANGE ('2014-10-07');
GO

SELECT
    t.name AS TableName, i.name AS IndexName, p.partition_number, 
    r.value AS BoundaryValue, p.rows
FROM
    sys.tables AS t
INNER JOIN
    sys.indexes AS i ON t.object_id = i.object_id
INNER JOIN
    sys.partitions AS p ON i.object_id = p.object_id AND i.index_id = p.index_id 
INNER JOIN
    sys.partition_schemes AS s ON i.data_space_id = s.data_space_id
INNER JOIN
    sys.partition_functions AS f ON s.function_id = f.function_id
LEFT OUTER JOIN
    sys.partition_range_values AS r ON f.function_id = r.function_id AND r.boundary_id = p.partition_number
WHERE
    t.name = 'DemoPartitionedTable'
AND
    i.type <= 1
ORDER BY p.partition_number;
GO

-- Outcome

TableName	IndexName	partition_number	BoundaryValue	rows
DemoPartitionedTable	PK_DemoPartitionedTable	1	2014-10-08 00:00:00.000	486
DemoPartitionedTable	PK_DemoPartitionedTable	2	2014-10-09 00:00:00.000	413
DemoPartitionedTable	PK_DemoPartitionedTable	3	2014-10-10 00:00:00.000	473
DemoPartitionedTable	PK_DemoPartitionedTable	4	2014-10-11 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	5	2014-10-12 00:00:00.000	422
DemoPartitionedTable	PK_DemoPartitionedTable	6	2014-10-13 00:00:00.000	461
DemoPartitionedTable	PK_DemoPartitionedTable	7	2014-10-14 00:00:00.000	273
DemoPartitionedTable	PK_DemoPartitionedTable	8	2014-10-15 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	9	2014-10-16 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	10	2014-10-17 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	11	2014-10-18 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	12	2014-10-19 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	13	2014-10-20 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	14	2014-10-21 00:00:00.000	0
DemoPartitionedTable	PK_DemoPartitionedTable	15	NULL	0

-- The partition has had its data switched out to the archive table and 
-- then was merged into the above partition.


SELECT TOP 1000 [DemoID]
      ,[SomeData]
      ,[CaptureDate]
  FROM [Demo].[dbo].[DemoPartitionedTable_Archive]

  SELECT Count(*) as Registros
  FROM [Demo].[dbo].[DemoPartitionedTable_Archive]

  --950
  /****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [DemoID]
      ,[SomeData]
      ,[CaptureDate]
  FROM [Demo].[dbo].[DemoPartitionedTable]

  SELECT Count(*) as Registros
  FROM [Demo].[dbo].[DemoPartitionedTable]
