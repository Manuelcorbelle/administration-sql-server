-- http://visakhm.blogspot.com.es/2016/08/whats-new-in-sql-2016-temporal-tables.html

-- https://sqlwithmanoj.com/2015/06/15/temporal-data-support-in-sql-server-2016-part-1/
-- https://sqlwithmanoj.com/2015/06/17/time-travel-with-temporal-tables-in-sql-server-2016-part-2/



---------------------

-- http://www.sqlservercentral.com/articles/SQL+Server+2016/147087/

IF DB_ID('Tablatemporal') is not null
	DROP DATABASE Tablatemporal
GO
Create database Tablatemporal
go
use Tablatemporal
go
CREATE TABLE [dbo].[Inventory](
    [ProductId] nvarchar(20) PRIMARY KEY CLUSTERED,
    [QuantityInStock] int NOT NULL,
    [QuantityReserved] int NOT NULL,
    [SysStartTime] datetime2 GENERATED ALWAYS AS ROW START NOT NULL,
    [SysEndTime] datetime2 GENERATED ALWAYS AS ROW END NOT NULL,
    PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime])
)
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [dbo].[Inventory_History]));
GO
-- Ver GUI 2 Tablas Inventory y Inventory_History (esta sin PK)
----------
ALTER TABLE [dbo].[Inventory] ADD
                             [SysStartTime] datetime2(0) GENERATED ALWAYS AS ROW START HIDDEN NOT NULL,
                             [SysEndTime] datetime2(0) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL,
                             PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime])

ALTER TABLE [dbo].[Inventory]
                             SET (SYSTEM_VERSIONING = ON);

							 --------------------------

INSERT INTO dbo.Inventory(ProductId, QuantityInStock, QuantityReserved) 
VALUES
('OilFilter1', 59, 5),
('OilFilter2', 23, 2),
('FuelFilter1', 120, 0),
('FuelFilter2', 35, 5),
('FuelFilter3', 10, 10);
GO
-- (5 row(s) affected)

SELECT * FROM dbo.Inventory
GO

-- [SysStartTime]						[SysEndTime]
-- 2016-11-17 19:10:11.1537764		9999-12-31 23:59:59.9999999

SELECT * FROM dbo.Inventory_History
GO

-- Vacia por ahora. Sin movimientos en la Tabla principal


WAITFOR DELAY '00:00:02';
GO

-- [SysStartTime]   2016-11-17 19:10:11.1537764
-- OilFilter1	QuantityInStock 59	QuantityReserved 5

-- Cambio informaci�n en Tabla Inventory
UPDATE dbo.Inventory SET
    QuantityInStock = 54,
    QuantityReserved = 0
WHERE ProductId = 'OilFilter1';
GO

-- (1 row(s) affected)


SELECT * FROM dbo.Inventory
GO

-- -- OilFilter1	54	0	2016-11-17 19:14:21.0291982	9999-12-31 23:59:59.9999999



SELECT * FROM dbo.Inventory_History
GO

--						[SysStartTime]					[SysEndTime]
-- OilFilter1	59	5	2016-11-17 19:10:11.1537764	2016-11-17 19:14:21.0291982

UPDATE dbo.Inventory SET
    QuantityInStock = 21,
    QuantityReserved = 0
WHERE ProductId = 'OilFilter2';
GO

-- (1 row(s) affected)

SELECT * FROM dbo.Inventory
GO

--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--FuelFilter1	120	0	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter2	35	5	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter3	10	10	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--OilFilter1	54	0	2016-11-17 19:14:21.0291982	9999-12-31 23:59:59.9999999
--OilFilter2	21	0	2016-11-17 19:21:26.9048453	9999-12-31 23:59:59.9999999


SELECT * FROM dbo.Inventory_History
GO

--ProductId	QuantityInStock	QuantityReserved	 SysStartTime							SysEndTime
--OilFilter1	59				5	              2016-11-17 19:10:11.1537764			2016-11-17 19:14:21.0291982
--OilFilter2	23				2				  2016-11-17 19:10:11.1537764			2016-11-17 19:21:26.9048453

WAITFOR DELAY '00:00:02';
Go

DELETE FROM dbo.Inventory
WHERE ProductId LIKE 'FuelFilter%';
GO

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory;
GO



SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory_History;
GO

--  To perform any type of time-based analysis, use the new FOR SYSTEM_TIME clause with four temporal-specific sub-clauses to query data across the current and history tables. 

-- AS OF <date_time>
-- FROM <start_date_time> TO <end_date_time>
-- BETWEEN <start_date_time> AND <end_date_time>
-- CONTAINED IN (<start_date_time> , <end_date_time>)
-- ALL

-----------------------


-- Pruebo Insert

INSERT INTO dbo.Inventory(ProductId, QuantityInStock, QuantityReserved) 
VALUES
('Ruedas', 13, 3)
GO


SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory;
GO

--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--FuelFilter1	120	0	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter2	35	5	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter3	10	10	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--OilFilter1	54	0	2016-11-17 19:14:21.0291982	9999-12-31 23:59:59.9999999
--OilFilter2	21	0	2016-11-17 19:21:26.9048453	9999-12-31 23:59:59.9999999

--Ruedas	13	3	2016-11-22 17:41:37.3566545	9999-12-31 23:59:59.9999999


SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory_History;
GO

--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--OilFilter1	59	5	2016-11-17 19:10:11.1537764	2016-11-17 19:14:21.0291982
--OilFilter2	23	2	2016-11-17 19:10:11.1537764	2016-11-17 19:21:26.9048453




DELETE dbo.Inventory
Where ProductId ='Ruedas'
GO

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory;
GO

--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--FuelFilter1	120	0	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter2	35	5	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter3	10	10	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--OilFilter1	54	0	2016-11-17 19:14:21.0291982	9999-12-31 23:59:59.9999999
--OilFilter2	21	0	2016-11-17 19:21:26.9048453	9999-12-31 23:59:59.9999999


SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory_History;
GO


--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--OilFilter1	59	5	2016-11-17 19:10:11.1537764	2016-11-17 19:14:21.0291982
--OilFilter2	23	2	2016-11-17 19:10:11.1537764	2016-11-17 19:21:26.9048453
--Ruedas	13	3	2016-11-22 17:41:37.3566545	2016-11-22 17:45:39.3566844



--Para comprobar lo que ocurrio desde el principio

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory
FOR SYSTEM_TIME ALL;
Go


--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--FuelFilter1	120	0	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter2	35	5	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter3	10	10	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--OilFilter1	54	0	2016-11-17 19:14:21.0291982	9999-12-31 23:59:59.9999999
--OilFilter2	21	0	2016-11-17 19:21:26.9048453	9999-12-31 23:59:59.9999999
--OilFilter1	59	5	2016-11-17 19:10:11.1537764	2016-11-17 19:14:21.0291982
--OilFilter2	23	2	2016-11-17 19:10:11.1537764	2016-11-17 19:21:26.9048453
--Ruedas	13	3	2016-11-22 17:41:37.3566545	2016-11-22 17:45:39.3566844

-- https://msdn.microsoft.com/en-us/library/mt591018.aspx


SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory
GO


--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--FuelFilter1	120	0	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter2	35	5	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter3	10	10	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--OilFilter1	54	0	2016-11-17 19:14:21.0291982	9999-12-31 23:59:59.9999999
--OilFilter2	21	0	2016-11-17 19:21:26.9048453	9999-12-31 23:59:59.9999999


-- --AS OF <date_time> -- Returns values for a specific data and time.

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory
FOR SYSTEM_TIME AS OF '2016-11-17 19:14:21.0291982' ;
Go

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory
FOR SYSTEM_TIME AS OF '2016-09-30 06:47:58' ;

-- --FROM <start_date_time> TO <end_date_time> -- Returns a range of values between the specified date and time.

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory
FOR SYSTEM_TIME FROM '2016-09-30 06:47:55' TO '2016-09-30 06:47:56';


-- BETWEEN <start_date_time> AND <end_date_time> -- Returns a range of values between the specified date and time.
SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory
FOR SYSTEM_TIME BETWEEN '2016-09-30 06:47:55' AND '2016-09-30 06:47:56';
GO


-- CONTAINED IN (<start_date_time>, <end_date_time>) -- Returns a range of values based in the supplied date and time values.

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory
FOR SYSTEM_TIME CONTAINED IN ('2016-09-30 06:47:54', '2016-09-30 06:47:57');


-- ALL � Return all values

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory
FOR SYSTEM_TIME ALL;
Go


--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--FuelFilter1	120	0	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter2	35	5	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--FuelFilter3	10	10	2016-11-17 19:10:11.1537764	9999-12-31 23:59:59.9999999
--OilFilter1	54	0	2016-11-17 19:14:21.0291982	9999-12-31 23:59:59.9999999
--OilFilter2	21	0	2016-11-17 19:21:26.9048453	9999-12-31 23:59:59.9999999
--OilFilter1	59	5	2016-11-17 19:10:11.1537764	2016-11-17 19:14:21.0291982
--OilFilter2	23	2	2016-11-17 19:10:11.1537764	2016-11-17 19:21:26.9048453

SELECT ProductId, QuantityInStock, QuantityReserved, SysStartTime, SysEndTime
FROM dbo.Inventory_History;
GO


--ProductId	QuantityInStock	QuantityReserved	SysStartTime	SysEndTime
--OilFilter1	59	5	2016-11-17 19:10:11.1537764	2016-11-17 19:14:21.0291982
--OilFilter2	23	2	2016-11-17 19:10:11.1537764	2016-11-17 19:21:26.9048453
------------------

BEGIN TRANSACTION;

ALTER TABLE dbo.Inventory
SET (SYSTEM_VERSIONING = OFF);

GO

ALTER TABLE dbo.Inventory ADD
                             City nvarchar(20) NULL;

ALTER TABLE dbo.Inventory_History ADD
                             City nvarchar(20) NULL;

ALTER TABLE dbo.Inventory
SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [dbo].[Inventory_History], DATA_CONSISTENCY_CHECK = ON));

COMMIT;

ALTER TABLE dbo.Inventory
SET (SYSTEM_VERSIONING = OFF);

GO

DELETE FROM dbo.Inventory_History
WHERE SysEndTime <= '2016-09-30 06:47:56';

ALTER TABLE dbo.Inventory
SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = [dbo].[Inventory_History], DATA_CONSISTENCY_CHECK = ON));

COMMIT;

CREATE TABLE [dbo].[Inventory](
                             [ProductId] nvarchar(20) PRIMARY KEY NONCLUSTERED,
                             [QuantityInStock] int NOT NULL,
                             [QuantityReserved] int NOT NULL,
                             [SysStartTime] datetime2(0) GENERATED ALWAYS AS ROW START NOT NULL,
                             [SysEndTime] datetime2(0) GENERATED ALWAYS AS ROW END NOT NULL,
                             PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime])
)
WITH (MEMORY_OPTIMIZED = ON, DURABILITY = SCHEMA_AND_DATA, SYSTEM_VERSIONING = ON (HISTORY_TABLE = [dbo].[Inventory_History]));


ALTER TABLE dbo.Inventory_History  
SET (REMOTE_DATA_ARCHIVE = ON (MIGRATION_STATE = OUTBOUND));
GO

---------------------










ALTER AUTHORIZATION ON DATABASE::[AdventureWorks2014] TO [WIN-776Q3S6FO22\cmm]
GO

-- Option #1: Create Temporal Table [dbo].[Department] with automatically named History table:

USE AdventureWorks2014
GO
 
CREATE TABLE dbo.Department 
(
    DepartmentID        int NOT NULL IDENTITY(1,1) PRIMARY KEY CLUSTERED, 
    DepartmentName      varchar(50) NOT NULL, 
    ManagerID           int NULL, 
 
    ValidFrom           datetime2 GENERATED ALWAYS AS ROW START NOT NULL, 
    ValidTo             datetime2 GENERATED ALWAYS AS ROW END   NOT NULL,   
 
    PERIOD FOR SYSTEM_TIME (
        ValidFrom, 
        ValidTo
    )   
)
WITH ( SYSTEM_VERSIONING = ON ); -- No History table name given here
GO

-- Check Metadata

SELECT object_id, temporal_type, temporal_type_desc, history_table_id, name -- Department
FROM SYS.TABLES 
WHERE object_id = OBJECT_ID('dbo.Department', 'U')
GO 
SELECT object_id, temporal_type, temporal_type_desc, history_table_id, name -- MSSQL_TemporalHistoryFor_1397580017
FROM SYS.TABLES 
WHERE object_id = ( 
    SELECT history_table_id 
    FROM SYS.TABLES 
    WHERE object_id = OBJECT_ID('dbo.Department', 'U')
)
GO

-- To DROP both the Tables, first you need to switch OFF the System Versioning on the parent Temporal Table:
ALTER TABLE [dbo].[Department] SET ( SYSTEM_VERSIONING = OFF )
GO
DROP TABLE [dbo].[Department]
GO
DROP TABLE [dbo].[MSSQL_TemporalHistoryFor_935674381]
GO

-- Option #2: Create Temporal Table [dbo].[Department] with a named 
-- History table [dbo].[DepartmentHistory]:

CREATE TABLE dbo.Department 
(
    DepartmentID        int NOT NULL IDENTITY(1,1) PRIMARY KEY CLUSTERED, 
    DepartmentName      varchar(50) NOT NULL, 
    ManagerID           int NULL, 
 
    ValidFrom           datetime2 GENERATED ALWAYS AS ROW START NOT NULL, 
    ValidTo             datetime2 GENERATED ALWAYS AS ROW END   NOT NULL,   
 
    PERIOD FOR SYSTEM_TIME (
        ValidFrom, 
        ValidTo
    )   
)
WITH ( SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.DepartmentHistory) );
GO

SELECT object_id, temporal_type, temporal_type_desc, history_table_id, name
FROM SYS.TABLES 
WHERE object_id = OBJECT_ID('dbo.Department', 'U')
GO
SELECT object_id, temporal_type, temporal_type_desc, history_table_id, name 
FROM SYS.TABLES 
WHERE object_id = OBJECT_ID('dbo.DepartmentHistory', 'U')
GO

-- Final Cleanup, As mentioned above to DROP both the Tables, first you need to switch OFF the System Versioning on the parent Temporal Table by ALTER statement:
ALTER TABLE [dbo].[Department] SET ( SYSTEM_VERSIONING = OFF )
GO
DROP TABLE [dbo].[Department]
GO
DROP TABLE [dbo].[DepartmentHistory]
GO

--Otherwise you will get following error message:

--Msg 13552, Level 16, State 1, Line 82
--Drop table operation failed on table �TestManDB.dbo.Department� because it is not supported operation on system-versioned temporal tables.
 

--�> Limitation of Temporal Tables:

--1. Temporal querying over Linked Server is not supported.

--2. History table cannot have constraints (PK, FK, Table or Column constraints).

--3. INSERT and UPDATE statements cannot reference the SYSTEM_TIME period columns.

--4. TRUNCATE TABLE is not supported while SYSTEM_VERSIONING is ON

--5. Direct modification of the data in a history table is not permitted.

--6. INSTEAD OF triggers are not permitted on either the tables.

--7. Usage of Replication technologies is limited.

--------------
 --Here in this post we will see how we can enable System-Versioning in an existing table containing data. I will also tweak the data to show you the demo on how you can point to a time back in history and get details relative to that time. This will be like Time Travelling to past and seeing record details as if its current data, without much change to the current table�s data-structure.
 
 -- 1. Let�s create a sample table [dbo].[Employee] and populated it by JOINing some tables on [AdventureWorks] Database:

 USE AdventureWorks2014
GO
 
;WITH CTE AS (
    SELECT
        E.BusinessEntityID, P.FirstName, P.LastName, D.Name AS DepartmentName, 
        ROW_NUMBER() OVER(PARTITION BY E.BusinessEntityID ORDER BY D.ModifiedDate DESC) as RN
 
    FROM [AdventureWorks2014].[HumanResources].[Employee] E
    JOIN [AdventureWorks2014].[Person].[Person] P
    ON P.BusinessEntityID = E.BusinessEntityID
    JOIN [AdventureWorks2014].[HumanResources].[EmployeeDepartmentHistory] DH
    ON DH.BusinessEntityID = E.BusinessEntityID
    JOIN [AdventureWorks2014].[HumanResources].[Department] D
    ON D.DepartmentID = DH.DepartmentID
)
SELECT BusinessEntityID, FirstName, LastName, DepartmentName
    INTO dbo.Employee
FROM CTE
WHERE RN = 1
GO

-- (290 row(s) affected)

ALTER TABLE dbo.Employee 
    ADD CONSTRAINT PK_BusinessEntityID PRIMARY KEY (BusinessEntityID)
GO

--3. Now to make [dbo].[Employee] table System Versioned we will add:

-- Two Audit columns of datetime2 datatype to store Start & End datetime.

-- Use PERIOD FOR SYSTEM_TIME clause to associate these two columns as System Time.

ALTER TABLE dbo.Employee ADD
    StartDate datetime2 GENERATED ALWAYS AS ROW START NOT NULL
        DEFAULT CAST('1900-01-01 00:00:00.0000000' AS DATETIME2),
    EndDate   datetime2 GENERATED ALWAYS AS ROW END   NOT NULL
        DEFAULT CAST('9999-12-31 23:59:59.9999999' AS DATETIME2),
PERIOD FOR SYSTEM_TIME (
    StartDate, 
    EndDate
)
GO

-- 4. After all pre-requisites let�s enable the System-Versioning on [dbo].[Employee] table:

ALTER TABLE dbo.Employee 
    SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.EmployeeHistory))
GO

-- �> So, as soon you enable the System-Versioning the SQL Engine creates an another History table with similar schema and nests it under the main Temporal table, let�s check both the tables columns and contents:

SELECT TOP 10 * FROM dbo.Employee
GO
SELECT TOP 10 * FROM dbo.EmployeeHistory
GO

--You can see above the History table is empty as there are no updates on the Parent table. I�ve rounded the 5th row because I will update this row in next step for the demo.

-- 5. Let�s make some updates on the parent Temporal Table (5th row): SQL Engine will automatically populate the History table.

UPDATE dbo.Employee 
SET FirstName = 'Gabriel'
WHERE BusinessEntityID = 5
GO
 
UPDATE dbo.Employee 
SET DepartmentName = 'Research and Development'
WHERE BusinessEntityID = 5
GO
 
UPDATE dbo.Employee 
SET DepartmentName = 'Executive'
WHERE BusinessEntityID = 5
GO
 
-- Let's check the records again:
SELECT * FROM dbo.Employee WHERE BusinessEntityID = 5
GO

--BusinessEntityID	FirstName	LastName	DepartmentName	StartDate	EndDate
--5	Gabriel	Erickson	Executive	2016-09-29 10:00:43.7827772	9999-12-31 23:59:59.9999999

SELECT * FROM dbo.EmployeeHistory WHERE BusinessEntityID = 5
GO

--BusinessEntityID	FirstName	LastName	DepartmentName	StartDate	EndDate
--5	Gail	Erickson	Engineering	1900-01-01 00:00:00.0000000	2016-09-29 10:00:28.8139456
--5	Gabriel	Erickson	Engineering	2016-09-29 10:00:28.8139456	2016-09-29 10:00:33.4081632
--5	Gabriel	Erickson	Research and Development	2016-09-29 10:00:33.4081632	2016-09-29 10:00:43.7827772

-- You will see that after doing 3 UPDATEs on the parent Temporal Table the History table [dbo].[EmployeeHistory] is populated with 3 rows that contains the older versions on data in [dbo].[Employee] table across all columns.

-- 6. Ok, now I�ll do some tweaks on the System Time column values of [dbo].[Employee] table.

--� First of all I will switch OFF the System-Versioning on dbo.Employee table.

--� Now I will update the date of System Time columns, set it to back in history (5-10 days back for an effective demo).

--� Enable back the System-Versioning

ALTER TABLE [dbo].[Employee] SET ( SYSTEM_VERSIONING = OFF )
GO
 
update dbo.EmployeeHistory
set EndDate = '2015-06-01 18:47:07.5566710'
where BusinessEntityID = 5 AND EndDate = '2015-06-09 18:47:07.5566710'
 
update dbo.EmployeeHistory
set StartDate = '2015-06-01 18:47:07.5566710',
    EndDate = '2015-06-05 18:47:28.0153416'
where BusinessEntityID = 5 AND StartDate = '2015-06-09 18:47:07.5566710'
 
update dbo.EmployeeHistory
set StartDate = '2015-06-05 18:47:28.0153416'
where BusinessEntityID = 5 AND StartDate = '2015-06-09 18:47:28.0153416'
GO
 
ALTER TABLE [dbo].[Employee] 
    SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.EmployeeHistory))
GO
 
-- Check the data after datetime changes:
SELECT * FROM dbo.Employee WHERE BusinessEntityID = 5
SELECT * FROM dbo.EmployeeHistory WHERE BusinessEntityID = 5
GO

-- �> Ok, now here comes the most interesting part of the topic i.e. �Time Travel�:

--SQL Server engine provides you FOR SYSTEM_TIME AS OF {datetime_value} option with your SELECT query to get details pointing to any time in the history, let�s check this here:

SELECT * 
FROM dbo.Employee
FOR SYSTEM_TIME AS OF '2015-01-01'
WHERE BusinessEntityID = 5
 
SELECT * 
FROM dbo.Employee
FOR SYSTEM_TIME AS OF '2015-06-03'
WHERE BusinessEntityID = 5
 
SELECT * 
FROM dbo.Employee
FOR SYSTEM_TIME AS OF '2015-06-07'
WHERE BusinessEntityID = 5
 
SELECT * 
FROM dbo.Employee
FOR SYSTEM_TIME AS OF '2015-06-10'
WHERE BusinessEntityID = 5
GO

-- The above four Queries will show you results from the History Table by pulling out the records for the particular date you mentioned by checking the date ranges in the Audit columns:

-- Let�s check the Execution Plan of the 4th SELECT statement:

--So, you can see that you are just querying the dbo.Employee Temporal table, but SQL Engine is internally also querying the dbo.EmployeeHistory table and concatenating (UNION) the rows from both the operators.
 

-- �> Final Cleanup: before Dropping the Temporal & History tables, you need to switch OFF the System Versioning feature in the parent table.


ALTER TABLE [dbo].[Employee] SET ( SYSTEM_VERSIONING = OFF )
GO
DROP TABLE [dbo].[Employee]
GO
DROP TABLE [dbo].[EmployeeHistory]
GO


