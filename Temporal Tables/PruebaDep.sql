CREATE DATABASE BDT
GO
USE BDT
GO
-- https://msdn.microsoft.com/en-us/library/mt590957.aspx



-- Creating a temporal table with an anonymous history table



CREATE TABLE Department   
(    
     DeptID int NOT NULL PRIMARY KEY CLUSTERED  
   , DeptName varchar(50) NOT NULL  
   , ManagerID INT  NULL  
   , ParentDeptID int NULL  
   , SysStartTime datetime2 GENERATED ALWAYS AS ROW START NOT NULL  
   , SysEndTime datetime2 GENERATED ALWAYS AS ROW END NOT NULL  
   , PERIOD FOR SYSTEM_TIME (SysStartTime,SysEndTime)     
)    
WITH (SYSTEM_VERSIONING = ON)   
;  



-- Creating a temporal table with a default history table

CREATE TABLE Department   
(    
     DeptID int NOT NULL PRIMARY KEY CLUSTERED  
   , DeptName varchar(50) NOT NULL  
   , ManagerID INT  NULL  
   , ParentDeptID int NULL  
   , SysStartTime datetime2 GENERATED ALWAYS AS ROW START NOT NULL  
   , SysEndTime datetime2 GENERATED ALWAYS AS ROW END NOT NULL  
   , PERIOD FOR SYSTEM_TIME (SysStartTime, SysEndTime)     
)   
WITH    
   (   
      SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.DepartmentHistory)   
   )   
;  



-- Creating a temporal table with a user-defined history table


CREATE TABLE DepartmentHistory   
(    
     DeptID int NOT NULL  
   , DeptName varchar(50) NOT NULL  
   , ManagerID INT  NULL  
   , ParentDeptID int NULL  
   , SysStartTime datetime2 NOT NULL  
   , SysEndTime datetime2 NOT NULL   
);   
GO   
CREATE CLUSTERED COLUMNSTORE INDEX IX_DepartmentHistory   
   ON DepartmentHistory;   
CREATE NONCLUSTERED INDEX IX_DepartmentHistory_ID_PERIOD_COLUMNS   
   ON DepartmentHistory (SysEndTime, SysStartTime, DeptID);   
GO   
CREATE TABLE Department   
(    
    DeptID int NOT NULL PRIMARY KEY CLUSTERED  
   , DeptName varchar(50) NOT NULL  
   , ManagerID INT  NULL  
   , ParentDeptID int NULL  
   , SysStartTime datetime2 GENERATED ALWAYS AS ROW START NOT NULL  
   , SysEndTime datetime2 GENERATED ALWAYS AS ROW END NOT NULL     
   , PERIOD FOR SYSTEM_TIME (SysStartTime,SysEndTime)      
)    
WITH (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.DepartmentHistory))   
;  


-- Modifying Data in a System-Versioned Temporal Table

https://msdn.microsoft.com/en-us/library/mt591019.aspx



If you specify the column list in your INSERT statement, you can omit the PERIOD columns 
because system will generate values for these columns automatically.
--Insert with column list and without period columns   
INSERT INTO [dbo].[Department] ([DeptID] ,[DeptName] ,[ManagerID] ,[ParentDeptID])   
VALUES(10, 'Marketing', 101, 1);  


If you do specify thePERIOD columns in the column list in your INSERT statement, then 
you need to specify DEFAULT as their value.
INSERT INTO [dbo].[Department] ([DeptID] ,[DeptName] ,[ManagerID] ,[ParentDeptID], 
SysStartTime, SysEndTime)   
VALUES(11, 'Sales', 101, 1, default, default);  


If you do not specify the column list in your INSERT statement, specify DEFAULT for 
PERIOD columns.
--Insert without  column list and DEFAULT values for period columns   
INSERT INTO [dbo].[Department]    
VALUES(12, 'Production', 101, 1, default, default);  



-- Insert data into a table with HIDDEN period columns

CREATE TABLE [dbo].[CompanyLocation]  
(   
     [LocID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY  
   , [LocName] [varchar](50) NOT NULL  
   , [City] [varchar](50) NOT NULL  
   , [SysStartTime] [datetime2](0) GENERATED ALWAYS AS ROW START HIDDEN NOT NULL   
   , [SysEndTime] [datetime2](0) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL   
PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime])   
)    
WITH ( SYSTEM_VERSIONING = ON );   
GO   
INSERT INTO [dbo].[CompanyLocation]   
VALUES  ('Headquarters', 'New York'); 


-- Modifying Data in a System-Versioned Temporal Table

SQL Server 2016 and later
 
Updated: March 28, 2016
Applies To: SQL Server 2016
THIS TOPIC APPLIES TO: yesSQL Server (starting with 2016)yesAzure SQL DatabasenoAzure 
SQL Data Warehouse noParallel Data Warehouse
Data in a system-versioned temporal table is modified using regular DML statements with 
one important difference: period column data cannot be directly modified. When data is 
updated, it is versioned, with the previous version of each updated row is inserted into 
the history table. When data is deleted, the delete is logical, with the row moved into 
the history table from the current table - it is not permanently deleted.
Inserting data
When you insert new data, you need to account for the PERIOD columns if they are not 
HIDDEN. You can also use partition switching with system-versioned temporal tables.
Insert new data with visible period columns
You can construct your INSERT statement when you have visible PERIOD columns as follows 
to account for the new PERIOD columns:
If you specify the column list in your INSERT statement, you can omit the PERIOD columns 
because system will generate values for these columns automatically.
--Insert with column list and without period columns   
INSERT INTO [dbo].[Department] ([DeptID] ,[DeptName] ,[ManagerID] ,[ParentDeptID])   
VALUES(10, 'Marketing', 101, 1);  


If you do specify thePERIOD columns in the column list in your INSERT statement, then 
you need to specify DEFAULT as their value.
INSERT INTO [dbo].[Department] ([DeptID] ,[DeptName] ,[ManagerID] ,[ParentDeptID], 
SysStartTime, SysEndTime)   
VALUES(11, 'Sales', 101, 1, default, default);  


If you do not specify the column list in your INSERT statement, specify DEFAULT for 
PERIOD columns.
--Insert without  column list and DEFAULT values for period columns   
INSERT INTO [dbo].[Department]    
VALUES(12, 'Production', 101, 1, default, default);  


Insert data into a table with HIDDEN period columns
If PERIOD columns are specified as HIDDEN, then you need only to specify the values for 
the visible columns when you use INSERT without specifying the column list. You do not 
need to account for the new PERIOD columns in your INSERT statement. This behavior 
guarantees that your legacy applications will continue to work when you enable system-
versioning on tables that will benefit from versioning.
CREATE TABLE [dbo].[CompanyLocation]  
(   
     [LocID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY  
   , [LocName] [varchar](50) NOT NULL  
   , [City] [varchar](50) NOT NULL  
   , [SysStartTime] [datetime2](0) GENERATED ALWAYS AS ROW START HIDDEN NOT NULL   
   , [SysEndTime] [datetime2](0) GENERATED ALWAYS AS ROW END HIDDEN NOT NULL   
PERIOD FOR SYSTEM_TIME ([SysStartTime], [SysEndTime])   
)    
WITH ( SYSTEM_VERSIONING = ON );   
GO   
INSERT INTO [dbo].[CompanyLocation]   
VALUES  ('Headquarters', 'New York');  
  

Inserting data using PARTITION SWITCH
If the current table is partitioned, you can use partition switch as an efficient 
mechanism to load data into an empty partition or to load into multiple partitions in 
parallel.
The staging table that is used in the PARTITION SWITCH IN statement with a system-
versioned temporal table must have SYSTEM_TIME PERIOD defined, but it does not need to 
be a system-versioned temporal table.
This ensures that temporal consistency checks are performed during the data insert into 
a staging table or when SYSTEM_TIME period is added to a pre-populated staging table.
/*Create staging table with period definition for SWITCH IN temporal table*/   
CREATE TABLE [dbo].[Staging_Department_Partition2]  
(   
     [DeptID] [int] NOT NULL  
   , [DeptName] [varchar](50)  NOT NULL  
   , [ManagerID] [int] NULL  
   , [ParentDeptID] [int] NULL  
   , [SysStartTime] [datetime2](7) GENERATED ALWAYS AS ROW START NOT NULL  
   , [SysEndTime] [datetime2](7) GENERATED ALWAYS AS ROW END NOT NULL  
   , PERIOD FOR SYSTEM_TIME ( [SysStartTime], [SysEndTime] )   
) ON [PRIMARY]   
  
/*Create aligned primary key*/   
ALTER TABLE [dbo].[Staging_Department_Partition2]    
ADD CONSTRAINT [Staging_Department_Partition2_PK]  
   PRIMARY KEY CLUSTERED  
   (  [DeptID] ASC )     
   ON [PRIMARY]   
  
/*   
Create and enforce constraints for partition boundaries.   
Partition 2 contains rows with DeptID > 100 and DeptID <=200   
*/   
ALTER TABLE [dbo].[Staging_Department_Partition2]      
   WITH CHECK ADD  CONSTRAINT [chk_staging_Department_partition_2]     
   CHECK  ([DeptID]>N'100' AND [DeptID]<=N'200')   
ALTER TABLE [dbo].[Staging_Department_Partition2]    
   CHECK CONSTRAINT [chk_staging_Department_partition_2]   
  
/*Load data into staging table*/   
INSERT INTO [dbo].[staging_Department] ([DeptID],[DeptName],[ManagerID],[ParentDeptID])  
 
VALUES (101,'D101',1,NULL)  
  
/*Use PARTITION SWITCH IN to efficiently add data to current table */    
ALTER TABLE [Staging_Department]    
SWITCH TO [dbo].[Department] PARTITION 2;  
  

If you try to perform PARTITION SWITCH from a table without period definition you�ll get 
error message: Msg 13577, Level 16, State 1, Line 25 ALTER TABLE SWITCH statement failed 
on table 'MyDB.dbo.Staging_Department_2015_09_26' because target table has SYSTEM_TIME 
PERIOD while source table does not have it.


Updating data

UPDATE [dbo].[Department] SET [ManagerID] = 501 WHERE [DeptID] = 10  

However, you cannot update a PERIOD column and you cannot update the history table. In 
this example, an attempt to update a PERIOD column generates an error.
UPDATE [dbo].[Department]    
SET SysStartTime = '2015-09-23 23:48:31.2990175'    
WHERE DeptID = 10 ;  
  
Msg 13537, Level 16, State 1, Line 3   
Cannot update GENERATED ALWAYS columns in table 'TmpDev.dbo.Department'.  
  

Updating the current table from the history table
You can use UPDATE on the current table to revert the actual row state to valid state at 
a specific point in time in the past (reverting to a �last good known row version�). The 
following example shows reverting to the values in the history table as of 2015-04-25 
where the DeptID = 10.
UPDATE Department   
SET DeptName = History.DeptName   
FROM Department    
FOR SYSTEM_TIME AS OF '2015-04-25' AS History   
WHERE History.DeptID  = 10   
AND Department.DeptID = 10 ;  
  

Using MERGE to modify data in temporal table
MERGE operation is supported with the same limitations that INSERT and UPDATE statements 
have regarding PERIOD columns.
CREATE TABLE DepartmentStaging (DeptId INT, DeptName varchar(50));   
GO   
INSERT INTO DepartmentStaging VALUES (1, 'Company Management');   
INSERT INTO DepartmentStaging VALUES (10, 'Science & Research');   
INSERT INTO DepartmentStaging VALUES (15, 'Process Management');   
  
MERGE dbo.Department AS target   
USING (SELECT DeptId, DeptName FROM DepartmentStaging) AS source (DeptId, DeptName)   
ON (target.DeptId = source.DeptId)   
WHEN MATCHED THEN    
    UPDATE   
   SET DeptName = source.DeptName   
WHEN NOT MATCHED THEN   
   INSERT (DeptName)   
   VALUES (source.DeptName);  
  



-- https://msdn.microsoft.com/en-us/library/mt591018.aspx

-- Querying Data in a System-Versioned Temporal Table

To perform any type of time-based analysis, use the new FOR SYSTEM_TIME clause with four 
temporal-specific sub-clauses to query data across the current and history tables. For 
more information on these clauses, see Temporal Tables and FROM (Transact-SQL)
AS OF <date_time>
FROM <start_date_time> TO <end_date_time>
BETWEEN <start_date_time> AND <end_date_time>
CONTAINED IN (<start_date_time> , <end_date_time>)
ALL
FOR SYSTEM_TIME can be specified independently for each table in a query. It can be used 
inside common table expressions, table-valued functions and stored procedures.


Query for a specific time using the AS OF sub-clause
Use theAS OF sub-clause when you need to reconstruct state of data as it was at any 
specific time in the past. You can reconstruct the data with the precision of datetime2 
type that was specified in PERIOD column definitions.
TheAS OF sub-clause clause can be used with constant literals or with variables, which 
allows you to dynamically specify time condition. The values provided values are 
interpreted as UTC time.
This first example returns the state of the dbo.Department table AS OF a specific date 
in the past.
/*State of entire table AS OF specific date in the past*/   
SELECT [DeptID], [DeptName], [SysStartTime],[SysEndTime]   
FROM [dbo].[Department]   
FOR SYSTEM_TIME AS OF '2015-09-01 T10:00:00.7230011' ;  
  

This second example compares the values between two points in time for a subset of rows.
DECLARE @ADayAgo datetime2   
SET @ADayAgo = DATEADD (day, -1, sysutcdatetime())   
/*Comparison between two points in time for subset of rows*/   
SELECT D_1_Ago.[DeptID], D.[DeptID],   
D_1_Ago.[DeptName], D.[DeptName],   
D_1_Ago.[SysStartTime], D.[SysStartTime],   
D_1_Ago.[SysEndTime], D.[SysEndTime]   
FROM [dbo].[Department] FOR SYSTEM_TIME AS OF @ADayAgo AS D_1_Ago   
JOIN [Department] AS D ON  D_1_Ago.[DeptID] = [D].[DeptID]    
AND D_1_Ago.[DeptID] BETWEEN 1 and 5 ;  




