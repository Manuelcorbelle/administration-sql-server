--
-- Trabajar con Roles de bases de Datos definidos 
-- por el usuario
--
USE [AdventureWorks2014]
GO

-- Creamos Rol de BD definido por el Usuario

CREATE ROLE Especialistas
GO
-- Creo Usuario de BD sin inicio de sesi�n Veronica
-- desde SSMS
-- From T_SQL
CREATE USER Veronica WITHOUT LOGIN
GO

Select User
GO
REVERT 
GO
EXECUTE AS USER = 'Veronica';
SELECT * from HumanResources.Employee

--Mens. 229, Nivel 14, Estado 5, L�nea 1
--Se deneg� el permiso SELECT en el objeto 'Employee', base de datos 'AdventureWorks2012', esquema 'HumanResources'.

Revert
-- Permitir hacer consultas sobre 'HumanResources.Employee' al 'role' Especialistas
GRANT SELECT ON HumanResources.Employee TO Especialistas ;
GO
-- 'role' de base de datos 'Especialistas' usuario Ver�nica
EXEC sp_addrolemember  'Especialistas', 'Veronica' ;
GO

Select User
EXECUTE AS USER = 'Veronica';
SELECT * from HumanResources.Employee
-- Funciona
REVERT
-- Ejercicio
-- Duplicar Tabla . Borrar Tabla
SELECT * 
INTO HumanResources.Empleados
FROM HumanResources.Employee
GO
REVERT
GO
-- DROP TABLE needs ALTER permission on the schema to which the table belongs, CONTROL permission on the table, or 
-- membership in the db_ddladmin fixed database role.
GRANT ALTER ON SCHEMA::[HumanResources] TO Especialistas 
GO
PRINT USER
GO
-- Como Veronica esta en Rol ESpecialistas puede crear Tablas
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE veronica(id int identity)
GO
--Msg 2760, Level 16, State 1, Line 58
--The specified schema name "dbo" either does not exist or you do not have permission to use it.
REVERT
GO

EXEC sp_addrolemember  'Especialistas', 'Veronica' ;
GO
-- Como Veronica esta en Rol ESpecialistas puede crear Tablas
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE [HumanResources].vero(id int identity)
GO
-- Command(s) completed successfully.
CREATE TABLE [Production].vero_pro(id int identity)
GO
--Msg 2760, Level 16, State 1, Line 77
--The specified schema name "Production" either does not exist or you do not have permission to use it.
DROP TABLE [HumanResources].vero
GO
-- Command(s) completed successfully.
REVERT
GO
REVOKE ALTER ON SCHEMA::[HumanResources] TO Especialistas
GO
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE [HumanResources].vero(id int identity)
GO
--Msg 2760, Level 16, State 1, Line 92
--The specified schema name "HumanResources" either does not exist or you do not have permission to use it.
CREATE TABLE veronica(id int identity)
GO

--Msg 2760, Level 16, State 1, Line 96
--The specified schema name "dbo" either does not exist or you do not have permission to use it.

REVERT
GO
GRANT ALTER ON SCHEMA::dbo TO Veronica 
GO
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE [HumanResources].vero(id int identity)
GO
--Msg 2760, Level 16, State 1, Line 92
--The specified schema name "HumanResources" either does not exist or you do not have permission to use it.
CREATE TABLE veronica(id int identity)
GO
-- Command(s) completed successfully.
-- [dbo].[veronica]

REVERT
GO
-- Creamos 'role' consultores
CREATE ROLE [consultores] ;
GO
-- Permitir hacer inserciones y actualizaciones sobre 'HumanResources.Employee' al 'role' consultores

GRANT INSERT, UPDATE ON HumanResources.Employee TO consultores ;
GO

-- A�adimos al'role' de base de datos 'consultores' el usuario Ver�nica

EXEC sp_addrolemember  'consultores', 'Veronica' ;
GO
ALTER ROLE consultores ADD MEMBER Veronica; 
GO
ALTER ROLE consultores DROP MEMBER Veronica;
GO
DROP ROLE consultores
GO

-- Cambiamos de nombre al 'role' Especialistas por Especialistas_RS

ALTER ROLE Especialistas WITH NAME = Especialistas_RS ;

-- Eliminar usuario Ver�nica del 'role' Especialistas

EXEC sp_droprolemember 'Especialistas_RS', 'Veronica'

-- Eliminar 'role' Especialistas
DROP ROLE Especialistas_RS
GO



-------------------
-- WITH GRANT OPTION and ROLES

USE pubs;

CREATE USER Federico WITHOUT LOGIN;
CREATE USER Maria WITHOUT LOGIN;
GO
CREATE ROLE Editores;
GO
ALTER ROLE Editores ADD MEMBER Federico; -- No funciona en 2008, 2008R2
GO
sp_addrolemember Editores, fred -- Funciona
GO

GRANT SELECT ON dbo.authors TO Editores WITH GRANT OPTION;
-- Comandos completados correctamente.

GO
EXECUTE AS USER = 'federico';
GO
SELECT USER_NAME()
Go
-- this does not work
GRANT SELECT ON dbo.authors TO Maria;

--Mens. 15151, Nivel 16, Estado 1, L�nea 2
--No se puede buscar el objeto 'authors' porque no existe o el usuario no tiene permiso.

-- this works
GRANT SELECT ON dbo.authors TO Maria AS Editores;
GO
-- Comandos completados correctamente.

--When fred issues a GRANT permission, he needs to specify AS contactReaders, the
--name of the group that has the WITH GRANT OPTION option, for the command to work
SELECT * from Authors;
GO
SELECT USER
GO
REVERT;