

-- PARTITIONS
-- Operations SPLIT-MERGE-SWITCH
-- TRUNCATE PARTITION

--Create a database with multiple files and filegroups
--Create a Partition Function and a Partition Scheme based on date
--Create a Table on the Partition
--Insert Data into the Table
--Investigate how the data is stored according to partition
 
USE master
IF EXISTS(select name 
	from sys.databases where name='PartitionTest')
DROP DATABASE PartitionTest
GO
 
 
CREATE DATABASE [PartitionTest]
 ON  PRIMARY
( NAME = N'PartitionTest', FILENAME = N'c:\data\PartitionTest.mdf' , SIZE = 4096KB , FILEGROWTH = 1024KB ), 
FILEGROUP [FGARCHIVE] 
( NAME = N'FGARCHIVE', FILENAME = N'c:\data\FGARCHIVE.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
 FILEGROUP [FG2013] 
( NAME = N'FG2013', FILENAME = N'c:\data\FG2013.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
FILEGROUP [FG2014] 
( NAME = N'FG2014', FILENAME = N'c:\data\FG2014.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
FILEGROUP [FG2015] 
( NAME = N'FG2015', FILENAME = N'c:\data\FG2015.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
FILEGROUP [FG2016] 
( NAME = N'FG2016', FILENAME = N'c:\data\FG2016.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB )
 LOG ON
( NAME = N'PartitionTest_log', FILENAME = N'c:\data\PartitionTest_log.ldf' , SIZE = 1024KB , FILEGROWTH = 10%)
GO
 

USE PartitionTest
 GO
CREATE PARTITION FUNCTION partFn_Date (datetime)
AS RANGE RIGHT FOR VALUES ('2013-01-01','2014-01-01','2015-01-01')
-- AS RANGE RIGHT FOR VALUES ('2013-01-01','2014-01-01','2015-01-01','2016-01-01')
GO
GO
 
CREATE PARTITION SCHEME part_Date
AS PARTITION partFn_Date
TO (FGARCHIVE,FG2013,FG2014,FG2015,FG2016)
GO

-- Partition scheme 'part_Date' has been created successfully. 'FG2015' is marked as the next 
-- used filegroup in partition scheme 'part_Date'.


-- SCHEME part_Date
Create Table Orders
(
OrderID Int Identity (10000,1),
OrderDesc Varchar(50),
OrderDate datetime,
OrderAmount money
)
ON part_Date (OrderDate)
GO
 
---Insert 2012 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-03-01',RAND()*100)
 
---Insert 2013 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-03-01',RAND()*100)
 
---Insert 2014 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-03-01',RAND()*100)
 
---Insert 2015 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-03-01',RAND()*100)
GO



---Insert 2016 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2016-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2016-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2016-03-01',RAND()*100)
GO
---Find rows and corresponding partitions
SELECT *,$Partition.partFn_Date(ORDERDate)
FROM Orders
GO
---find ranges for partitions
select *
from sys.partition_functions f
inner join sys.partition_range_values rv on f.function_id=rv.function_id
where f.name = 'partFn_Date'
GO
----Find number of rows per partition
select p.*
from sys.partitions p
inner join sys.tables t on p.object_id=t.object_id
and t.name = 'orders'
GO 

--partition_id	object_id	index_id	partition_number	hobt_id	rows	filestream_filegroup_id	data_compression	data_compression_desc
--72057594040549376	245575913	0	1	72057594040549376	3	0	0	NONE
--72057594040614912	245575913	0	2	72057594040614912	3	0	0	NONE
--72057594040680448	245575913	0	3	72057594040680448	3	0	0	NONE
--72057594040745984	245575913	0	4	72057594040745984	6	0	0	NONE


----other useful system views with paritioning info
 
select *
from sys.partition_schemes
GO

--------------------------
-- SPLIT - MERGE - SWITCH

--Creating a new Range in our Partition for new data (SPLIT RANGE)
--Merging an older Range of data for archiving (MERGE RANGE)
--Reviewing detailed metadata on partitions
--Removing old Filegroups and Data files no longer needed after archiving
--Using the SWITCH PARTITION command to move data between partitions (Archiving, Moving data from Staging to 
--Production tables in DW environment)



--
--Let us put the 2015 data into a new partition
Use PartitionTest
GO 
-- SPLIT

ALTER PARTITION FUNCTION partFn_Date()
SPLIT RANGE ('2015-01-01');
GO

SELECT *,$Partition.partFn_Date(ORDERDate)
FROM Orders
GO

-- MERGE
--Remove 2013 Partition using a Merge
ALTER PARTITION FUNCTION partFn_Date ()
MERGE RANGE ('2013-01-01');
GO
 
--
--Query to determine table filegroup by index and partition
--Script provided by Jason Strate
--http://www.jasonstrate.com/2013/01/determining-file-group-for-a-table/
--
USE PartitionTest
Go
SELECT OBJECT_SCHEMA_NAME(t.object_id) AS schema_name
,t.name AS table_name
,i.index_id
,i.name AS index_name
,p.partition_number
,fg.name AS filegroup_name
,FORMAT(p.rows, '#,###') AS rows
FROM sys.tables t
INNER JOIN sys.indexes i ON t.object_id = i.object_id
INNER JOIN sys.partitions p ON i.object_id=p.object_id AND i.index_id=p.index_id
LEFT OUTER JOIN sys.partition_schemes ps ON i.data_space_id=ps.data_space_id
LEFT OUTER JOIN sys.destination_data_spaces dds ON ps.data_space_id=dds.partition_scheme_id AND 
p.partition_number=dds.destination_id
INNER JOIN sys.filegroups fg ON COALESCE(dds.data_space_id, i.data_space_id)=fg.data_space_id
 
---Remove the file and filegroup you no longer need
USE master
GO
ALTER DATABASE PartitionTest
REMOVE FILE FG2013
GO
 
-- The file 'FG2013' has been removed.

USE master
GO
ALTER DATABASE PartitionTest
REMOVE FILEGROUP FG2013
GO

-- The filegroup 'FG2013' has been removed.

-- SWITCH
USE PartitionTest
--Switch Data into another table then truncate
Create Table OrdersArchive
(
OrderID Int NOT NULL,
OrderDesc Varchar(50),
OrderDate datetime,
OrderAmount money
)
ON FGArchive
GO
 
ALTER TABLE Orders SWITCH Partition 1 to OrdersArchive
GO

SELECT *
FROM OrdersArchive
GO
SELECT *
FROM Orders
GO
