

-- METADATOS : Tablas / Vistas del Sistema

SELECT * 
FROM sys.databases
GO

sp_configure
GO

-- Procedimientos extendidos
-- By default, xp_cmdshell is disabled in SQL Server for security reasons

-- Activar xp_cmdshell
-- To allow advanced options to be changed.

-- See State xp_cmdshell

SELECT * 
FROM SYS.CONFIGURATIONS 
WHERE Name = 'xp_cmdshell' 
GO

EXEC sp_configure 'show advanced options', 1
GO
-- To update the currently configured value for advanced options.
RECONFIGURE
GO
--  To enable the feature.
EXEC sp_configure 'xp_cmdshell', 1
GO
-- To update the currently configured value for this feature.
RECONFIGURE
GO

--
EXEC master..xp_cmdshell 'dir *.exe'
GO
-- Create Folder
EXEC master..xp_cmdshell 'MD C:\Prueba\'
GO
-- Delete File
EXEC master..xp_cmdshell 'Erase C:\Prueba\Prueba.txt'
GO
-- Delete Folder
EXEC master..xp_cmdshell 'RD C:\Prueba\'
GO
-- Ejecutar desde aqui
DECLARE @cmd Varchar(50),@path varchar(50)
SET @path = 'c:\data\'
SET @path = 'c:\log\'
SET @cmd = 'RD ' + @path + ' /S /Q' -- Delete Files and the Folder IF all files deletions were successful.
EXEC master..xp_cmdshell @cmd
GO

-- 
DECLARE @cmd sysname, @var sysname;
SET @var = 'dir/p';
SET @cmd = @var + ' > dir_out.txt';
EXEC master..xp_cmdshell @cmd;
GO

-- DISABLE
-- Disable xp_cmdshell feature
EXEC sp_configure 'xp_cmdshell', 0
GO
-- To update the currently configured value for this feature.
RECONFIGURE
GO 

-- Note that some configuration options require a server stop and restart to update 
-- the currently running value.
-- " By using �WITH OVERRIDE� you should be able to run successfully.

RECONFIGURE WITH OVERRIDE;
GO

-- Configuration option 'xp_cmdshell' changed from 1 to 0. Run the RECONFIGURE statement to install.

SELECT * 
FROM SYS.CONFIGURATIONS 
WHERE Name = 'xp_cmdshell' 
GO

--configuration_id	name	value	minimum	maximum	value_in_use	description	is_dynamic	is_advanced
--16390	xp_cmdshell	0	0	1	0	Enable or disable command shell	1	1

EXEC master..xp_cmdshell 'dir *.exe'
GO

-- Msg 15281, Level 16, State 1, Procedure xp_cmdshell, Line 64
-- SQL Server blocked access to procedure 'sys.xp_cmdshell' of component 'xp_cmdshell' because this component is turned off as part of the security configuration for this server. A system administrator can enable the use of 'xp_cmdshell' by using sp_configure. For more information about enabling 'xp_cmdshell', search for 'xp_cmdshell' in SQL Server Books Online.



-- Command(s) completed successfully. 


-- Using GUI

--Option 2: Uses the GUI and is simpler to use if you are not familiar with running scripts in SQL, and are running SQL Server 2008 R2, or later, with SQL Server Management Studio. If that is applicable to your scenario, then you may follow these steps instead:

--    Open SQL Server Management Studio
--    Connect to the SQL Instance hosting your GeoCue database.
--    Right-click on your SQL Instance, listed at the top on the left hand side under Object Explorer, and select Facets.
--    From the Facet dropdown at the top of the dialog select Surface Area Configuration.
--    Set XPCmdShellEnabled to True.
--    Then ok to close the Facets dialogue.

---------

 

SELECT name, user_access_desc, is_read_only, state_desc, recovery_model_desc
FROM sys.databases;
GO

-- DATABASEPROPERTYEX
-- https://msdn.microsoft.com/en-us/library/ms186823.aspx


SELECT DATABASEPROPERTYEX('AdventureWorks2014', 'Collation');
GO

SELECT DATABASEPROPERTYEX('AdventureWorks2014', 'Status');
GO

SELECT DATABASEPROPERTYEX('AdventureWorks2014', 'Recovery');
GO
