﻿-- Desde sa

CREATE DATABASE pepe
GO
DROP DATABASE pepe
GO

-- OK

-- Desde pepe

CREATE DATABASE pepe
GO

--Msg 262, Level 14, State 1, Line 1
--CREATE DATABASE permission denied in database 'master'.

-- https://msdn.microsoft.com/es-es/library/ms189751.aspx
-- https://msdn.microsoft.com/en-us/library/ms189828.aspx


/* SEGURIDAD */
-- Syllabus an outline of the subjects in a course of study or teaching.
/* INICIOS DE SESIÓN (LOGIN) */

USE [master]
GO

-- Generado automáticamente


/****** Object:  Login [DESKTOP-66DNHHT\usuario]    Script Date: 1/11/2017 5:33:22 PM ******/
CREATE LOGIN [DESKTOP-66DNHHT\usuario] 
FROM WINDOWS 
WITH DEFAULT_DATABASE=[master], 
DEFAULT_LANGUAGE=[us_english]
GO

ALTER SERVER ROLE [sysadmin] 
ADD MEMBER [DESKTOP-66DNHHT\usuario]
GO


-- Tiene que existir el usuario en Windows para poder crear el 'login'

CREATE LOGIN [JLF_W10JESUS\luis]
	FROM WINDOWS
	WITH DEFAULT_DATABASE = master,
	DEFAULT_LANGUAGE = Spanish ;
GO

--Msg 15401, Level 16, State 1, Line 7
--Windows NT user or group 'JLF_W10JESUS\luis' not found. Check the name again.


-- Msg 15407, Level 16, State 1, Line 1
-- 'luis' is not a valid Windows NT name. Give the complete name: <domain\username>.

USE [master]
GO

/* For security reasons the login is crealuis disabled and with a random password. */
/****** Object:  Login [pepe]    Script Date: 1/11/2017 5:43:04 PM ******/
CREATE LOGIN [pepe] 
WITH PASSWORD=N'NG2HopMWJEByl7YfGYHlHkUz+B7sYgyYGG0LWq21jZQ=', 
DEFAULT_DATABASE=[master], 
DEFAULT_LANGUAGE=[us_english], 
CHECK_EXPIRATION=OFF, 
CHECK_POLICY=OFF
GO

ALTER LOGIN [pepe] DISABLE
GO

/* Creamos un inicio de sesión
For security reasons the login is crealuis disabled and with a random password. */

CREATE LOGIN [luis]
WITH PASSWORD='luis',
DEFAULT_DATABASE=[master],
DEFAULT_LANGUAGE=[Español],
CHECK_EXPIRATION=ON, 
CHECK_POLICY=ON
GO

/* Crear un inicio de session (LOGIN) */

USE master
GO
CREATE LOGIN Veronica
	WITH PASSWORD = 'veronica' ;
GO




REVOKE CONNECT SQL TO Veronica 
GO

GRANT CONNECT SQL TO Veronica ;
GO


/* Eliminar un Login */

DROP LOGIN veronica
GO



-- Primeras Pruebas con Grant - Revoke - deny

/* Concede un permiso de: CONECTARSE A SQL a LUIS */

GRANT CONNECT SQL TO luis ;
GO
GRANT CONNECT SQL TO [DESKTOP-SMUBIJJ\ana];
GO
-- Conectamos con Luis. Funciona

/* Deniégale el permiso de: CONECTARSE A SQL a LUIS */

DENY CONNECT SQL TO luis ;
GO

/* Retírale el permiso de: CONECTARSE A SQL a LUIS */

REVOKE CONNECT SQL TO luis ;
GO

/* Cambiamos la contraseña del LOGIN */

ALTER LOGIN luis 
WITH PASSWORD = 'luis'
GO

/* Habilitar y deshabilitar un Login */

ALTER LOGIN luis
	DISABLE ;
GO

ALTER LOGIN luis
	ENABLE ;
GO


-- ROLES FIJOS DE SERVIDOR
-- Probamos GUI


/* Utilizamos STORED PROCEDURED  y ALTER SERVER*/

/* Asignamos el rol 'sysadmin' al LOGIN */

EXEC sp_addsrvrolemember 'luis', 'sysadmin' ;
GO

ALTER SERVER ROLE diskadmin 
ADD MEMBER luis ;  
GO 
 
-- Comprobar GUI

/* Eliminamos el rol 'sysadmin' al LOGIN Veronica */

EXEC sp_dropsrvrolemember 'luis', 'sysadmin' ;
GO

ALTER SERVER ROLE diskadmin 
DROP MEMBER luis ;  
GO 
/* Ver información sobre roles */

EXEC sp_helpsrvrole ;
GO
---------------------

--Agregar una cuenta de dominio a un rol de servidor
--En el ejemplo siguiente se agrega una cuenta de dominio denominada adventure-works\roberto0 para el rol de servidor definido por el usuario denominado Production.
ALTER SERVER ROLE Production 
ADD MEMBER [adventure-works\roberto0] ;  

--C. Agregar un inicio de sesión de SQL Server a un rol de servidor
--En el ejemplo siguiente se agrega un SQL Server Inicio de sesión denominado luis a la diskadmin rol fijo de servidor.
ALTER SERVER ROLE diskadmin 
ADD MEMBER luis ;  
GO  

--D. Quitar una cuenta de dominio de un rol de servidor
--En el ejemplo siguiente se quita una cuenta de dominio denominada adventure-works\roberto0 desde el rol de servidor definido por el usuario denominado Production.
ALTER SERVER ROLE Production 
DROP MEMBER [adventure-works\roberto0] ;  

--E. Quitar un inicio de sesión de SQL Server de un rol de servidor
--En el ejemplo siguiente se quita la SQL Server Inicio de sesión luis desde el diskadmin rol fijo de servidor.
ALTER SERVER ROLE Production 
DROP MEMBER luis ;  
GO  

--F. Conceder a un inicio de sesión el permiso para agregar inicios de sesión a un rol de servidor definido por el usuario
--En el siguiente ejemplo se permite a luis agregar otros inicio de sesión al rol de servidor definido por el usuario denominado Production.
GRANT ALTER ON SERVER ROLE::Production TO luis ;  
GO  

--G. Para ver la pertenencia al rol
--Para ver la pertenencia al rol, use la rol de servidor (miembros) página SQL Server Management Studio o ejecute la consulta siguiente:
SELECT SRM.role_principal_id, SP.name AS Role_Name,   
SRM.member_principal_id, SP2.name  AS Member_Name  
FROM sys.server_role_members AS SRM  
JOIN sys.server_principals AS SP  
    ON SRM.Role_principal_id = SP.principal_id  
JOIN sys.server_principals AS SP2   
    ON SRM.member_principal_id = SP2.principal_id  
ORDER BY  SP.name,  SP2.name  

---------------------------



 
/* USUARIOS DE BASE DE DATOS */
use AdventureWorks2014
go
CREATE USER [pepe] 
FOR LOGIN [pepe] 
WITH DEFAULT_SCHEMA=[dbo]
GO
-- DATABASE Users
-- GUI

USE [AdventureWorks2014]
GO

/****** Object:  User [ana]    Script Date: 1/20/2017 5:22:50 PM ******/
CREATE USER [ana] 
FOR LOGIN [DESKTOP-66DNHHT\usuario] 
WITH DEFAULT_SCHEMA=[dbo]
GO

/****** Object:  User [anasinlogin]    Script Date: 1/20/2017 5:25:21 PM ******/
CREATE USER [anasinlogin] 
WITHOUT LOGIN 
WITH DEFAULT_SCHEMA=[dbo]
GO

-- DATABASE USERS

-- Starting
USE pubs
GO

-- 
DROP USER juan
GO

CREATE USER juan
WITHOUT LOGIN
GO

IF OBJECT_ID('Autores','U') is  null 
	BEGIN
		SELECT * 
		INTO Autores
		FROM Authors
	END
GO

SELECT * FROM Autores
GO

-- Impersonate : Usar Contexto de Seguridad de otra cuenta

EXECUTE AS USER='juan'
GO

PRINT user_name()
GO

-- juan


SELECT * FROM Autores
GO


--Msg 229, Level 14, State 5, Line 243
--The SELECT permission was denied on the object 'authors', database 'pubs', schema 'dbo'.


DELETE Autores
GO

--Msg 229, Level 14, State 5, Line 289
--The DELETE permission was denied on the object 'Autores', database 'pubs', schema 'dbo'.

DROP TABLE Autores
GO

--Msg 3701, Level 14, State 20, Line 295
--Cannot drop the table 'Autores', because it does not exist or you do not have permission.


REVERT
GO

PRINT user_name()
GO

-- dbo

GRANT SELECT ON OBJECT::dbo.Autores TO juan
GO

-- Impersonate Juan

EXECUTE AS USER='juan'
GO
SELECT * FROM Autores
GO
-- Works

-- But ...
DELETE Autores
GO

--Msg 229, Level 14, State 5, Line 259
--The DELETE permission was denied on the object 'Autores', database 'pubs', schema 'dbo'.

REVERT
GO


/* Damos la propiedad de la tabla autores a juan. Hacemos que juan sea el creador */

GRANT DELETE ON Autores TO Juan
GO

GRANT CONTROL ON Autores TO Juan
GO

EXECUTE AS USER='juan'
GO

DELETE Autores
GO

-- (23 row(s) affected)

DROP TABLE Autores
GO 

-- Command(s) completed successfully.



/* Concedemos permisos de SELECT a juan sobre la tabla autores */

GRANT SELECT ON OBJECT::dbo.Autores TO juan
GO
EXECUTE AS USER='juan'
GO

PRINT user_name()
GO

SELECT * FROM HumanResources.Employee
GO

REVERT
GO
/* Elimino el permiso de SELECT a juan sobre la table autores */

REVOKE SELECT ON OBJECT::dbo.Autores TO juan
GO

-- GRANT Conceder
-- WITH GRANT OPTION
-- Owner to Juan - Juan To ana

/* Concedemos permisos a juan, y hacemos que este pueda otorgar permisos a otros usuarios de la base de datos */

USE Pubs
GO

SELECT * 
INTO Autores
FROM Authors
GO


CREATE USER juan without login
GO

GRANT SELECT ON OBJECT::dbo.Autores TO juan
WITH GRANT OPTION
GO


EXECUTE AS USER='juan'
GO

PRINT user_name()
GO
SELECT * FROM Autores
GO
REVERT
GO
CREATE USER ana without login
GO
EXECUTE AS USER='ana'
GO
PRINT user_name()
GO
SELECT * FROM Autores
GO

--Msg 229, Level 14, State 5, Line 419
--The SELECT permission was denied on the object 'Autores', database 'pubs', schema 'dbo'.

REVERT
EXECUTE AS USER='juan'
GO
PRINT user_name()
GO
GRANT SELECT ON OBJECT::dbo.Autores TO ana
GO
REVERT
EXECUTE AS USER='ana'
GO
PRINT user_name()
GO
SELECT * FROM Autores
GO
REVERT
/* Eliminamos permisos a juan, y por defecto, a los demás usuarios*/
REVOKE SELECT ON AUTORES TO JUAN CASCADE
GO
EXECUTE AS USER='ana'
GO

PRINT user_name()
GO
SELECT * FROM Autores
GO

--Msg 229, Level 14, State 5, Line 458
--The SELECT permission was denied on the object 'Autores', database 'pubs', schema 'dbo'.
----------------------------------


--  SCHEMAS son contenedores de objetos. 
REVERT
GO
USE AdventureWorks2014
GO

-- Exists Schema ???

-- Old block of code
IF EXISTS (SELECT * 
			FROM sys.schemas 
			WHERE name = 'ventas')
DROP SCHEMA ventas
GO
 
-- New block of code in SQL Server 2016
DROP SCHEMA IF EXISTS ventas
GO

-- Add new schema.
CREATE SCHEMA ventas AUTHORIZATION [dbo]
GO




-- Ver los nombres y propietarios de los diferentes 'schemas' 
-- que hay en 'AdventureWorks2014'

SELECT SCHEMA_NAME, SCHEMA_OWNER
FROM AdventureWorks2014.INFORMATION_SCHEMA.SCHEMATA 
GO

-- Mostrar información sobre los esquemas disponibles
SELECT *
FROM sys.schemas ;
GO
-- Mostrar información sobre el esquema de un usuario
SELECT name, default_schema_name
	FROM sys.database_principals
	WHERE type = 'S'
	AND name = 'ana' ;
GO


-- Mostrar el propietario de un 'schema'
--	ejemplo de uso de 'alias'
SELECT s.name, u.name AS OWNER
	FROM sys.schemas s, sys.database_principals u
	WHERE s.principal_id = u.principal_id ;
GO

-- Mostrar todos los objetos que hay en un 'schema'
SELECT o.name, o.schema_id, o.type_desc
	FROM sys.objects o, sys.schemas s
	WHERE o.schema_id = s.schema_id
	AND s.name = 'HumanResources' ;
GO



---------------------------------
USE pubs
GO
CREATE USER juan without login
GO
CREATE SCHEMA Prod AUTHORIZATION juan
GO

/* Creamos una tabla dentro del esquema Prod */

CREATE TABLE Prod.Article (
	ID INT,
	NAME CHAR(32))
GO

/* Eliminamos el esquema */

DROP SCHEMA Prod
GO

---Msg 3729, Level 16, State 1, Line 522
--Cannot drop schema 'Prod' because it is being referenced by object 'Article'.

/* da un error porque tiene un objecto: Mens. 3729, Nivel 16, Estado 1, Línea 1 No se puede drop schema 'Prueba' porque se le hace referencia en el objeto 'Prueba'.*/

DROP TABLE Prod.Article
GO

DROP SCHEMA Prod
GO


-- 
USE AdventureWorks2014
GO

--  Moving objects among schemas
--  GUI


ALTER SCHEMA HumanResources TRANSFER Person.Address
GO

ALTER SCHEMA Person TRANSFER HumanResources.Address
GO

/* Concedemos permisos de consultar, actualizar y borrar */

-- Grant permissions to Ana on HumanResources
-- then deny permissions on HumanResources.Employee

USE AdventureWorks2014
GO
CREATE USER ana WITHOUT LOGIN
GO
DROP USER ana
GO
 
GRANT SELECT,UPDATE,DELETE ON SCHEMA::HumanResources
TO ana
GO

EXECUTE AS USER='ana'
GO

PRINT user_name()
GO

SELECT * FROM HumanResources.Employee
GO

REVERT
GO

-- DENY

DENY SELECT,UPDATE,DELETE ON HumanResources.Employee TO ana
GO

-- Try Out with ana

EXECUTE AS USER='ana'
GO

PRINT user_name()
GO

SELECT * FROM HumanResources.Employee /* este no podemos verlo */
GO

--Msg 229, Level 14, State 5, Line 547
--The SELECT permission was denied on the object 'Employee', database 'AdventureWorks2014', schema 'HumanResources'.



SELECT * FROM HumanResources.JobCandidate /* este podemos verlo */
GO

-- Works

---------------

---------------------------
-- Forum problem

-- DROP User owns SChema


-- Unable to delete a database role, somehow my custom database role had become the owner of the db_datareader role.

-- Error

--Deletion of the role was not possible due to the following error:

--Error: 15138 The database principal owns a schema in the database, and cannot be dropped.

--The solution is straightforward:

--First find out which schema is being owned by the rolename:

REVERT
CREATE SCHEMA ventas
GO

ALTER AUTHORIZATION
	ON SCHEMA::ventas
	TO ana
GO

SELECT name 
FROM sys.schemas
 WHERE principal_id = USER_ID('ana' )
GO
-- ana
GRANT CREATE TABLE TO ana
GO
-- 
CREATE TABLE ventas.[Password](
	[BusinessEntityID] [int] NOT NULL,
	[PasswordHash] [varchar](128) NOT NULL,
	[PasswordSalt] [varchar](10) NOT NULL,
	[rowguid] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Password_BusinessEntityID] PRIMARY KEY CLUSTERED 
(
	[BusinessEntityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

DROP SCHEMA ventas
GO

--Msg 3729, Level 16, State 1, Line 628
--Cannot drop schema 'ventas' because it is being referenced by object 'Password'.

DROP USER ana
GO

--Msg 15138, Level 16, State 1, Line 634
--The database principal owns a schema in the database, and cannot be dropped.

ALTER AUTHORIZATION ON SCHEMA::ventas TO dbo
GO
-- After which you can drop the role


DROP USER ana
GO

-- Same problem with ROLES

DROP SCHEMA ventas 
GO
--Msg 3729, Level 16, State 1, Line 674
--Cannot drop schema 'ventas' because it is being referenced by object 'Password'.
DROP TABLE ventas.[Password]
GO

DROP SCHEMA ventas 
GO
---------------
-- https://www.mssqltips.com/sqlservertip/4402/new-drop-if-exists-syntax-in-sql-server-2016/

-- ROLES

-- ROLES DE BASE DE DATOS DE USUARIO

-- Trabajar con Roles de bases de Datos definidos 
-- por el usuario
--
-- Server roles fixed
sp_helpSrvRole
GO
USE AdventureWorks2014
GO
-- Database Roles
sp_helpRole 
GO
-- Rol Database Fixed

CREATE USER maria without login
GO
ALTER ROLE db_owner ADD member maria
GO
-- GUI
ALTER ROLE db_owner DROP member maria
GO
DROP USER maria
GO
-- Controlando la existencia de Roles

-- Database rol user 

-- New in SQL SERVER 2016

DROP ROLE IF EXISTS Especialistas
GO
CREATE ROLE Especialistas
GO

-- SQL Server 2014
USE AdventureWorks2014
GO
if not exists (select 1 from sys.database_principals 
	where name='Especialistas' and Type = 'R')
begin
	CREATE ROLE Especialistas 
end	

GO

-- Table exists ???
-- SQL Server 2016

DROP TABLE IF EXISTS  HumanResources.Empleado
GO
SELECT *
INTO HumanResources.Empleado
FROM HumanResources.Employee
GO


-- Permissions over Roles

CREATE USER Veronica WITHOUT LOGIN
GO

EXECUTE AS USER = 'Veronica'
GO
SELECT * from HumanResources.Empleado
GO
--Mens. 229, Nivel 14, Estado 5, Línea 1
--Se denegó el permiso SELECT en el objeto 'Employee', base de datos 'AdventureWorks2012', esquema 'HumanResources'.

REVERT
GO
-- Permitir hacer consultas sobre 'HumanResources.Employee' al 'role' Especialistas
GRANT SELECT ON HumanResources.Empleado TO Especialistas ;
GO
-- 'role' de base de datos 'Especialistas' usuario Verónica
EXEC sp_addrolemember  'Especialistas', 'Veronica' ;
GO


sp_helproleMember 'Especialistas'
GO

--DbRole	MemberName	MemberSID
--Especialistas	saul	0x010500000000000903000000E7CC788F283B6B4EBFAC87E02A3F1EFC
--Especialistas	Veronica	0x0105000000000009030000007BB5B45123DFF44496E62C5BA394BB09

SELECT pm.state_desc PermType,
  pm.permission_name PermName, 
  pm.class_desc PermClass, 
  CASE
    WHEN pm.class_desc = 'schema' THEN sc.name
    ELSE OBJECT_NAME(pm.major_id)
  END ObjectName
FROM sys.database_principals pr
  LEFT JOIN sys.database_permissions pm
    ON pr.principal_id = pm.grantee_principal_id
  LEFT JOIN sys.schemas sc
    ON pm.major_id = sc.schema_id
WHERE pr.name = 'Especialistas';
GO

-- Impersonate
EXECUTE AS USER = 'Veronica'
GO

SELECT * from HumanResources.Empleado
-- Funciona
GO
REVERT
GO



GRANT INSERT,DELETE,UPDATE ON HumanResources.Empleado TO Especialistas 
GO
CREATE USER Pepe without login
GO
ALTER ROLE especialistas ADD member pepe
GO
DENY DELETE ON HumanResources.Empleado To pepe
GO
EXECUTE AS USER='pepe'
GO
SELECT * FROM HumanResources.Empleado
GO
-- Works
DELETE HumanResources.Empleado
GO
--Msg 229, Level 14, State 5, Line 823
--The DELETE permission was denied on the object 'Empleado', database 'AdventureWorks2014', schema 'HumanResources'.


----------------------------
-------------------
-- WITH GRANT OPTION and ROLES

USE pubs;
GO
DROP USER If Exists Federico
GO
CREATE USER Federico WITHOUT LOGIN
GO
DROP USER If Exists maria
GO
CREATE USER Maria WITHOUT LOGIN;
GO
DROP ROLE If Exists Editores
GO
CREATE ROLE Editores;
GO
ALTER ROLE Editores ADD MEMBER Federico
GO
--sp_addrolemember Editores, maria
--GO


GRANT SELECT ON dbo.authors TO Editores WITH GRANT OPTION
GO
-- Comandos completados correctamente.

EXECUTE AS USER = 'federico';
GO
SELECT USER_NAME()
Go
-- this does not work
GRANT SELECT ON dbo.authors TO Maria;
GO

--Msg 15151, Level 16, State 1, Line 1473
--Cannot find the object 'authors', because it does not exist or you do not have permission.

-- this works
GRANT SELECT ON dbo.authors TO Maria AS Editores;
GO
-- Comandos completados correctamente.
REVERT

EXECUTE AS USER='maria'
GO
--When maria issues a GRANT permission, he needs to specify AS contactReaders, the
--name of the group that has the WITH GRANT OPTION option, for the command to work
SELECT * from Authors;
GO
SELECT USER
GO
REVERT
GO

--------------------------
-- Column Level Permissions
-- Permisos sobre campos de Tablas


-- Creamos a Pablo
Use [Northwind]
GO
create user Pablo without login
GO
GRANT SELECT ON OBJECT::dbo.Employees (EmployeeId, LastName, Firstname,city) TO Pablo;
Go
sp_help Employees
GO

--Nota:
-- Ir al GUI en Permisos de la Tabla Employees
--  Seleccionar Vigente (Effective)
--	Permisos de Columnas con SELECT seleccionado

print user 
-- Impersonate
EXECUTE AS USER= 'Pablo'
Go
Print User
Go
--Consequently, the first query in the following code snippet will succeed, but the second will
--fail with a permission denied error:

SELECT FirstName, LastName,City
FROM dbo.Employees;
Go

-- Funciona

SELECT FirstName + ' ..............               ' + LastName as Employee
FROM dbo.Employees
ORDER BY LastName, FirstName;
GO
-- Works !!!

SELECT FirstName,LastName, [Title],[Address]
FROM dbo.Employees
Go
--Msg 230, Level 14, State 1, Line 34
--The SELECT permission was denied on the column 'Title' of the object 'Employees', database 'Northwind', schema 'dbo'.
--Msg 230, Level 14, State 1, Line 34
--The SELECT permission was denied on the column 'Address' of the object 'Employees', database 'Northwind', schema 'dbo'.


SELECT FirstName + ' ' + LastName as Employee
FROM dbo.Employees
ORDER BY [HireDate] DESC;
GO

--Mens. 230, Nivel 14, Estado 1, Línea 1
--Se denegó el permiso SELECT en la columna 'HireDate' del objeto 'Employees', base de datos 'Northwind', esquema 'dbo'.

REVERT
GO



-- USING in Order By

USE AdventureWorks2014
GO

CREATE USER juan WITHOUT LOGIN
GO
--To assign permissions to columns, you can use GRANT SELECT ON <object>, and add the
--list of granted columns in parenthesis, as shown in the following example:
GRANT SELECT ON OBJECT::HumanResources.Employee ([BusinessEntityID],[JobTitle],[OrganizationLevel]) TO juan
GO
--This code allows members of the HumanResourceAssistant database role to read only
--four columns of the dbo.Employee table. The columns Birthdate or Salary cannot
--be selected.
--Consequently, the first query in the following code snippet will succeed, but the second will
--fail with a permission denied error:

EXECUTE AS USER = 'juan'
GO
SELECT [BusinessEntityID],[JobTitle]
FROM HumanResources.Employee
ORDER BY [JobTitle]
GO

-- Works

SELECT [BusinessEntityID],[JobTitle]
FROM HumanResources.Employee
ORDER BY [HireDate] DESC;
GO

--Msg 230, Level 14, State 1, Line 969
--The SELECT permission was denied on the column 'HireDate' of the object 'Employee', database 'AdventureWorks2014', schema 'HumanResources'.

REVERT
GO
--In fact, the second query is an attempt to cheat; as a member of HumanResourceAssistant,
--I have no permission to read the Salary column, but could I use it in ORDER BY to get a sense
--of who are the most paid employees? Of course not.
--You can also use the column-level permission to forbid modification of some columns:
GRANT SELECT ON OBJECT::dbo.Employee TO HumanResourceEmployee;
GRANT UPDATE ON OBJECT::dbo.Employee (LastName, Firstname, email) TO
HumanResourceEmployee;
--The HumanResourceEmployee members will be able to view all the employee information,
--and to change their name and e-mails, but they will not be able to change their salary or
--other information.
-----------

--Msg 15199, Level 16, State 1, Line 998
--The current security context cannot be reverted. Please switch to the original database where 'Execute As' was called and try it again.



USE Northwind
GO
REVERT
GO
-- Permisos sobre Columnas y Roles
USE Tempdb
GO
Print user
GO
CREATE TABLE dbo.Employee 
		( EmployeeID INT IDENTITY(1,1), 
		FirstName VARCHAR(20) NOT NULL, 
		MiddleName VARCHAR(20) NULL, 
		SurName VARCHAR(20) NOT NULL, 
		SSN CHAR(9) NOT NULL, 
		Salary INT NOT NULL
		CONSTRAINT PK_Employee PRIMARY KEY (EmployeeID) );
GO


INSERT INTO dbo.Employee (FirstName, MiddleName, SurName, SSN, Salary) 
VALUES ('John', 'Mark', 'Doe', '111223333', 50000); 
INSERT INTO dbo.Employee (FirstName, MiddleName, SurName, SSN, Salary) 
VALUES ('Jane', 'Eyre', 'Doe', '222334444', 65000);
Go
-- Let's go ahead and set up two users and two roles

CREATE ROLE HR_Employee; 
GO 
CREATE ROLE HR_Intern; 
GO 



CREATE USER SalaryPerson WITHOUT LOGIN; 
GO 
ALTER ROLE HR_Employee ADD MEMBER SalaryPerson 
GO
-- Or
EXEC sp_addrolemember @membername = 'SalaryPerson',
 @rolename = 'HR_Employee'; 
GO 


CREATE USER SummerIntern WITHOUT LOGIN; 
GO 
EXEC sp_addrolemember @membername = 'SummerIntern', 
@rolename = 'HR_Intern';
 GO
ALTER ROLE HR_Intern ADD MEMBER SummerIntern
GO




--Grants SELECT permission against the dbo.Employee
--table to HR_Employee role members

GRANT SELECT ON dbo.Employee TO HR_Employee;
GO
--Now, we don't want interns to have this level of permissions. We only want them to
--have access to specific columns. There's a way to do this. Immediately after the
--table name, we can specify the columns we want to grant permission to (or DENY,
--if we needed to do that) within a set of parentheses, like so:

GRANT SELECT ON dbo.Employee (EmployeeID, FirstName, MiddleName,
SurName) TO HR_Intern
GO

--This should work just fine, because HR_Employees can SELECT against
-- the whole table:

EXECUTE AS USER = 'SalaryPerson';
 GO 
 SELECT * FROM dbo.Employee; 
 GO 

-- 1	John	Mark	Doe	111223333	50000
-- 2	Jane	Eyre	Doe	222334444	65000

 REVERT; 
 GO

 --This will fail with a couple of access denied errors, listing the columns 
 -- the user cannot access:

 EXECUTE AS USER = 'SummerIntern'; 
 GO
  SELECT * FROM dbo.Employee
GO

--  Mens. 230, Nivel 14, Estado 1, Línea 1
--Se denegó el permiso SELECT en la columna 'SSN' del objeto 'Employee', base de datos 'TrainingDB', esquema 'dbo'.
--Mens. 230, Nivel 14, Estado 1, Línea 1
--Se denegó el permiso SELECT en la columna 'Salary' del objeto 'Employee', base de datos 'TrainingDB', esquema 'dbo'.

  

 -- This will work, because the columns in the query are accessible to HR_Intern:
 EXECUTE AS USER = 'SummerIntern';
 GO
 SELECT EmployeeID, FirstName, SurName FROM dbo.Employee;
 GO 

--	1	John	Doe
--	2	Jane	Doe

 REVERT 
 GO
--That's how to restrict using column permissions. Incidentally, you can do the same
-- for DENY. Therefore, if a group of users already have access to columns 
--they shouldn't, and you can't rework security in this manner, 
--you could use DENY if you had to, like so:

-- DENY SELECT ON dbo.Employee (SSN, Salary) TO HR_Intern;

-- Since DENY trumps any other permissions, this will effectively block access 
-- to those columns. This should be used as a last resort, obviously, because 
-- the use of DENY is not intuitive. 
--And DENY at the column level is another step removed 
-- from what we're used to when looking at permissions.

ALTER ROLE HR_Intern DROP MEMBER SummerIntern;

EXECUTE AS USER = 'SummerIntern';
GO
SELECT EmployeeID, FirstName, SurName 
FROM dbo.Employee;
GO 
--Mens. 229, Nivel 14, Estado 5, Línea 1
--Se denegó el permiso SELECT en el objeto 'Employee', base de datos 'TrainingDB', esquema 'dbo'.



--------------------
-- Permiso EXECUTE con Procedimientos Almacenados

-- Conceder permiso para ejecución de procedimientos a usuarios 
-- o Roles


USE AdventureWorks2014
GO

-- Creamos Usuario Pepe sin Login
GRANT EXECUTE ON dbo.uspGetEmployeeManagers
	TO pepe
GO
SETUSER 'pepe' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;
GO
-- Funciona
-- Intento ejecutar otro Procedimiento
EXEC [dbo].[uspGetManagerEmployees] 3
GO
--Msg 229, Level 14, State 5, Procedure uspGetManagerEmployees, Line 290
--The EXECUTE permission was denied on the object 'uspGetManagerEmployees', database 'AdventureWorks2014', schema 'dbo'.
SETUSER
Go

REVOKE EXECUTE ON [dbo].[uspGetEmployeeManagers] TO pepe AS [dbo]
GO
SETUSER 'pepe' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;
GO
--Msg 229, Level 14, State 5, Procedure uspGetEmployeeManagers, Line 302
--The EXECUTE permission was denied on the object 'uspGetEmployeeManagers', database 'AdventureWorks2014', schema 'dbo'.

SETUSER
GO

-- Prueba con DENY
GRANT EXECUTE ON dbo.uspGetEmployeeManagers
	TO pepe
GO
SETUSER 'pepe' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;
GO
-- Funciona
SETUSER
Go

DENY EXECUTE ON [dbo].[uspGetEmployeeManagers] TO pepe AS [dbo]
GO
GRANT EXECUTE ON DATABASE::AdventureWorks2014
	TO pepe ;
GO
SETUSER 'pepe' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;
GO
--Msg 229, Level 14, State 5, Procedure uspGetEmployeeManagers, Line 302
--The EXECUTE permission was denied on the object 'uspGetEmployeeManagers', database 'AdventureWorks2014', schema 'dbo'.

SETUSER
GO


-----------------------
-- Otra version
GRANT EXECUTE ON DATABASE::AdventureWorks2014
	TO ana ;
GO

-- Impersonate de otro modo
-- Cambiamos al usuario ana . Otra Forma
SETUSER 'ana' ;
GO
-- Dejamos el usuario 'ana'
SETUSER ;
GO 
-- Fin Impersonate de otro modo

-- Impersonación del usuario ana
EXECUTE AS USER = 'ana' ;
GO
-- Dejamos el usuario ana
REVERT ;
GO

-- Comprobamos que usuario somos
SELECT USER ;
GO

-- Ejecutar Procedimiento uspGetEmployeeManagers 
-- dando Parametro de entrada 3

EXECUTE dbo.uspGetEmployeeManagers 3 ;
-- Funciona
GO 
REVERT
Go


-- Denegamos el permiso de ejecución para el usuario 'Veronica'


DENY EXECUTE ON DATABASE::AdventureWorks2014
	TO ana ;
GO

-- Comprobamos que usuario somos
SELECT USER ;
GO

-- Intentamos ejecutar de nuevo como usuario ana
SETUSER 'ana' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;

--Mens 229, Nivel 14, Estado 5, Procedimiento uspGetEmployeeManagers, Línea 1
--Se denegó el permiso EXECUTE en el objeto 'uspGetEmployeeManagers', base de datos 'AdventureWorks2012', esquema 'dbo'.

SETUSER ;
GO
-- Con Procedimiento uspGetEmployeeManagers
GRANT EXECUTE ON dbo.uspGetEmployeeManagers
	TO ana ;
GO
SETUSER 'ana' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;

--Msg 229, Level 14, State 5, Procedure uspGetEmployeeManagers, Line 273
--The EXECUTE permission was denied on the object 'uspGetEmployeeManagers', database 'AdventureWorks2014', schema 'dbo'.
GO
revert
--Msg 15357, Level 16, State 1, Line 278
--The current security context was set by "SetUser". It cannot be reverted by statement "Revert".
SETUSER
GO

---------------------------
CREATE USER ana WITHOUT LOGIN 
GO
-- Role's Members

ALTER ROLE Especialistas ADD MEMBER ana
GO
SELECT m.name MemberName
FROM sys.database_role_members rm 
  JOIN sys.database_principals r
    ON rm.role_principal_id = r.principal_id
  JOIN sys.database_principals m
    ON rm.member_principal_id = m.principal_id
WHERE r.name = 'Especialistas'; 
GO

GRANT SELECT ON DATABASE::AdventureWorks2014 TO Especialistas
GO
GRANT EXECUTE ON OBJECT::HumanResources.uspUpdateEmployeeHireInfo TO Especialistas 
GO
-- Roles - Permissions

SELECT pm.state_desc PermType,
  pm.permission_name PermName, 
  pm.class_desc PermClass, 
  CASE
    WHEN pm.class_desc = 'schema' THEN sc.name
    ELSE OBJECT_NAME(pm.major_id)
  END ObjectName
FROM sys.database_principals pr
  LEFT JOIN sys.database_permissions pm
    ON pr.principal_id = pm.grantee_principal_id
  LEFT JOIN sys.schemas sc
    ON pm.major_id = sc.schema_id
WHERE pr.name = 'Especialistas';
GO


SELECT * FROM HumanResources.Employee; 
 
SELECT * FROM HumanResources.EmployeePayHistory;
 
SELECT * FROM HumanResources.JobCandidate; 
 
SELECT * FROM Person.Person; 
 
EXEC HumanResources.uspUpdateEmployeeHireInfo 
  5, 'Design Engineer II', '2008-01-06', '2016-07-14', 44.2938, 2, 1;


REVOKE EXECUTE ON OBJECT::HumanResources.uspUpdateEmployeeHireInfo TO Especialistas;
REVOKE SELECT ON SCHEMA::HumanResources TO Especialistas;
REVOKE SELECT ON OBJECT::HumanResources.JobCandidate TO Especialistas;  

DROP ROLE Especialistas
GO

-- If don't exists ...

--Msg 15151, Level 16, State 1, Line 683
--Cannot drop the role 'Especialistas', because it does not exist or you do not have permission.



-- 
CREATE ROLE consultores
GO
-- Añadimos al'role' de base de datos 'consultores' el usuario Verónica
ALTER ROLE consultores ADD MEMBER Veronica; 
GO
ALTER ROLE consultores DROP MEMBER Veronica;
GO
DROP ROLE consultores
GO
-- Error User in Rol
CREATE ROLE consultores
GO
EXEC sp_addrolemember  'consultores', 'Veronica' ;
GO
-- Cambiamos de nombre al 'role' Especialistas por Especialistas_RS
DROP ROLE consultores
GO

--Msg 15144, Level 16, State 1, Line 710
--The role has members. It must be empty before it can be dropped.

ALTER ROLE Consultores DROP MEMBER veronica
GO
DROP ROLE consultores
GO
-- Command(s) completed successfully.

CREATE ROLE consultores
GO
ALTER ROLE Consultores WITH NAME Consultores_RS ;
GO
-- Eliminar usuario Verónica del 'role' Especialistas
DROP ROLE Consultores_RS
GO

---
-- Permissions over Roles

CREATE USER Veronica WITHOUT LOGIN
GO

EXECUTE AS USER = 'Veronica'
GO
SELECT * from HumanResources.Employee
GO
--Mens. 229, Nivel 14, Estado 5, Línea 1
--Se denegó el permiso SELECT en el objeto 'Employee', base de datos 'AdventureWorks2012', esquema 'HumanResources'.

REVERT
GO
-- Permitir hacer consultas sobre 'HumanResources.Employee' al 'role' Especialistas
GRANT SELECT ON HumanResources.Employee TO Especialistas ;
GO
-- 'role' de base de datos 'Especialistas' usuario Verónica
EXEC sp_addrolemember  'Especialistas', 'Veronica' ;
GO

Select User
GO
EXECUTE AS USER = 'Veronica'
GO

SELECT * from HumanResources.Employee
-- Funciona
REVERT
-
GO
-- DROP TABLE needs ALTER permission on the schema to which the table belongs, 
-- CONTROL permission on the table, or 
-- membership in the db_ddladmin fixed database role.
GRANT CREATE TABLE TO Especialistas
GO
--GRANT CONTROL ON SCHEMA::[HumanResources] TO Especialistas 
-- GO
PRINT USER
GO
-- Como Veronica esta en Rol ESpecialistas puede crear Tablas
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE dbo.veronica(id int identity)
GO

--Msg 262, Level 14, State 1, Line 752
--CREATE TABLE permission denied in database 'AdventureWorks2014'.


--Msg 2760, Level 16, State 1, Line 58
--The specified schema name "dbo" either does not exist or you do not have permission to use it.
REVERT
GO

EXEC sp_addrolemember  'Especialistas', 'Veronica' ;
GO
-- Como Veronica esta en Rol ESpecialistas puede crear Tablas
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE [HumanResources].vero(id int identity)
GO
SELECT * FROM [HumanResources].vero
GO

REVERT
GO
DENY CREATE TABLE TO veronica
GO
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE [HumanResources].otra(id int identity)
GO
--Msg 262, Level 14, State 1, Line 786
--CREATE TABLE permission denied in database 'AdventureWorks2014'.

REVERT
GO
CREATE USER saul WITHOUT LOGIN
GO

ALTER ROLE Especialistas ADD MEMBER saul
GO
EXECUTE AS USER = 'saul';
GO
PRINT USER
GO
CREATE TABLE [HumanResources].otra(id int identity)
GO
SELECT * FROM [HumanResources].otra
GO

-- Command(s) completed successfully.
CREATE TABLE [Production].vero_pro(id int identity)
GO
--Msg 2760, Level 16, State 1, Line 77
--The specified schema name "Production" either does not exist or you do not have permission to use it.
DROP TABLE [HumanResources].vero
GO
-- Command(s) completed successfully.
REVERT
GO
REVOKE ALTER ON SCHEMA::[HumanResources] TO Especialistas
GO
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE [HumanResources].vero(id int identity)
GO
--Msg 2760, Level 16, State 1, Line 92
--The specified schema name "HumanResources" either does not exist or you do not have permission to use it.
CREATE TABLE veronica(id int identity)
GO

--Msg 2760, Level 16, State 1, Line 96
--The specified schema name "dbo" either does not exist or you do not have permission to use it.

REVERT
GO
GRANT ALTER ON SCHEMA::dbo TO Veronica 
GO
EXECUTE AS USER = 'Veronica';
GO
PRINT USER
GO
CREATE TABLE [HumanResources].vero(id int identity)
GO
--Msg 2760, Level 16, State 1, Line 92
--The specified schema name "HumanResources" either does not exist or you do not have permission to use it.
CREATE TABLE veronica(id int identity)
GO
-- Command(s) completed successfully.
-- [dbo].[veronica]

REVERT
GO
-- Creamos 'role' consultores
CREATE ROLE [consultores] ;
GO
-- Permitir hacer inserciones y actualizaciones sobre 'HumanResources.Employee' al 'role' consultores

GRANT INSERT, UPDATE ON HumanResources.Employee TO consultores ;
GO


-- GRANT Permissions on schema to Roles
GRANT SELECT, EXECUTE
	ON SCHEMA::[Dbj]
	TO [Especialistas] ;
GO




GRANT EXECUTE ON OBJECT::HumanResources.uspUpdateEmployeeHireInfo TO ana; 

GRANT SELECT ON SCHEMA::HumanResources TO ana;
DENY SELECT ON OBJECT::HumanResources.JobCandidate TO ana;  

-----------------------------

-- Permisos sobre campos de Tablas


-- Creamos a Pablo
Use [Northwind]
GO
GRANT SELECT ON OBJECT::dbo.Employees (EmployeeId, LastName, Firstname,city) TO Pablo;
Go
sp_help Employees
GO

--Nota:
-- Ir al GUI en Permisos de la Tabla Employees
--  Seleccionar Vigente (Effective)
--	Permisos de Columnas con SELECT seleccionado 
-- Impersonate
EXECUTE AS USER= 'Pablo'
Go
Print User
Go
--Consequently, the first query in the following code snippet will succeed, but the second will
--fail with a permission denied error:

SELECT FirstName, LastName,City
FROM dbo.Employees;
Go

-- Funciona

SELECT FirstName + ' ' + LastName as Employee
FROM dbo.Employees
ORDER BY LastName, FirstName;

-- Works !!!

SELECT FirstName,LastName, [Title],[Address]
FROM dbo.Employees
Go
--Msg 230, Level 14, State 1, Line 34
--The SELECT permission was denied on the column 'Title' of the object 'Employees', database 'Northwind', schema 'dbo'.
--Msg 230, Level 14, State 1, Line 34
--The SELECT permission was denied on the column 'Address' of the object 'Employees', database 'Northwind', schema 'dbo'.


SELECT FirstName + ' ' + LastName as Employee
FROM dbo.Employees
ORDER BY [HireDate] DESC;


--Mens. 230, Nivel 14, Estado 1, Línea 1
--Se denegó el permiso SELECT en la columna 'HireDate' del objeto 'Employees', base de datos 'Northwind', esquema 'dbo'.

REVERT
GO


REVERT
-- Otro ejemplo Desarrollado abajo
USE tempdb
GO
DROP ROLE IF EXists HumanResourceAssistant;
GO
CREATE TABLE employees
( employee_id INT identity(1,1) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  first_name VARCHAR(50),
  salary int default 1000
);
GO
INSERT INTO employees
(employee_id, last_name, first_name)
VALUES('Anderson', 'Sarah',1111),
('Anderson', 'Sarah',1278),
('Ander', 'luis',1111),
('Ande', 'arturo',1234),
('And', 'Juan',3333)
GO

INSERT INTO employees
(employee_id, last_name, first_name)
VALUES
(11, 'Johnson', 'Dale');
CREATE ROLE HumanResourceAssistant;
GO
--To assign permissions to columns, you can use GRANT SELECT ON <object>, and add the
--list of granted columns in parenthesis, as shown in the following example:
GRANT SELECT ON OBJECT::dbo.Employee (EmployeeId, LastName, Firstname,email) TO HumanResourceAssistant;
GO
--This code allows members of the HumanResourceAssistant database role to read only
--four columns of the dbo.Employee table. The columns Birthdate or Salary cannot
--be selected.
CREATE USER mario WITHOUT LOGIN
GO
ALTER ROLE HumanResourceAssistanT ADD MEMBER mario
GO 
EXECUTE AS USER = 'mario'
GO

 

-- 1	John	Mark	Doe	111223333	50000
-- 2	Jane	Eyre	Doe	222334444	65000

 REVERT; 
 GO

 --This will fail with a couple of access denied errors, listing the columns 
 -- the user cannot access:

 EXECUTE AS USER = 'SummerIntern'; 
 GO
  SELECT * FROM dbo.Employee
GO

--  Mens. 230, Nivel 14, Estado 1, Línea 1
--Se denegó el permiso SELECT en la columna 'SSN' del objeto 'Employee', base de datos 'TrainingDB', esquema 'dbo'.
--Mens. 230, Nivel 14, Estado 1, Línea 1
--Se denegó el permiso SELECT en la columna 'Salary' del objeto 'Employee', base de datos 'TrainingDB', esquema 'dbo'.

  GO 
  REVERT;
  GO

 -- This will work, because the columns in the query are accessible to HR_Intern:
 EXECUTE AS USER = 'SummerIntern';
 GO
 SELECT EmployeeID, FirstName, SurName FROM dbo.Employee;
 GO 

--	1	John	Doe
--	2	Jane	Doe

 REVERT 
 GO
--That's how to restrict using column permissions. Incidentally, you can do the same
-- for DENY. Therefore, if a group of users already have access to columns 
--they shouldn't, and you can't rework security in this manner, 
--you could use DENY if you had to, like so:

-- DENY SELECT ON dbo.Employee (SSN, Salary) TO HR_Intern;

-- Since DENY trumps any other permissions, this will effectively block access 
-- to those columns. This should be used as a last resort, obviously, because 
-- the use of DENY is not intuitive. 
--And DENY at the column level is another step removed 
-- from what we're used to when looking at permissions.

ALTER ROLE HR_Intern DROP MEMBER SummerIntern;

EXECUTE AS USER = 'SummerIntern';
GO
SELECT EmployeeID, FirstName, SurName 
FROM dbo.Employee;
GO 
--Mens. 229, Nivel 14, Estado 5, Línea 1
--Se denegó el permiso SELECT en el objeto 'Employee', base de datos 'TrainingDB', esquema 'dbo'.


--------------------
-- Permiso EXECUTE con Procedimientos Almacenados

-- Conceder permiso para ejecución de procedimientos a usuarios 
-- o Roles


USE AdventureWorks2014
GO

-- Creamos Usuario Pepe sin Login
GRANT EXECUTE ON dbo.uspGetEmployeeManagers
	TO pepe
GO
SETUSER 'pepe' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;
GO
-- Funciona
-- Intento ejecutar otro Procedimiento
EXEC [dbo].[uspGetManagerEmployees] 3
GO
--Msg 229, Level 14, State 5, Procedure uspGetManagerEmployees, Line 290
--The EXECUTE permission was denied on the object 'uspGetManagerEmployees', database 'AdventureWorks2014', schema 'dbo'.
SETUSER
Go

REVOKE EXECUTE ON [dbo].[uspGetEmployeeManagers] TO pepe AS [dbo]
GO
SETUSER 'pepe' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;
GO
--Msg 229, Level 14, State 5, Procedure uspGetEmployeeManagers, Line 302
--The EXECUTE permission was denied on the object 'uspGetEmployeeManagers', database 'AdventureWorks2014', schema 'dbo'.

SETUSER
GO

-- Prueba con DENY
GRANT EXECUTE ON dbo.uspGetEmployeeManagers
	TO pepe
GO
SETUSER 'pepe' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;
GO
-- Funciona
SETUSER
Go

DENY EXECUTE ON [dbo].[uspGetEmployeeManagers] TO pepe AS [dbo]
GO
GRANT EXECUTE ON DATABASE::AdventureWorks2014
	TO pepe ;
GO
SETUSER 'pepe' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;
GO
--Msg 229, Level 14, State 5, Procedure uspGetEmployeeManagers, Line 302
--The EXECUTE permission was denied on the object 'uspGetEmployeeManagers', database 'AdventureWorks2014', schema 'dbo'.

SETUSER
GO




-- Otra version
GRANT EXECUTE ON DATABASE::AdventureWorks2014
	TO ana ;
GO

-- Impersonate de otro modo
-- Cambiamos al usuario ana . Otra Forma
SETUSER 'ana' ;
GO
-- Dejamos el usuario 'ana'
SETUSER ;
GO 
-- Fin Impersonate de otro modo

-- Impersonación del usuario ana
EXECUTE AS USER = 'ana' ;
GO
-- Dejamos el usuario ana
REVERT ;
GO

-- Comprobamos que usuario somos
SELECT USER ;
GO

-- Ejecutar Procedimiento uspGetEmployeeManagers 
-- dando Parametro de entrada 3

EXECUTE dbo.uspGetEmployeeManagers 3 ;
-- Funciona
GO 
REVERT
Go


-- Denegamos el permiso de ejecución para el usuario 'Veronica'


DENY EXECUTE ON DATABASE::AdventureWorks2014
	TO ana ;
GO

-- Comprobamos que usuario somos
SELECT USER ;
GO

-- Intentamos ejecutar de nuevo como usuario ana
SETUSER 'ana' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;

--Mens 229, Nivel 14, Estado 5, Procedimiento uspGetEmployeeManagers, Línea 1
--Se denegó el permiso EXECUTE en el objeto 'uspGetEmployeeManagers', base de datos 'AdventureWorks2012', esquema 'dbo'.

SETUSER ;
GO
-- Con Procedimiento uspGetEmployeeManagers
GRANT EXECUTE ON dbo.uspGetEmployeeManagers
	TO ana ;
GO
SETUSER 'ana' 
GO
EXECUTE dbo.uspGetEmployeeManagers 3 ;

--Msg 229, Level 14, State 5, Procedure uspGetEmployeeManagers, Line 273
--The EXECUTE permission was denied on the object 'uspGetEmployeeManagers', database 'AdventureWorks2014', schema 'dbo'.
GO
revert
--Msg 15357, Level 16, State 1, Line 278
--The current security context was set by "SetUser". It cannot be reverted by statement "Revert".
SETUSER
GO

------

/* ROLES DE APLICACIÓN */
/* Utilizan una aplicación para conectarse a nuestra base de datos. */

/* Creamos el rol de aplicacion */

USE AdventureWorks2014
GO
DROP ROLE IF Exists AplicacionAlmacen
GO
CREATE APPLICATION ROLE AplicacionAlmacen
	WITH PASSWORD = 'abcd123.',
	DEFAULT_SCHEMA = dbo;
GO

--/* Eliminar el rol */

DROP APPLICATION ROLE AplicacionAlmacen
GO

/* Conceder permisos al rol "SOBRE UNA VISTA: v" */

GRANT SELECT ON Sales.vSalesPersonSalesByFiscalYears TO AplicacionAlmacen
GO

/* Hago una consulta y funciona*/ 

-- WHO ???

PRINT USER
SELECT USER ;
SELECT USER_NAME();
GO

SELECT *
	FROM Sales.vSalesPersonSalesByFiscalYears ;
GO


/* IMPERSONAR */

/* Activamos el rol de aplicación AplicacionAlmacen IMPERSONACIÓN */
-- From Application Active Role

EXEC sp_setapprole 'AplicacionAlmacen', 'abcd123.' ;
GO

PRINT USER_NAME()
GO

-- AplicacionAlmacen


/* Consulto la vista a la que le di permisos select y una sobre la cual no tengo permisos */

SELECT COUNT(*)
	FROM Sales.vSalesPersonSalesByFiscalYears ;
GO
-- 14
SELECT *
	FROM Sales.vSalesPersonSalesByFiscalYears ;
GO



SELECT COUNT(*)
	FROM HumanResources.vJobCandidate ;
GO


--SELMsg 229, Level 14, State 5, Line 1934
--The ECT permission was denied on the object 'vJobCandidate', database 'AdventureWorks2014', schema 'HumanResources'.


SELECT *
	FROM HumanResources.vJobCandidate ;
GO
/* Salgo del rol: Si nos impersonamos, ya no podemos volver atrás con REVERT, debemos cerrar la sesión. */

PRINT USER_NAME()
GO

REVERT
/* ERROR */

--Msg 15357, Level 16, State 1, Line 1946
--The current security context was set by "SetApprole". It cannot be reverted by statement "Revert".

 -- QUIT SESSION !!!



/* Modificar el nombre del rol */

ALTER APPLICATION ROLE AplicacionAlmacen
	WITH NAME = Almacen,
	PASSWORD = 'abcd123.' ;
GO

/* Permisos específicos */
/* FALTA PEGAR ESTA PARTE QUE NO LA HEMOS ACABADO */

--------------

-------------

--create a test database
CREATE DATABASE [SecurityTest]
GO
USE SecurityTest
GO
CREATE TABLE [dbo].[table1](
       [pkcol] [int] IDENTITY(1,1) NOT NULL,
       [col1] [int] NULL,
PRIMARY KEY CLUSTERED ([pkcol])
)
GO
--create test user login
CREATE LOGIN [User1] WITH PASSWORD=N'p@55w0rd'
GO
--create user in test database
CREATE USER [User1] FOR LOGIN [User1] WITH DEFAULT_SCHEMA=[Developer_Schema]
GO
--create role
CREATE ROLE [Developer_Role] AUTHORIZATION [dbo]
GO
--create schema
CREATE SCHEMA [Developer_Schema] AUTHORIZATION [User1]
GO
--apply permissions to schemas
GRANT ALTER ON SCHEMA::[Developer_Schema] TO [Developer_Role]
GO
GRANT CONTROL ON SCHEMA::[Developer_Schema] TO [Developer_Role]
GO
GRANT SELECT ON SCHEMA::[Developer_Schema] TO [Developer_Role]
GO
GRANT DELETE ON SCHEMA::[dbo] TO [Developer_Role]
GO
GRANT INSERT ON SCHEMA::[dbo] TO [Developer_Role]
GO
GRANT SELECT ON SCHEMA::[dbo] TO [Developer_Role]
GO
GRANT UPDATE ON SCHEMA::[dbo] TO [Developer_Role]
GO
GRANT REFERENCES ON SCHEMA::[dbo] TO [Developer_Role]
GO
--ensure role membership is correct
EXEC sp_addrolemember N'Developer_Role ', N'User1'
GO
--allow users to create tables in Developer_Schema
GRANT CREATE TABLE TO [Developer_Role]
GO
--Allow user to connect to database
GRANT CONNECT TO [User1]

------------------

------------------




-------------------------
-- Revocar permisos / privilegios a un usuario
USE AdventureWorks2014
GO

REVOKE SELECT 
	ON OBJECT::Person.Address
	FROM ana ;
GO

-- Revocamos permiso de ejecución de un procedimiento almacenado a un 'role'
REVOKE EXECUTE
	ON OBJECT::HumanResources.uspUpdateEmployeeHireInfo
	FROM Especialistas_RS ;
GO

-- Denegamos permiso para hacer consultas sobre un objeto al usuario 'ana'
DENY SELECT ON OBJECT::Person.Address
	TO ana ;
GO

-- Denegamos permiso para crear certificados al usuario 'ana'
DENY CREATE CERTIFICATE
	TO ana ;
GO

-- Denegamos permiso para crear 'schemas' al usuario 'ana'
DENY CREATE SCHEMA
	TO ana ;
GO

-- Cambiamos al usuario 'ana' y comprobamos que es efectivo el cambio
EXECUTE AS USER = 'ana' ;
SELECT USER_NAME();
GO

-- Intentamos crear un 'schema' como usuario 'ana'
CREATE SCHEMA Marketing ;
GO

-- Volvemos al usuario 'dbo'
REVERT

-- Concedemos permiso para crear 'schemas' al usuario 'ana'
GRANT CREATE SCHEMA
	TO ana ;
GO

-- Cambiamos al usuario 'ana' y comprobamos que es efectivo el cambio
EXECUTE AS USER = 'ana' ;
SELECT USER_NAME();
GO

-- Intentamos crear un 'schema' como usuario 'ana'
CREATE SCHEMA Marketing ;
GO



---------------------------------------
/* ROLES DE SERVIDOR */

/* Crear rol de servidor */

CREATE SERVER ROLE Prueba
GO



/* Borrar un rol de servidor */

DROP SERVER ROLE Pruebas
GO

/* Al querer borrar un rol con usuario da error */

DROP SERVER ROLE Prueba
GO
/* ERROR */

DROP LOGIN ana
GO

DROP SERVER ROLE Prueba
GO

/* Modificar el nombre*/

ALTER SERVER ROLE Prueba WITH NAME = Pruebas
GO

/* Agregamos usuarios al ROL de servidor */

ALTER SERVER ROLE Prueba ADD MEMBER luis
GO

/* También lo hacíamos con stored procedured */

EXEC sp_addsrvrolemember 'ana', 'sysadmin'
GO

/* Eliminamos usuarios de un rol */

ALTER SERVER ROLE Prueba DROP MEMBER luis
GO

/* También lo hacíamos con stored procedured */

EXEC sp_dropsrvrolemember 'ana', 'sysadmin'
GO

/* Ver información sobre roles */

EXEC sp_helpsrvrole
GO

 
/* DOS SCRIPT PARA VER LOS MIEMBROS DE LOS ROLES */

/* Ver miembros del Rol de servidor Prueba */

SELECT SPR.name AS role_name
	SPM.name AS member_name
FROM sys.server_role_member AS SRM
JOIN sys.server_principals AS SPR
	ON SPR.principal_id = SRM.role_principal_id
JOIN sys.server_principals AS SPM
	ON SPM.principal_id = SRM.member_principal_id
WHERE SPR.name = 'Prueba'
GO

/* ROLES DE BASE DE DATOS  */

/* Crear rol de base de datos */

CREATE ROLE Prueba AUTHORIZATION ana
GO

/* Borrar un rol de base de datos */

DROP ROLE Pruebas
GO

/* Modificar el nombre */

ALTER ROLE Prueba WITH NAME = Pruebas
GO

/* Asignamos permisos en un esquema a roles de bases de datos (Grupos de usuarios) */

GRANT SELECT, EXECUTE
	ON SCHEMA::Prueba
	TO public
GO

GRANT SELECT, EXECUTE
	ON SCHEMA::Prueba
	TO public
	WITH GRANT OPTION
GO

/* ESTRUCTURA ALTERNATIVA */
/* Consulta la tabla sys.databases y si existe la base de datos 'PruebaDB' bórrala y creála. En caso contrario, o sea, si no existe, créala */

USE master
GO
IF EXISTS ( SELECT NAME
			FROM sys.databases
			WHERE name = 'PruebaDB' )
	BEGIN
		DROP DATABASE PruebaDB
		CREATE DATABASE PruebaDB
	END
ELSE
	CREATE DATABASE PruebaDB
GO

/* Copia datos de una tabla de otra base de datos a una tabla de mi base de datos
Hago una copia de la tabla [base de datos].[esquema].[tabla] y que la almacene en [base de datos].[esquema].[tabla] */

SELECT *
INTO PruebaDB.RecursosHumanos.Empleado
FROM AdventureWorks2012.HumanResources.Employee
GO

/* Da un error porque el esquema no existe, debemos crearlo antes */

/* Comprobamos */

USE PruebaDB
GO
SELECT *
FROM PruebaDB.RecursosHumanos.Empleado
GO

/* Creo dos usuarios a la base de datos. Uno es un usuario con login. El otro no tiene login */

USE PruebaDB
GO

CREATE USER carlos
	FROM LOGIN [asia16\carlos]
	WITH DEFAULT_SCHEMA = debemos
GO

CREATE USER Veronica WITHOUT LOGIN
GO

/* Información sobre usuarios */

EXEC sp_helpuser 'ana'
GO
EXEC sp_helpuser 'Veronica'
GO

