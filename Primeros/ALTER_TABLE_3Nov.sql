-- http://www.c-sharpcorner.com/UploadFile/f0b2ed/constraints-in-sql-server/
-- https://msdn.microsoft.com/en-us/library/ms176089.aspx

-- Columnas Calculadas
-- [Sales].[SalesOrderHeader][TotalDue]




-- Constraint = restricci�n
--SQL Server contains the following 6 types of constraints:

-- Primary Key
-- Foreign Key
-- Unique Key
-- Check 
-- Default
 
-- Null / Not Null 
-

-- ALTER TABLE

SELECT DB_ID('AdventureWorks2014') AS [Database ID];
GO

------------
USE master
GO
DECLARE @db_id int;
DECLARE @object_id int;
SET @db_id = DB_ID('AdventureWorks2014');
SET @object_id = OBJECT_ID('AdventureWorks2014.Person.Address');
IF @db_id IS NULL 
  BEGIN;
    PRINT 'Invalid database';
  END;
ELSE IF @object_id IS NULL
  BEGIN;
    PRINT 'Invalid object';
  END;
ELSE
  BEGIN;
    SELECT * FROM sys.dm_db_index_operational_stats(@db_id, @object_id, NULL, NULL);
  END;
GO


-------
-- 
IF DB_ID('PruebaConstraints') is not null
	DROP DATABASE PruebaConstraints
GO
CREATE DATABASE PruebaConstraints
GO 
USE PruebaConstraints
GO
--
-- Coches
--
-- Creamos tabla tCoches
CREATE TABLE tCoches (
	matricula CHAR(8) NOT NULL,
	marca VARCHAR(255) NULL,
	modelo VARCHAR(255) NULL,
	color VARCHAR(255) NULL,
	numero_kilometros int NOT NULL DEFAULT 0,
	CONSTRAINT PK_tCoches PRIMARY KEY(matricula) )
GO
--CREATE TABLE tCoches (
--	matricula CHAR(8) PRIMARY KEY NOT NULL,
--	marca VARCHAR(255) NULL,
--	modelo VARCHAR(255) NULL,
--	color VARCHAR(255) NULL,
--	numero_kilometros NUMERIC(14,2) NULL DEFAULT 0)
--GO

-- Informaci�n sobre Tabla
sp_help 'tCoches'
GO

-- Eliminamos tabla tCoches
 DROP TABLE tCoches ;
 Go

 CREATE TABLE tCoches (
	matricula CHAR(8) PRIMARY KEY NOT NULL,
	marca VARCHAR(255) NULL,
	modelo VARCHAR(255) NULL,
	color VARCHAR(255) NULL,
	numero_kilometros int NULL DEFAULT 0)
GO


-- A�adimos nuevo campo "num_plazas" a tabla tCoches


ALTER TABLE tCoches
	ADD num_plazas INTEGER NOT NULL DEFAULT 5 ;
GO

sp_help tCoches
GO


-- Eliminamos campo "num_plazas" de tabla tCoches
--   en caso de que haya una restricci�n sobre el campo,
--   debemos eliminarlo primero
ALTER TABLE tCoches
DROP CONSTRAINT [DF__tCoches__num_pla__145C0A3F]
go
--   luego procedemos a eliminar la columna
ALTER TABLE tcoches
	DROP COLUMN num_plazas ;
GO

--Msg 5074, Level 16, State 1, Line 102
--The object 'DF__tCoches__num_pla__1B0907CE' is dependent on column 'num_plazas'.
--Msg 4922, Level 16, State 9, Line 102
--ALTER TABLE DROP COLUMN num_plazas failed because one or more objects access this column.

-- Creamos tabla tClientes
CREATE TABLE tClientes (
	codigo INTEGER NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	apellidos VARCHAR(255) NULL,
	nif VARCHAR(10) NULL,
	telefono VARCHAR(9) NULL,
	movil VARCHAR(9) NULL );
GO	
-- Definimos campo "codigo" como clave primaria
ALTER TABLE tClientes 
	ADD	CONSTRAINT PK_tClientes PRIMARY KEY(codigo) ;
GO

-- Creamos la tabla tAlquileres
CREATE TABLE tAlquileres (
	codigo INTEGER NOT NULL,
	codigo_cliente INTEGER NOT NULL,
	matricula CHAR(8) NOT NULL,
	alquiler DATETIME NOT NULL,
	devolucion DATETIME NULL );
GO


ALTER TABLE tAlquileres ADD
	CONSTRAINT PK_tAlquileres 
		PRIMARY KEY(codigo),
	CONSTRAINT FK_tClientes 
		FOREIGN KEY(codigo_cliente) 
		REFERENCES tClientes(codigo),
	CONSTRAINT FK_tCoches 
		FOREIGN KEY(matricula) 
		REFERENCES tCoches(matricula) ;
GO

-- Muestra informaci�n sobre tabla "tAlquileres"
sp_help "dbo.tAlquileres"
GO
---------------------------
-- Muestra informaci�n sobre base de datos "AdventureWorks2014"
EXEC sp_helpdb 'AdventureWorks2014'
GO

-- Muestra informaci�n sobre la base de datos "AdventureWorks2014"
sp_helpdb "AdventureWorks2014"
GO
-- System Tables
USE Pubs
GO
-- Consulta 
sp_helpconstraint Authors
GO

Select * from sys.check_constraints
GO

Select * from sys.foreign_keys
GO

SELECT obj_table.NAME      AS TableName, 
       obj_Constraint.NAME AS ConstraintName, 
       columns.NAME        AS ColumnName 
FROM   sys.objects obj_table 
       JOIN sys.objects obj_Constraint 
         ON obj_table.object_id = obj_Constraint.parent_object_id 
       JOIN sys.sysconstraints constraints 
         ON constraints.constid = obj_Constraint.object_id 
       JOIN sys.columns columns 
         ON columns.object_id = obj_table.object_id 
            AND columns.column_id = constraints.colid 
ORDER  BY tablename




--
-- Ejemplos ALTER
--

USE AdventureWorks2014
GO



 -- Examples ALTER TABLE

USE AdventureWorks2014
GO

SELECT * 
INTO Empleados
FROM HumanResources.Employee
GO
SELECT * FROM Empleados
GO
sp_help 'dbo.empleados'
GO

-- ADD

ALTER TABLE Empleados
	-- Add column to an existing table
	ADD Latest_EducationTypeID int NULL ; 
GO
ALTER TABLE Empleados
	-- DROP column to an existing table
	DROP Latest_EducationTypeID  
GO

--Msg 3728, Level 16, State 1, Line 255
--'Latest_EducationTypeID' is not a constraint.
--Msg 3727, Level 16, State 0, Line 255
--Could not drop constraint. See previous errors.

ALTER TABLE Empleados
	-- DROP column to an existing table
	DROP COLUMN Latest_EducationTypeID  
GO

-- MODIFY

ALTER TABLE Empleados
	-- Changing an existing column definition
	-- Make it nullable
	ALTER COLUMN Gender nchar(1) NULL ; 
GO

ALTER TABLE Empleados
	-- Expanded nvarchar(256) to nvarchar(300)
	ALTER COLUMN LoginID nvarchar(300) NOT NULL ;
GO







-- Calculated Column

USE TempDB
GO
CREATE TABLE dbo.Products   
(  
    ProductID int IDENTITY (1,1) NOT NULL  
  , QtyAvailable smallint  
  , UnitPrice money  
  , InventoryValue AS QtyAvailable * UnitPrice  
);  

-- Insert values into the table.  
INSERT  Products (QtyAvailable, UnitPrice)  
VALUES (25, 2.00), 
		(10, 1.5);  
GO
-- Display the rows in the table.  
SELECT ProductID, QtyAvailable, UnitPrice, InventoryValue  
FROM dbo.Products;  
GO
-- Para agregar una nueva columna calculada a una tabla existente

ALTER TABLE dbo.Products 
ADD RetailValue AS (QtyAvailable * UnitPrice * 1.35);
GO
SELECT ProductID, QtyAvailable, UnitPrice, InventoryValue,RetailValue  
FROM dbo.Products;  
GO

-- Para cambiar una columna existente a una columna calculada

ALTER TABLE dbo.Products DROP COLUMN RetailValue;  
GO  
ALTER TABLE dbo.Products ADD RetailValue AS (QtyAvailable * UnitPrice * 1.5);  
GO

SELECT ProductID, QtyAvailable, UnitPrice, InventoryValue,RetailValue  
FROM dbo.Products;  
GO
--------------

Unique Constraint
-- http://www.kodyaz.com/t-sql/add-sql-server-unique-constraint-multiple-columns.aspx

-- http://lgitsmart.com/2014/10/18/sql-server-tip-preventing-duplicate-records-using-the-unique-constraint/

-- Constraint UNIQUE KEY
USE tempdb
GO
DROP TABLE Users
GO
CREATE TABLE Users
 (
   UserID int NOT NULL  IDENTITY(1,1), 
   Name varchar(100) NOT NULL,
   EmailAddress varchar(100) NOT NULL,
   CONSTRAINT AK_UniqueEmail UNIQUE(EmailAddress) 
); 
GO
INSERT Users 
VALUES ('luis','luis@gmail.com'),
	   ('pepe','pepe@gmail.com')
GO
SELECT * FROM Users
GO
INSERT Users 
VALUES ('juan','juan@gmail.com'),
	   ('ana','pepe@gmail.com')
GO
-- No inserta ninguno de los 2
--Msg 2627, Level 14, State 1, Line 31
--Violation of UNIQUE KEY constraint 'AK_UniqueEmail'. Cannot insert duplicate key in object 'dbo.Users'. The duplicate key value is (luis@gmail.com).
--The statement has been terminated.
SELECT * FROM Users
GO
INSERT Users 
VALUES ('juan','juan@gmail.com')
GO
-- Inserta sin problema
INSERT Users 
VALUES ('ana','pepe@gmail.com')
GO
--Msg 2627, Level 14, State 1, Line 51
--Violation of UNIQUE KEY constraint 'AK_UniqueEmail'. Cannot insert duplicate key in object 'dbo.Users'. The duplicate key value is (pepe@gmail.com).
--The statement has been terminated.


-- DISABLE CONSTRAINT UNIQUE KEY
ALTER TABLE Users
NOCHECK CONSTRAINT AK_UniqueEmail
GO

--Msg 11415, Level 16, State 1, Line 63
--Object 'AK_UniqueEmail' cannot be disabled or enabled. This action applies only to foreign key and check constraints.
--Msg 4916, Level 16, State 0, Line 63
--Could not enable or disable the constraint. See previous errors.

-- Delete the unique constraint.
ALTER TABLE dbo.DocExc 
DROP CONSTRAINT AK_UniqueEmail
GO



-- Another Option

ALTER TABLE Users 
ADD CONSTRAINT AK_UniqueEmail UNIQUE (EmailAddress); 
GO

-- https://msdn.microsoft.com/es-es/library/ms190024(v=sql.120).aspx

USE AdventureWorks2014;
GO
CREATE TABLE Production.TransactionHistoryArchive4
 (
   TransactionID int NOT NULL, 
   CONSTRAINT AK_TransactionID UNIQUE(TransactionID) 
); 
GO
ALTER TABLE Person.Password 
ADD CONSTRAINT AK_Password UNIQUE (PasswordHash, PasswordSalt); 
GO


-- Disable and enable Foreign Key and Check Constraints
-- http://zarez.net/?p=551

-- SQL Server: Disable Table Constraints (all or some)
-- http://www.sqlservercurry.com/2011/04/sql-server-disable-table-constraints.html
 
 -- http://www.techonthenet.com/sql_server/foreign_keys/foreign_keys.php

 --------------------------
 - Constraint  CHECK or NOCHECK
CREATE TABLE Nomina 
(id INT NOT NULL,
 name VARCHAR(10) NOT NULL,
 salary MONEY NOT NULL
    CONSTRAINT salary_cap CHECK (salary < 100000)
);

-- Valid inserts
INSERT INTO Nomina VALUES (1,'Joe Brown',65000);
INSERT INTO Nomina VALUES (2,'Mary Smith',75000);

-- This insert violates the constraint.
INSERT INTO Nomina VALUES (3,'Pat Jones',105000);

-- Disable the constraint and try again.
ALTER TABLE Nomina NOCHECK CONSTRAINT salary_cap;
INSERT INTO Nomina VALUES (3,'Pat Jones',105000);

-- Re-enable the constraint and try another insert; this will fail.
ALTER TABLE Nomina CHECK CONSTRAINT salary_cap;
INSERT INTO Nomina VALUES (4,'Eric James',110000) ;
GO

-------------
-----------------------
-- CONSTRAINT CHECK

-- Jobs
--

-- Creamos tabla "jobs"
CREATE TABLE jobs (
	job_id SMALLINT IDENTITY(1,1) PRIMARY KEY CLUSTERED,
	job_desc VARCHAR(50) NOT NULL DEFAULT 'New Positition - title not formalized yet',
	min_lvl TINYINT NOT NULL CHECK(min_lvl>=10),
	max_lvl TINYINT NOT NULL CHECK(max_lvl<=250) );

-- Creamos tabla "publishers"
CREATE TABLE publishers (
	pub_id CHAR(4) NOT NULL 
		CONSTRAINT UPKCL_pub_id PRIMARY KEY CLUSTERED
		CHECK(pub_id IN('1389', '0736', '0877', '1622', '1756')
			OR pub_id LIKE '99[0-9][0-9]'),
	pub_name VARCHAR(40) NULL,
	city VARCHAR(20) NULL,
	state CHAR(2) NULL,
	country VARCHAR(30) NULL DEFAULT ('USA') );
	
-- Creamos tabla "employee"
CREATE TABLE employee (
	emp_id VARCHAR(10)
		CONSTRAINT PK_emp_id PRIMARY KEY NONCLUSTERED
		CONSTRAINT CK_emp_id CHECK (emp_id LIKE '[A-Z][A-Z][A-Z][1-9][0-9][0-9][0-9][0-9][FM]'
			OR emp_id LIKE '[A-Z][A-Z][1-9][0-9][0-9][0-9][0-9][FM]'),
	fname VARCHAR(20) NOT NULL,
	minit CHAR(1) NULL,
	job_id SMALLINT NOT NULL DEFAULT 1 REFERENCES jobs(job_id),
	job_lvl TINYINT DEFAULT 10,
	pub_id CHAR(4) NOT NULL DEFAULT ('9952') REFERENCES publishers(pub_id),
	hire_date DATETIME NOT NULL DEFAULT (getdate()) );
------------------------

DECLARE @myid uniqueidentifier = NEWID();  
SELECT CONVERT(char(255), @myid) AS 'char';  

--------

DECLARE @myVariable AS varchar(40);  
SET @myVariable = 'This string is longer than thirty characters';  
SELECT CAST(@myVariable AS varchar);  
SELECT DATALENGTH(CAST(@myVariable AS varchar)) AS 'VarcharDefaultLength';  
SELECT CONVERT(char, @myVariable);  
SELECT DATALENGTH(CONVERT(char, @myVariable)) AS 'VarcharDefaultLength';  

-----------------
USE AdventureWorks2014;  
GO  
SELECT  BusinessEntityID,   
   SalesYTD,   
   CONVERT (varchar(12),SalesYTD,1) AS MoneyDisplayStyle1,   
   GETDATE() AS CurrentDate,   
   CONVERT(varchar(12), GETDATE(), 3) AS DateDisplayStyle3  
FROM Sales.SalesPerson  
WHERE CAST(SalesYTD AS varchar(20) ) LIKE '1%';  
GO



-----------
--There are two types of computed columns namely persisted and non-persisted. 

--There are some major differences between these two

--1. Non-persisted columns are calculated on the fly (ie when the SELECT query is executed) whereas persisted columns are calculated as soon as data is stored in the table.

--2. Non-persisted columns do not consume any space as they are calculated only when you SELECT the column. Persisted columns consume space for the data

--3. When you SELECT data from these columns Non-persisted columns are slower than Persisted columns



- 


 -- Renaming
USE Pubs
GO
SELECT * 
INTO Autores
FROM Authors
GO
SELECT * FROM Autores
GO
SELECT Name 
from SYS.Tables 
ORDER By Name
GO
EXEC sp_rename 'Autores', 'MisAutores';
GO
SELECT * FROM MisAutores
GO
EXEC sp_rename  'MisAutores', 'Autores';
GO
--
EXEC sp_rename 'Autores.au_lname', 'Apellidos', 'COLUMN';
GO

EXEC sp_rename 'Autores.Apellidos', 'au_lname', 'COLUMN';
GO


----
USE AdventureWorks2014
GO

-- Renames the SalesTerritory table to SalesTerr in the Sales schema
SELECT Name 
from SYS.Tables 
ORDER By Name
GO
EXEC sp_rename 'Sales.SalesTerritory', 'SalesTerr';
GO
-- Caution: Changing any part of an object name could break scripts and stored procedures.

SELECT Name 
from SYS.Tables 
ORDER By Name
GO
EXEC sp_rename 'SalesTerr','Sales.SalesTerritory';
GO

--Msg 15225, Level 11, State 1, Procedure sp_rename, Line 941
--No item by the name of 'SalesTerr' could be found in the current database 'AdventureWorks2014', given that @itemtype was input as '(null)'.


EXEC sp_rename 'Sales.SalesTerr','Sales.SalesTerritory';
GO
-- renames the TerritoryID column in the SalesTerritory table to TerrID.

EXEC sp_rename 'Sales.SalesTerritory.TerritoryID', 'TerrID', 'COLUMN';
GO

-- renames the IX_ProductVendor_VendorID index to IX_VendorID.

EXEC sp_rename N'Purchasing.ProductVendor.IX_ProductVendor_VendorID', N'IX_VendorID', N'INDEX';
GO


-- Examples BOL


-- https://msdn.microsoft.com/es-es/library/ms190273(v=sql.120).aspx

USE TempDB
GO

-- Agregar columnas y restricciones
-- Agrega una columna que permite valores NULL y a la que no se han proporcionado valores mediante una definici�n DEFAULT. En la nueva columna, cada fila tendr� valores NULL.

CREATE TABLE dbo.doc_exa (column_a INT) ;
GO
ALTER TABLE dbo.doc_exa ADD column_b VARCHAR(20) NULL ;
GO

-- Agrega una nueva columna con una restricci�n UNIQUE.

CREATE TABLE dbo.doc_exc (column_a INT) ;
GO
ALTER TABLE dbo.doc_exc ADD column_b VARCHAR(20) NULL 
    CONSTRAINT exb_unique UNIQUE ;
GO
EXEC sp_help doc_exc ;
GO
DROP TABLE dbo.doc_exc ;
GO

-- agrega una restricci�n a una columna existente de la tabla. La columna tiene un valor que infringe la restricci�n. Por tanto, WITH NOCHECK se usa para evitar que la restricci�n se valide en las filas existentes y para poder agregar la restricci�n.

CREATE TABLE dbo.doc_exd ( column_a INT) ;
GO
INSERT INTO dbo.doc_exd VALUES (-1) ;
GO
ALTER TABLE dbo.doc_exd WITH NOCHECK 
ADD CONSTRAINT exd_check CHECK (column_a > 1) ;
GO
EXEC sp_help doc_exd ;
GO
DROP TABLE dbo.doc_exd ;
GO
-

-- se crea una tabla con dos columnas y se inserta un valor en la primera columna, y la otra columna sigue siendo NULL. A continuaci�n se agrega una restricci�n DEFAULT a la segunda columna. Para comprobar que se aplica el valor predeterminado, se inserta otro valor en la primera columna y se consulta la tabla.

CREATE TABLE dbo.doc_exz ( column_a INT, column_b INT) ;
GO
INSERT INTO dbo.doc_exz (column_a)VALUES ( 7 ) ;
GO
ALTER TABLE dbo.doc_exz
ADD CONSTRAINT col_b_def
DEFAULT 50 FOR column_b ;
GO
INSERT INTO dbo.doc_exz (column_a) VALUES ( 10 ) ;
GO
SELECT * FROM dbo.doc_exz ;
GO
DROP TABLE dbo.doc_exz ;
GO

-- agregan varias columnas con restricciones que se definen con la nueva columna. La primera columna nueva tiene una propiedad IDENTITY. Cada fila de la tabla tiene nuevos valores incrementales en la columna de identidad.

CREATE TABLE dbo.doc_exe ( column_a INT CONSTRAINT column_a_un UNIQUE) ;
GO
ALTER TABLE dbo.doc_exe ADD 
-- Add a PRIMARY KEY identity column.
column_b INT IDENTITY
CONSTRAINT column_b_pk PRIMARY KEY, 

-- Add a column that references another column in the same table.
column_c INT NULL  
CONSTRAINT column_c_fk 
REFERENCES doc_exe(column_a),

-- Add a column with a constraint to enforce that 
-- nonnull data is in a valid telephone number format.
column_d VARCHAR(16) NULL 
CONSTRAINT column_d_chk
CHECK 
(column_d LIKE '[0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]' OR
column_d LIKE
'([0-9][0-9][0-9]) [0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]'),
			-- Add a non null column with a default.
column_e DECIMAL(3,3)
CONSTRAINT column_e_default
DEFAULT .081 ;
GO
EXEC sp_help doc_exe ;
GO
DROP TABLE dbo.doc_exe ;
GO

-- agrega una columna que acepta valores NULL con una definici�n DEFAULT y se usa WITH VALUES para proporcionar valores para cada fila existente en la tabla. Si no se utiliza WITH VALUES, cada fila tiene el valor NULL en la nueva columna.
CREATE TABLE dbo.doc_exf ( column_a INT) ;
GO
INSERT INTO dbo.doc_exf VALUES (1) ;
GO
ALTER TABLE dbo.doc_exf 
ADD AddDate smalldatetime NULL
CONSTRAINT AddDateDflt
DEFAULT GETDATE() WITH VALUES ;
GO
DROP TABLE dbo.doc_exf ;
GO

--  crea la restricci�n PRIMARY KEY PK_TransactionHistoryArchive_TransactionID y se establecen las opciones FILLFACTOR, ONLINE y PAD_INDEX. El �ndice cl�ster resultante tendr� el mismo nombre que la restricci�n.

USE AdventureWorks2014;
GO
ALTER TABLE Production.TransactionHistoryArchive WITH NOCHECK 
ADD CONSTRAINT PK_TransactionHistoryArchive_TransactionID PRIMARY KEY CLUSTERED (TransactionID)
WITH (FILLFACTOR = 75, ONLINE = ON, PAD_INDEX = ON);
GO


-- Quitar columnas y restricciones

-- se modifica una tabla para quitar una columna. En el segundo ejemplo se quitan varias columnas.

CREATE TABLE dbo.doc_exb 
    (column_a INT
     ,column_b VARCHAR(20) NULL
     ,column_c datetime
     ,column_d int) ;
GO
-- Remove a single column.
ALTER TABLE dbo.doc_exb DROP COLUMN column_b ;
GO
-- Remove multiple columns.
ALTER TABLE dbo.doc_exb DROP COLUMN column_c, column_d;

-- se quita una restricci�n UNIQUE de una tabla. En el segundo ejemplo se quitan dos restricciones y una sola columna.

CREATE TABLE dbo.doc_exc ( column_a int NOT NULL CONSTRAINT my_constraint UNIQUE) ;
GO

-- Example 1. Remove a single constraint.
ALTER TABLE dbo.doc_exc DROP my_constraint ;
GO

DROP TABLE dbo.doc_exc;
GO

CREATE TABLE dbo.doc_exc ( column_a int  
                          NOT NULL CONSTRAINT my_constraint UNIQUE
                          ,column_b int 
                          NOT NULL CONSTRAINT my_pk_constraint PRIMARY KEY) ;
GO

-- Example 2. Remove two constraints and one column
-- The keyword CONSTRAINT is optional. The keyword COLUMN is required.
ALTER TABLE dbo.doc_exc 
   DROP CONSTRAINT  my_constraint, my_pk_constraint, COLUMN column_b ;
GO

-- se crea la tabla ContactBackup y, a continuaci�n, se modifica la tabla; primero se agrega una restricci�n FOREIGN KEY que hace referencia a la tabla Person.Person y, a continuaci�n, se quita la restricci�n FOREIGN KEY

CREATE TABLE Person.ContactBackup
    (ContactID int) ;
GO

ALTER TABLE Person.ContactBackup
ADD CONSTRAINT FK_ContactBacup_Contact FOREIGN KEY (ContactID)
    REFERENCES Person.Person (BusinessEntityID) ;
GO

ALTER TABLE Person.ContactBackup
DROP CONSTRAINT FK_ContactBacup_Contact ;
GO

DROP TABLE Person.ContactBackup ;

-- Modificar una definici�n de columna

-- se modifica una columna de una tabla de INT a DECIMAL
CREATE TABLE dbo.doc_exy (column_a INT ) ;
GO
INSERT INTO dbo.doc_exy (column_a) VALUES (10) ;
GO
ALTER TABLE dbo.doc_exy ALTER COLUMN column_a DECIMAL (5, 2) ;
GO
DROP TABLE dbo.doc_exy ;
GO

-- En el ejemplo siguiente se aumenta el tama�o de una columna varchar y la precisi�n y la escala de una columna decimal. Dado que las columnas contienen datos, solo se puede aumentar el tama�o de columna. Observe tambi�n que col_a se define en un �ndice �nico. A�n se puede aumentar el tama�o de col_a porque el tipo de datos es varchar y el �ndice no es el resultado de una restricci�n KEY PRIMARY

IF OBJECT_ID ( 'dbo.doc_exy', 'U' ) IS NOT NULL 
    DROP TABLE dbo.doc_exy;
GO
-- Create a two-column table with a unique index on the varchar column.
CREATE TABLE dbo.doc_exy ( col_a varchar(5) UNIQUE NOT NULL, col_b decimal (4,2));
GO
INSERT INTO dbo.doc_exy VALUES ('Test', 99.99);
GO
-- Verify the current column size.
SELECT name, TYPE_NAME(system_type_id), max_length, precision, scale
FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.doc_exy');
GO
-- Increase the size of the varchar column.
ALTER TABLE dbo.doc_exy ALTER COLUMN col_a varchar(25);
GO
-- Increase the scale and precision of the decimal column.
ALTER TABLE dbo.doc_exy ALTER COLUMN col_b decimal (10,4);
GO
-- Insert a new row.
INSERT INTO dbo.doc_exy VALUES ('MyNewColumnSize', 99999.9999) ;
GO
-- Verify the current column size.
SELECT name, TYPE_NAME(system_type_id), max_length, precision, scale
FROM sys.columns WHERE object_id = OBJECT_ID(N'dbo.doc_exy');

-- http://raresql.com/2014/03/16/sql-server-how-to-alter-an-existing-computed-column-in-a-table/

-- http://sqlserverzest.com/2013/09/17/sql-server-how-to-add-more-than-one-column-as-a-primary-key/



-------------------
-- Ejemplo Completo

ALTER TABLE

http://msdn.microsoft.com/es-es/library/ms190273.aspx

USE Coches
GO

--
-- Coches
--

-- Creamos tabla tCoches
CREATE TABLE tCoches (
	matricula CHAR(8) NOT NULL,
	marca VARCHAR(255) NULL,
	modelo VARCHAR(255) NULL,
	color VARCHAR(255) NULL,
	numero_kilometros NUMERIC(14,2) NULL DEFAULT 0,
	CONSTRAINT PK_tCoches PRIMARY KEY(matricula) );

-- Eliminamos tabla tCoches
-- DROP TABLE tCoches ;

-- A�adimos nuevo campo "num_plazas" a tabla tCoches
ALTER TABLE tCoches
	ADD num_plazas INTEGER NULL DEFAULT 5 ;

-- Eliminamos campo "num_plazas" de tabla tCoches
--   en caso de que haya una restricci�n sobre el campo,
--   debemos eliminarlo primero
--ALTER TABLE tCoches
--	DROP CONSTRAINT DF__tCoches__num_pla__07F6335A ;
--   luego procedemos a eliminar la columna
--ALTER TABLE tcoches
--	DROP COLUMN num_plazas ;

-- Creamos tabla tClientes
CREATE TABLE tClientes (
	codigo INTEGER PRIMARY KEY NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	apellidos VARCHAR(255) NULL,
	nif VARCHAR(10) NULL,
	telefono VARCHAR(9) NULL,
	movil VARCHAR(9) NULL );
	
-- Definimos campo "codigo" como clave primaria
ALTER TABLE tClientes ADD
	CONSTRAINT PK_tClientes PRIMARY KEY(codigo) ;

-- Creamos la tabla tAlquileres
CREATE TABLE tAlquileres (
	codigo INTEGER NOT NULL,
	codigo_cliente INTEGER NOT NULL,
	matricula CHAR(8) NOT NULL,
	alquiler DATE NOT NULL,
	devolucion DATETIME NULL );

ALTER TABLE tAlquileres ADD
	CONSTRAINT PK_tAlquileres 
		PRIMARY KEY(codigo),
	CONSTRAINT FK_tClientes 
		FOREIGN KEY(codigo_cliente) 
		REFERENCES tClientes(codigo),
	CONSTRAINT FK_tCoches 
		FOREIGN KEY(matricula) 
		REFERENCES tCoches(matricula) ;
GO

-- Muestra informaci�n sobre tabla "tAlquileres"
execute sp_help "dbo.tAlquileres"
GO

--rename tables


EXEC sp_rename 'tCoches', 'Coches';
GO
-- Precauci�n: al cambiar cualquier parte del nombre de un
-- objeto pueden dejar de ser scripts v�lidos y procedimientos almacenados.
EXEC sp_rename 'Coches', 'tCoches';
GO
EXEC sp_rename 'dbo.Coches.modelo', 'model', 'COLUMN';
GO

--Caution: Changing any part of an object name could break scripts and stored procedures.
-- Muestra informaci�n sobre base de datos "AdventureWorks2008"
EXEC sp_helpdb N'AdventureWorks2008'
GO

-- Muestra informaci�n sobre la base de datos "AdventureWorks2008"
sp_helpdb "AdventureWorks2008"
GO



-- CONSTRAINT CHECK

-- Deshabilitar y Habilitar Restricciones

-- Deshabilitar
ALTER TABLE employee NOCHECK CONSTRAINT CK_emp_id
GO
-- Habilitar
ALTER TABLE employee CHECK CONSTRAINT CK_emp_id
GO


-- Example CHECK ENABLE _ DISABLED
USE tempdb
GO
CREATE TABLE Nomina 
(id INT PRIMARY KEY NOT NULL,
 name VARCHAR(10) NOT NULL,
 salary MONEY NOT NULL
    CONSTRAINT salary_cap CHECK (salary < 100000)
);
GO
-- Valid inserts
INSERT INTO Nomina VALUES (1,'Joe Brown',65000)
GO
INSERT INTO Nomina VALUES (2,'Mary Smith',75000);
GO
-- This insert violates the constraint.
INSERT INTO Nomina VALUES (3,'Pat Jones',105000);
GO

--Msg 547, Level 16, State 0, Line 981
--The INSERT statement conflicted with the CHECK constraint "salary_cap". The conflict occurred in database "tempdb", table "dbo.Nomina", column 'salary'.
--The statement has been terminated.

-- Disable the constraint and try again.
ALTER TABLE Nomina NOCHECK CONSTRAINT salary_cap
GO
INSERT INTO Nomina VALUES (3,'Pat Jones',105000);
GO
-- (1 row(s) affected)

-- Re-enable the constraint and try another insert; this will fail.
ALTER TABLE Nomina CHECK CONSTRAINT salary_cap
go
INSERT INTO Nomina VALUES (4,'Eric James',110000) ;
GO
--Msg 547, Level 16, State 0, Line 998
--The INSERT statement conflicted with the CHECK constraint "salary_cap". The conflict occurred in database "tempdb", table "dbo.Nomina", column 'salary'.
--The statement has been terminated.



-- http://www.techonthenet.com/sql_server/check.php

-- CONSTRAINT CHECK
USE master
GO
SELECT DB_ID('DBCheck') AS [Database ID];
GO
IF DB_ID('DBCheck') is NOT NULL
		DROP DATABASE DBCheck
GO
CREATE DATABASE DBCheck
GO
USE DBCheck
GO

-- Creamos tabla "jobs"
CREATE TABLE jobs (
	job_id SMALLINT IDENTITY(1,1) PRIMARY KEY CLUSTERED,
	job_desc VARCHAR(50) NOT NULL DEFAULT 'New Positition - title not formalized yet',
	min_lvl TINYINT NOT NULL CHECK(min_lvl>=10),
	max_lvl TINYINT NOT NULL CHECK(max_lvl<=250) );
GO
-- Creamos tabla "publishers"
CREATE TABLE publishers (
	pub_id CHAR(4) NOT NULL 
		CONSTRAINT UPKCL_pub_id PRIMARY KEY CLUSTERED
		CHECK(pub_id IN('1389', '0736', '0877', '1622', '1756')
			OR pub_id LIKE '99[0-9][0-9]'),
	pub_name VARCHAR(40) NULL,
	city VARCHAR(20) NULL,
	state CHAR(2) NULL,
	country VARCHAR(30) NULL DEFAULT ('USA') );
GO	
-- Creamos tabla "employee"
CREATE TABLE employee (
	emp_id VARCHAR(10)
		CONSTRAINT PK_emp_id PRIMARY KEY NONCLUSTERED
		CONSTRAINT CK_emp_id CHECK (emp_id LIKE '[A-Z][A-Z][A-Z][1-9][0-9][0-9][0-9][0-9][FM]'
			OR emp_id LIKE '[A-Z][A-Z][1-9][0-9][0-9][0-9][0-9][FM]'),
	fname VARCHAR(20) NOT NULL,
	minit CHAR(1) NULL,
	job_id SMALLINT NOT NULL DEFAULT 1 REFERENCES jobs(job_id),
	job_lvl TINYINT DEFAULT 10,
	pub_id CHAR(4) NOT NULL DEFAULT ('9952') REFERENCES publishers(pub_id),
	hire_date DATE NOT NULL DEFAULT (getdate()) );
GO
--


-- DISABLE ALL CONSTRAINTS
EXEC sp_msforeachtable 'alter table ? nocheck constraint all'
GO
ALTER TABLE Table_name NOCHECK CONSTRAINT ALL
GO

ALTER TABLE Table_name
WITH CHECK CHECK CONSTRAINT ALL
GO


-- System Tables Ver BOL
-- To see if you have untrusted constraints in your database run:

SELECT *
from sys.check_constraints
WHERE is_not_trusted = 1
GO
SELECT *
from sys.foreign_keys
WHERE is_not_trusted = 1
GO
-- To list all Check constraints in a database run:
SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE CONSTRAINT_TYPE = 'CHECK'
GO

-- To see what Check constraints are disabled run:
SELECT name, is_disabled
FROM sys.check_constraints
GO
-- To list all Foreign key constraints in a database run:
SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS
WHERE CONSTRAINT_TYPE = 'FOREIGN KEY'
GO
-- To see what Foreign keys are disabled run:
SELECT name, is_disabled
FROM sys.foreign_keys
GO

-- To Defer = Aplazar
