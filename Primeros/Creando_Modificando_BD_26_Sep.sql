--

USE master;
GO
CREATE DATABASE mytest;
GO
DROP DATABASE mytest
GO
-- System tables / Views   Metadata
-- Verify the database files and sizes
sp_helpdb mytest
GO
-- Verify the database files and sizes  
SELECT name --, size, size*1.0/128 AS [Size in MBs]   
FROM sys.master_files  
WHERE name = 'mytest';  
GO  
USE master
GO
SELECT * 
FROM [sys].[databases]
GO
SELECT * 
FROM [sys].[filegroups]
GO
USE AdventureWorks2014
GO
SELECT  [name],[filegroup_guid]
FROM [sys].[filegroups]
-- WHERE name = '[AdventureWorks2014]'
GO
SELECT name, size, size*1.0/128 AS [Size in MBs] 
FROM sys.database_files
-- WHERE name = '[AdventureWorks2014]'
GO

USE Chinook
Go
SELECT name, size, size*1.0/128 AS [Size in MBs] 
FROM sys.database_files
-- WHERE name = '[AdventureWorks2014]'
GO

SELECT *
FROM sys.procedures
WHERE name = '[Chinook]'
GO
sp_helpdb [AdventureWorks2014]
GO
sp_helpdb [Chinook]
GO
SELECT name
FROM sys.databases
-- WHERE name = '[Chinook]'
GO


--
USE master;
GO
CREATE DATABASE Sales
ON 
( NAME = Sales_dat,
    --FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\DATA\saledat.mdf',
	FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\saledat.mdf',
    SIZE = 10,
    MAXSIZE = 50,
    FILEGROWTH = 5 )
LOG ON
( NAME = Sales_log,
    --FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL12.JLF_MSSQLSERVER\MSSQL\DATA\salelog.ldf',
	FILENAME = 'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\salelog.ldf',
    SIZE = 5MB,
    MAXSIZE = 25MB,
    FILEGROWTH = 5MB ) ;
GO
DROP DATABASE Sales
GO
-- 
SELECT name 
FROM sys.databases 
WHERE name = 'Sales'
GO
-- NULL
SELECT name 
FROM sys.databases 
WHERE name = 'Pubs'
GO
-- Row Name Pubs
-- EXISTS verdadero si la consulata devuelve alguna fila
IF EXISTS (SELECT name 
		FROM sys.databases 
		WHERE name = 'Archive')
   DROP DATABASE Archive -- No se ejecuta
GO
--IF EXISTS (SELECT name 
--		FROM sys.databases 
--		WHERE name = 'Pubs')
--   DROP DATABASE Pubs -- Se ejecuta
--GO
CREATE DATABASE Archive 
ON
PRIMARY  
    (NAME = Arch1,
    FILENAME = 'C:\BD\archdat1.mdf',
    SIZE = 100MB,
    MAXSIZE = 200,
    FILEGROWTH = 20),
    ( NAME = Arch2,
    FILENAME = 'C:\BD\archdat2.ndf',
    SIZE = 100MB,
    MAXSIZE = 200,
    FILEGROWTH = 20),
    ( NAME = Arch3,
    FILENAME = 'C:\BD\archdat3.ndf',
    SIZE = 100MB,
    MAXSIZE = 200,
    FILEGROWTH = 20)
LOG ON 
   (NAME = Archlog1,
    FILENAME = 'C:\BD\archlog1.ldf',
    SIZE = 100MB,
    MAXSIZE = 200,
    FILEGROWTH = 20),
   (NAME = Archlog2,
    FILENAME = 'C:\BD\archlog2.ldf',
    SIZE = 100MB,
    MAXSIZE = 200,
    FILEGROWTH = 20) ;
GO

-- FILEGROUPS

CREATE DATABASE Sales
ON PRIMARY
( NAME = SPri1_dat,
    FILENAME = 'C:\BD\SPri1dat.mdf',
    SIZE = 10,
    MAXSIZE = 50,
    FILEGROWTH = 15% ),
( NAME = SPri2_dat,
    FILENAME = 'C:\BD\SPri2dt.ndf',
    SIZE = 10,
    MAXSIZE = 50,
    FILEGROWTH = 15% ),
FILEGROUP SalesGroup1
( NAME = SGrp1Fi1_dat,
    FILENAME = 'C:\BD\SG1Fi1dt.ndf',
    SIZE = 10,
    MAXSIZE = 50,
    FILEGROWTH = 5 ),
( NAME = SGrp1Fi2_dat,
    FILENAME = 'C:\BD\SG1Fi2dt.ndf',
    SIZE = 10,
    MAXSIZE = 50,
    FILEGROWTH = 5 ),
FILEGROUP SalesGroup2
( NAME = SGrp2Fi1_dat,
    FILENAME = 'C:\BD\SG2Fi1dt.ndf',
    SIZE = 10,
    MAXSIZE = 50,
    FILEGROWTH = 5 ),
( NAME = SGrp2Fi2_dat,
    FILENAME = 'C:\BD\SG2Fi2dt.ndf',
    SIZE = 10,
    MAXSIZE = 50,
    FILEGROWTH = 5 )
LOG ON
( NAME = Sales_log,
    FILENAME = 'C:\BD\salelog.ldf',
    SIZE = 5MB,
    MAXSIZE = 25MB,
    FILEGROWTH = 5MB ) ;
GO

-- ALTER DATABASE
-- https://msdn.microsoft.com/en-us/library/bb522469.aspx

-- Modify 
ALTER DATABASE Sales
Modify Name = Ventas ;
GO
ALTER DATABASE Ventas
Modify Name = Sales ;
GO

-- Warning . Allow increase the size no decrease

ALTER DATABASE Sales 
MODIFY FILE
    (NAME = SPri2_dat,
    SIZE = 20MB);
GO



-- ADD File
ALTER DATABASE Sales
ADD FILE 
(
    NAME = SPri10_dat,
    FILENAME = 'C:\BD\SPri2_dat10.ndf',
    SIZE = 5MB,
    MAXSIZE = 100MB,
    FILEGROWTH = 5MB
);
GO



-- DELETE File
ALTER DATABASE Sales
REMOVE FILE SPri10_dat;
GO



-- FILEGROUP

-- ADD Filegroup

ALTER DATABASE Sales
ADD FILEGROUP FG_Ventas;
GO



-- ADD File/s to FILEGROUP
ALTER DATABASE Sales
ADD FILE 
(
    NAME = test1dat3,
    FILENAME = 'C:\BD\t1dat3.ndf',
    SIZE = 5MB,
    MAXSIZE = 100MB,
    FILEGROWTH = 5MB
),
(
    NAME = test1dat4,
    FILENAME = 'C:\BD\t1dat4.ndf',
    SIZE = 5MB,
    MAXSIZE = 100MB,
    FILEGROWTH = 5MB
)
TO FILEGROUP FG_Ventas;
GO


ALTER DATABASE Sales
ADD LOG FILE 
(
    NAME = test1log2,
    FILENAME = 'C:\BD\test2log.ldf',
    SIZE = 5MB,
    MAXSIZE = 100MB,
    FILEGROWTH = 5MB
),
(
    NAME = test1log3,
    FILENAME = 'C:\BD\test3log.ldf',
    SIZE = 5MB,
    MAXSIZE = 100MB,
    FILEGROWTH = 5MB
);
GO

-- To delete a filegroup, you must first delete the file/s associated 
-- with the filegroup.

ALTER DATABASE Sales 
REMOVE FILE test1dat3
GO

ALTER DATABASE Sales 
REMOVE FILE test1dat4
GO

ALTER DATABASE Sales 
REMOVE FILEGROUP FG_Ventas
GO



-- To MOVE Files
-- You must physically move the file to the new directory before running this example. 
-- Afterward, stop and start the instance of SQL Server or take the AdventureWorks2014 database OFFLINE
--  and then ONLINE to implement the change.

ALTER DATABASE Sales
MODIFY FILE
(
    NAME = Test1dat2,
    FILENAME = 'c:\t1dat2.ndf'
);
GO

-- makes the Test1FG1 filegroup created in example B the default filegroup. Then, the default filegroup is reset to the PRIMARY filegroup. Note that PRIMARY must be delimited by brackets or quotation marks.

ALTER DATABASE Sales 
MODIFY FILEGROUP Test1FG1 DEFAULT;
GO
ALTER DATABASE Sales
MODIFY FILEGROUP [PRIMARY] DEFAULT;
GO

-- Add a FILEGROUP that contains the FILESTREAM clause to the FileStreamPhotoDB database.

--Create and add a FILEGROUP that CONTAINS the FILESTREAM clause to
--the FileStreamPhotoDB database.
ALTER DATABASE FileStreamPhotoDB
ADD FILEGROUP TodaysPhotoShoot
CONTAINS FILESTREAM;
GO

--Add a file for storing database photos to FILEGROUP 
ALTER DATABASE FileStreamPhotoDB
ADD FILE
(
    NAME= 'PhotoShoot1',
    FILENAME = 'C:\Users\Administrator\Pictures\TodaysPhotoShoot.ndf'
)
TO FILEGROUP TodaysPhotoShoot;
GO
-- Create Test DB
CREATE DATABASE [myDB] 
GO
-- Take the Database Offline
ALTER DATABASE [myDB] SET OFFLINE WITH
ROLLBACK IMMEDIATE
GO
-- Take the Database Online
ALTER DATABASE [myDB] SET ONLINE
GO
-- Clean up
DROP DATABASE [myDB] 
GO
-- 
-- ALTER over Properties BD
-- GUI
-- T-SQL

USE [master];
GO
----
ALTER DATABASE [pubs] SET SINGLE_USER WITH ROLLBACK IMMEDIATE;
GO
ALTER DATABASE [pubs] SET MULTI_USER;
GO

----
ALTER DATABASE [pubs] SET OFFLINE;
GO
ALTER DATABASE [pubs] SET ONLINE;
GO
---
ALTER DATABASE [pubs] SET READ_ONLY;
GO
ALTER DATABASE [pubs] SET READ_WRITE;
GO
---



--ALTER AUTHORIZATION 

ALTER AUTHORIZATION ON DATABASE::[AdventureWorks2014] TO [Manuel\manuelcorbelle]
GO
ALTER AUTHORIZATION ON DATABASE::[AdventureWorks2014] TO sa
GO

-- BD Options

-- From GUI
-- From T-SQL

-- System Tables / Views

SELECT name, user_access_desc, is_read_only, state_desc, recovery_model_desc
FROM sys.databases;
GO
SELECT name, user_access_desc, is_read_only, state_desc, recovery_model_desc
FROM sys.databases
WHERE name='Pubs'
GO
SELECT *
FROM sys.databases
WHERE name='Pubs'
GO
-- DATABASEPROPERTYEX
-- https://msdn.microsoft.com/en-us/library/ms186823.aspx


SELECT DATABASEPROPERTYEX('AdventureWorks2014', 'Collation');
GO

SELECT DATABASEPROPERTYEX('AdventureWorks2014', 'Status');
GO

SELECT DATABASEPROPERTYEX('AdventureWorks2014', 'Recovery');
GO

-- 
DROP DATABASE [Sales]
GO

-- ATACH y DETACh

-- Los archivos de registro de datos y transacciones de una base de datos se pueden desasociar y volverse 
-- a adjuntar posteriormente a la misma instancia u otra instancia de SQL Server. Separar y adjuntar una base de datos es �til si desea cambiar la base de datos a otra instancia de SQL Server en el mismo equipo o si desea mover la base de datos.


--------------

-- FILEGROUPS

Use master
Go

-- Script 1: Create DB and add additional file group
-- If DB pre exists then drop it
IF exists (SELECT * FROM sys.databases WHERE NAME = 'BDFileGroup')
	DROP DATABASE BDFileGroup
GO
-- Create new DB
CREATE DATABASE BDFileGroup
GO
-- Add file groups to DB
ALTER DATABASE BDFileGroup ADD FILEGROUP FG1
ALTER DATABASE BDFileGroup ADD FILEGROUP FG2
GO
-- Verify file groups in DB
USE BDFileGroup
GO 
SELECT groupName AS FileGroupName FROM sysfilegroups
GO

-- Script 2: Create tables in file groups
-- tbl1 would be created on primary file group
CREATE Table tbl1 (ID int identity(1,1))
GO
-- tbl2 would be created on FG1
CREATE Table tbl2 (ID int identity(1,1), fname varchar(20))
ON FG1
GO
-- Verify file group of tbl1
sp_help tbl1
GO
-- Verify file group of tbl2
sp_help tbl2
GO

INSERT INTO tbl2 (fname) values ('Atif')
GO

--Msg 622, Level 16, State 3, Line 35
--The filegroup "FG1" has no files assigned to it. 
-- Tables, indexes, text columns, ntext columns, and image columns cannot be populated 
-- on this filegroup until a file is added.


-- Script 3: Add data files to file groups
-- Add data file to FG1
ALTER DATABASE BDFileGroup
ADD FILE (NAME = BDFileGroup_1,FILENAME = 'C:\Databases\BDFileGroup_1.ndf')
TO FILEGROUP FG1
GO
-- Add data file to FG2
ALTER DATABASE BDFileGroup
ADD FILE (NAME = BDFileGroup_2,FILENAME = 'C:\Databases\BDFileGroup_2.ndf')
TO FILEGROUP FG2
GO
-- Verify files in file groups
USE BDFileGroup
GO
sp_helpfile
GO

--Script 4: Set FG1 as default file group
-- Set FG1 as default file group
ALTER DATABASE BDFileGroup MODIFY FILEGROUP FG1 DEFAULT
GO
-- Create a table without specifying file group
Create table table3 (ID TINYINT)
GO
--Verify the file group of table3 is FG1
sp_help table3
GO
-- insert some data to make sure no errors
insert into table3 values (10)
GO

--Script 5: verify default filegroup
USE BDFileGroup
GO
SELECT groupname AS DefaultFileGroup FROM sysfilegroups
WHERE convert(bit, (status & 0x10)) = 1
GO




