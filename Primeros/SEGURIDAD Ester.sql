﻿/* SEGURIDAD */
-- Syllabus an outline of the subjects in a course of study or teaching.
/* INICIOS DE SESIÓN (LOGIN) */


/* Creamos un inicio de sesión
For security reasons the login is created disabled and with a random password. */

CREATE LOGIN [Daniel]
WITH PASSWORD='abc123.',
DEFAULT_DATABASE=[master],
DEFAULT_LANGUAGE=[Español],
CHECK_EXPIRATION=ON, CHECK_POLICY=ON
GO

/* Crear un inicio de session (LOGIN) */

USE master
GO
CREATE LOGIN Veronica
	WITH PASSWORD = 'abc123.' ;
GO

/* Eliminar un Login */

DROP LOGIN luis;
GO

/* Concede un permiso de: CONECTARSE A SQL a LUIS */

GRANT CONNECT SQL TO luis ;
GO
 
/* Deniégale el permiso de: CONECTARSE A SQL a LUIS */

DENY CONNECT SQL TO luis ;
GO

/* Retírale el permiso de: CONECTARSE A SQL a LUIS */

REVOKE CONNECT SQL TO luis ;
GO

/* Cambiamos la contraseña del LOGIN */

ALTER LOGIN Daniel WITH PASSWORD = 'daniel'
GO

/* Habilitar y deshabilitar un Login */

ALTER LOGIN luis
	DISABLE ;
GO

ALTER LOGIN luis
	ENABLE ;
GO

/* Utilizamos STORED PROCEDURED */

/* Asignamos el rol 'sysadmin' al LOGIN Veronica */

EXEC sp_addsrvrolemember 'Veronica', 'sysadmin' ;
GO

/* Eliminamos el rol 'sysadmin' al LOGIN Veronica */

EXEC sp_dropsrvrolemember 'Veronica', 'sysadmin' ;
GO

/* Ver información sobre roles */

EXEC sp_helpsrvrole ;
GO

/* Impersonación: Usar su contexto de seguridad. Actuamos como si fueramos juan. Utilizar sus permisos */

EXECUTE as user = 'juan'
GO

Print user_name()
GO

/* para dejar de ser "juan" */

REVERT
GO


 
/* USUARIOS DE BASE DE DATOS */

/* Damos la propiedad de la tabla autores a juan. Hacemos que juan sea el creador */

ALTER AUTHORIZATION ON OBJECT::dbo.Autores TO juan
GO

/* Concedemos permisos de SELECT a juan sobre la tabla autores */

GRANT SELECT ON OBJECT::dbo.Autores TO juan
GO

/* Elimino el permiso de SELECT a juan sobre la table autores */

REVOKE SELECT ON OBJECT::dbo.Autores TO juan
GO

/* Concedemos permisos a juan, y hacemos que este pueda otorgar permisos a otros usuarios de la base de datos */

GRANT SELECT ON OBJECT::dbo.Autores TO juan
WITH GRANT OPTION
GO

/* Eliminamos permisos a juan, y por defecto, a los demás usuarios*/

REVOKE SELECT ON AUTORES TO JUAN CASCADE
GO

/* Concede el permiso de crear tablas */

GRANT CREATE TABLE TO juan
GO 

/* ESQUEMAS */
/* SCHEMAS son contenedores de objetos. Tienen dos finalidades, una es la organización y la otra los permisos */

/* Creamos un esquema y quien es el propietario del esquema */

USE pubs
GO

CREATE SCHEMA Prod AUTHORIZATION juan
GO

/* Creamos una tabla dentro del esquema Prod */

CREATE TABLE Prod.Article (
	ID INT,
	NAME CHAR(32))
GO

/* Eliminamos el esquema */

DROP SCHEMA Prueba
GO
/* da un error porque tiene un objecto: Mens. 3729, Nivel 16, Estado 1, Línea 1 No se puede drop schema 'Prueba' porque se le hace referencia en el objeto 'Prueba'.*/

DROP TABLE Prueba.Prueba
GO

 /* Concedo y deniego permisos a un esquema con una tabla */

CREATE SCHEMA Prueba AUTHORIZATION juan
	CREATE TABLE Prueba (
		source INT,
		cost INT,
		partnumber INT )
	GRANT SELECT TO ana
	DENY SELECT TO 
GO

/* Concedemos permisos de consultar, actualizar y borrar */

	GRANT SELECT,UPDATE,DELETE
	ON SCHEMA::HumanResources
	TO ana
GO

EXECUTE AS USER='ana'
GO

PRINT user_name()
GO

SELECT * FROM HumanResources.Employee
GO

REVERT
GO

/* Quitar permisos: REVOKE Y DENY no funcionan igual. Quitamos el permiso de SELECT sobre la tabla, pero como le concedí permisos sobre el esquema, las demás tablas las vemos*/

REVOKE SELECT ON HumanResources.Employee TO ana
GO

EXECUTE AS USER='ana'
GO

PRINT user_name()
GO

SELECT * FROM HumanResources.Employee /* este no podemos verlo */
GO

SELECT * FROM HumanResources.JobCandidate /* este podemos verlo */
GO

/* Transferimos objetos entre esquemas, en este caso, la tabla Articulo desde el esquema Prod al nuevo esquema Dbj */

USE AdventureWorks2012
GO

ALTER SCHEMA HumanResources TRANSFER Person.Address
GO

ALTER SCHEMA Person TRANSFER HumanResources.Address
GO

/* Cambiamos la propiedad del esquema: primero se la damos a dbo y luego a ana */

ALTER AUTHORIZATION
	ON SCHEMA::Dbj
	TO dbo
GO

ALTER AUTHORIZATION
	ON SCHEMA::Dbj
	TO ana
GO

/* Actualizamos */

UPDATE HumanResources.JobCandidate
SET [BusinessEntityID]=2
WHERE [JobCandidateID]=1
GO

/* ROLES */
/* ROLES DE SERVIDOR */

/* Crear rol de servidor */

CREATE SERVER ROLE Prueba
GO

DROP

/* Borrar un rol de servidor */

DROP SERVER ROLE Pruebas
GO

/* Al querer borrar un rol con usuario da error */

DROP SERVER ROLE Prueba
GO
/* ERROR */

DROP LOGIN ana
GO

DROP SERVER ROLE Prueba
GO

/* Modificar el nombre*/

ALTER SERVER ROLE Prueba WITH NAME = Pruebas
GO

/* Agregamos usuarios al ROL de servidor */

ALTER SERVER ROLE Prueba ADD MEMBER luis
GO

/* También lo hacíamos con stored procedured */

EXEC sp_addsrvrolemember 'ana', 'sysadmin'
GO

/* Eliminamos usuarios de un rol */

ALTER SERVER ROLE Prueba DROP MEMBER luis
GO

/* También lo hacíamos con stored procedured */

EXEC sp_dropsrvrolemember 'ana', 'sysadmin'
GO

/* Ver información sobre roles */

EXEC sp_helpsrvrole
GO

 
/* DOS SCRIPT PARA VER LOS MIEMBROS DE LOS ROLES */

/* Ver miembros del Rol de servidor Prueba */

SELECT SPR.name AS role_name
	SPM.name AS member_name
FROM sys.server_role_member AS SRM
JOIN sys.server_principals AS SPR
	ON SPR.principal_id = SRM.role_principal_id
JOIN sys.server_principals AS SPM
	ON SPM.principal_id = SRM.member_principal_id
WHERE SPR.name = 'Prueba'
GO

/* ROLES DE BASE DE DATOS  */

/* Crear rol de base de datos */

CREATE ROLE Prueba AUTHORIZATION ana
GO

/* Borrar un rol de base de datos */

DROP ROLE Pruebas
GO

/* Modificar el nombre */

ALTER ROLE Prueba WITH NAME = Pruebas
GO

/* Asignamos permisos en un esquema a roles de bases de datos (Grupos de usuarios) */

GRANT SELECT, EXECUTE
	ON SCHEMA::Prueba
	TO public
GO

GRANT SELECT, EXECUTE
	ON SCHEMA::Prueba
	TO public
	WITH GRANT OPTION
GO

/* ESTRUCTURA ALTERNATIVA */
/* Consulta la tabla sys.databases y si existe la base de datos 'PruebaDB' bórrala y creála. En caso contrario, o sea, si no existe, créala */

USE master
GO
IF EXISTS ( SELECT NAME
			FROM sys.databases
			WHERE name = 'PruebaDB' )
	BEGIN
		DROP DATABASE PruebaDB
		CREATE DATABASE PruebaDB
	END
ELSE
	CREATE DATABASE PruebaDB
GO

/* Copia datos de una tabla de otra base de datos a una tabla de mi base de datos
Hago una copia de la tabla [base de datos].[esquema].[tabla] y que la almacene en [base de datos].[esquema].[tabla] */

SELECT *
INTO PruebaDB.RecursosHumanos.Empleado
FROM AdventureWorks2012.HumanResources.Employee
GO

/* Da un error porque el esquema no existe, debemos crearlo antes */

/* Comprobamos */

USE PruebaDB
GO
SELECT *
FROM PruebaDB.RecursosHumanos.Empleado
GO

/* Creo dos usuarios a la base de datos. Uno es un usuario con login. El otro no tiene login */

USE PruebaDB
GO

CREATE USER carlos
	FROM LOGIN [asia16\carlos]
	WITH DEFAULT_SCHEMA = debemos
GO

CREATE USER Veronica WITHOUT LOGIN
GO

/* Información sobre usuarios */

EXEC sp_helpuser 'ana'
GO
EXEC sp_helpuser 'Veronica'
GO

/* ROLES DE APLICACIÓN */
/* Utilizan una aplicación para conectarse a nuestra base de datos. */

/* Creamos el rol de aplicacion */

CREATE APPLICATION ROLE AplicacionAlmacen
	WITH PASSWORD = 'abcd123.',
	DEFAULT_SCHEMA = dbo;
GO

/* Eliminar el rol */

DROP APPLICATION ROLE Almacen ;
GO

/* Conceder permisos al rol "SOBRE UNA VISTA: v" */

GRANT SELECT ON Sales.vSalesPersonSalesByFiscalYears TO AplicacionAlmacen
GO

/* Hago una consulta y funciona*/ 

SELECT USER ;
SELECT USER_NAME();

/* IMPERSONAR */
/* Activamos el rol de aplicación AplicacionAlmacen IMPERSONACIÓN */

EXEC sp_setapprole 'AplicacionAlmacen', 'abcd123.' ;
GO

PRINT USER_NAME()

/* Consulto la vista a la que le di permisos select y una sobre la cual no tengo permisos */

SELECT COUNT(*)
	FROM Sales.vSalesPersonSalesByFiscalYears ;
GO

SELECT COUNT(*)
	FROM HumanResources.vJobCandidate ;
GO

/* Salgo del rol: Si nos impersonamos, ya no podemos volver atrás con REVERT, debemos cerrar la sesión. */

PRINT USER_NAME()
GO

REVERT
/* ERROR */
 
/* Modificar el nombre del rol */

ALTER APPLICATION ROLE AplicacionAlmacen
	WITH NAME = Almacen,
	PASSWORD = 'abcd123.' ;
GO

/* Permisos específicos */
/* FALTA PEGAR ESTA PARTE QUE NO LA HEMOS ACABADO */