-- ATTACH - DETACH

-- Lanzo Script para crear Pubs le llamo Editoriales

-- Transact-SQL

-- -- Detach Database
USE [master]
GO
EXEC MASTER.dbo.sp_detach_db @dbname = 'editoriales'
GO
-- Command(s) completed successfully.
-- No aparece editoriales el Explorador de Objetos

-- Correct Way to Attach Database
USE [master]
GO
-- C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA
CREATE DATABASE [Editoriales] ON
( FILENAME = 'C:\Data\editoriales.mdf'),
( FILENAME = 'C:\Data\editoriales.ldf')
FOR ATTACH
GO

--Msg 5120, Level 16, State 101, Line 19
--Unable to open the physical file "C:\Data\editoriales.ldf". Operating system error 2: "2(The system cannot find the file specified.)".
--Msg 1802, Level 16, State 7, Line 19
--CREATE DATABASE failed. Some file names listed could not be created. Check related errors.


-- Cambio la Autentificación a mixta y me paso a sa (ver Link)


USE [master]
GO
-- C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA
CREATE DATABASE [Editoriales] ON
( FILENAME = 'C:\Data\editoriales.mdf'),
( FILENAME = 'C:\Data\editoriales.ldf')
FOR ATTACH
GO

--Msg 5120, Level 16, State 101, Line 4
--Unable to open the physical file "C:\Data\editoriales.ldf". Operating system error 2: "2(The system cannot find the file specified.)".
--Msg 1802, Level 16, State 7, Line 4
--CREATE DATABASE failed. Some file names listed could not be created. Check related errors.

CREATE DATABASE [editoriales] ON
( FILENAME = 'C:\Data\editoriales.mdf')
FOR ATTACH_REBUILD_LOG
GO

--Msg 5120, Level 16, State 101, Line 15
--Unable to open the physical file "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\editoriales_log.ldf". Operating system error 5: "5(Access is denied.)".
--File activation failure. The physical file name "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\editoriales_log.ldf" may be incorrect.
--New log file 'C:\Data\editoriales_log.ldf' was created.

-- Pese a este mensaje hace el ATTACH. Editoriales se ve en el Explorador de objetos

DROP DATABASE editoriales
GO

-- Command(s) completed successfully.

-- Mirando la Carpeta Data esta vacia . Borro los archivos fisicos