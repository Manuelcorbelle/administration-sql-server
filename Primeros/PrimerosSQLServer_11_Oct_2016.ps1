﻿# $PSVersionTable
Get-ExecutionPolicy
Set-ExecutionPolicy Unrestricted

# Ejemplo Función
Function Obtenet-UsuarioActual { 
[System.Security.Principal.WindowsIdentity]::
GetCurrent().Name }
 
# Ejecutando Función
Obtenet-UsuarioActual


<#PSProvider and PSDrive: 
PowerShell allows different data stores to be accessed as if
they are regular files and folders.
 PSProvider is similar to an adapter, which allows
these data stores to be seen as drives.
#>
PSProvider
PSDrive
Set-Location -Path C:\Windows
cd \
cd C:\Users\manuel

<#
New-Item cmdlet is generic—it doesnt know you want 
to create a folder. It can create folders, files, registry keys, 
and much more, but you have to tell it what type of item you want 
to create .
Posibles: File y directory
#>

new-item NuevaCarpeta
get-help  new-item -Examples
# Fichero testfile1.txt
new-item -path . -name testfile1.txt -itemtype "file" -value "This 
    is a text string."
# Directorio logfiles
new-item -path c:\ -name logfiles -itemtype directory
<#  This command creates a directory named Logfiles in the C: drive. The 
    ItemType parameter specifies that the new item is a directory, not a file 
    or other file system object.
    #>
get-help  *item*| Out-GridView

remove-item NuevaCarpeta
# 
cd hkcu:
dir
cd software
#dir | gm
#gm is get members
get-alias gc
# gc -> Get-Content  
gc env:computername

Set-Location C: 
Set-Location Documents 
Set-Location C:\usr\tmp 
chdir C:\usr\tmp # alias for Set-Location 
cd C:\usr\tmp # alias for Set-Location 
sl C:\usr\tmp # alias for Set-Location


# PowerShell y SQL Server

Psdrive
# Get-Module Confirmar que el Modulo esta cargado
get-command -module sqlps

# El modulo sqlps se encuentra en la carpeta Tools\powershell de la instancia de SQL Server
Import-Module SQLPS -DisableNameChecking
Psdrive
# Preguntando por el Proveedor SQL Server

help sqlserver |out-file c:\sqlserver.txt
notepad c:\sqlserver.txt

cd sqlserver:

ls|out-file c:\sqlserverElementos.csv
notepad c:\sqlserverElementos.csv

cd sql
ls     
#da Nombre equipo WIN-BLK8Q52U6UP
cd WIN-BLK8Q52U6UP
# cd DEFAULT
ls
cd \
ls
cd default
# PS SQLSERVER:\sql\WIN-BLK8Q52U6UP\default>
ls | Out-GridView
cd databases
ls | Out-GridView
dir databases
dir databases | sort Size | select Name,CreateDate,Status,RecoveryModel,Size,Owner |out-gridview
cd AdventureWorks2014
ls | Out-GridView
cd Tables
ls | Out-GridView


Get-ChildItem sqlserver:\sql | Format-List -Force *

Get-Process | 
` Where-Object ProcessName -EQ 'sqlservr'

# script out all foreign keys in T-SQL
dir sqlserver:\sql\WIN-BLK8Q52U6UP\default\databases\AdventureWorks2014\tables | % {$_.foreignkeys } |  % {$_.script()};

# if we need to save the script to a file, we just add out-file at the end of the code
dir sqlserver:\sql\WIN-BLK8Q52U6UP\default\databases\AdventureWorks2014\tables | % {$_.foreignkeys } |  % {$_.script()} | out-file "c:\temp\fk.sql" -force;


#script out all stored procedures
dir sqlserver:\sql\WIN-BLK8Q52U6UP\default\databases\AdventureWorks2014\StoredProcedures | % {$_.script()+'go'};

#script out views with prefix as 'vEmployee'
dir sqlserver:\sql\WIN-BLK8Q52U6UP\default\databases\AdventureWorks2014\Views | ? {$_.name -like 'vEmployee*' } | % {$_.script()+'go'};

#script out all DDL triggers
dir sqlserver:\sql\WIN-BLK8Q52U6UP\default\databases\AdventureWorks2014\Triggers | % {$_.script()+'go'};

#script out UDFs
dir sqlserver:\sql\WIN-BLK8Q52U6UP\default\databases\AdventureWorks2014\UserDefinedFunctions | % {$_.script()+'go'};

#script out SQL Server Agent Jobs whose name is 'ps test' and save it to a file at c:\temp\job.sql, if the file exist, just append the script to it
dir sqlserver:\sql\WIN-BLK8Q52U6UP\default\jobserver\jobs | ? {$_.name -eq 'ps test'}| % {$_.script()+'go'} | out-file c:\temp\job.sql -append;

# Librerias Conexion Bases de Datos
#               SMO (SQL Server Management Object)
#     Para que funcione correctamente instalar SQL Server 2014 features Pack
#               ADO.NET

help invoke-sqlcmd -ShowWindow
invoke-sqlcmd -query "select @@version"
Invoke-Sqlcmd -Query "SELECT GETDATE() AS TimeOfQuery;" -ServerInstance "WIN-BLK8Q52U6UP"
Invoke-Sqlcmd -Query "SELECT * FROM HumanResources.Department;" –Database “AdventureWorks2014” | Out-GridView
# Probar Filtro GridView

$server = New-Object ('Microsoft.SqlServer.Management.SMO.Server') "WIN-BLK8Q52U6UP" 
$server.Databases | 
` Select-Object Name, AutoShrink, LastBackupDate, PageVerify, RecoveryModel |
 ` Format-Table –AutoSize
  


 Get-ChildItem SQLSERVER:\SQL\LocalHost |
 ` Select-Object InstanceName, ServiceAccount, Platform, Version, ProductLevel, Edition |
 ` Format-Table -AutoSize

 
# Ver Script en documento texto
# Crear Script con las siguientes sentencias
#replace WIN-BLK8Q52U6UP with your instance name.
$instanceName = "WIN-BLK8Q52U6UP"  
$managedComputer = New-Object 'Microsoft.SqlServer.Management.Smo.Wmi.ManagedComputer' $instanceName
#list services        
$managedComputer.Services | 
Select Name, ServiceAccount, DisplayName, ServiceState | Out-GridView
# Fin de Script



#VARIABLES

$services = get-service
$services | where-object {$_.status –eq 'running'}


