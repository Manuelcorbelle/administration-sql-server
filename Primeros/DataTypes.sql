--En el ejemplo siguiente se crea una tabla con los tipos de datos bigint, 
-- int, smallint y tinyint. 
-- Se insertan valores en cada columna y se devuelven en la instrucción SELECT.


USE tempdb
GO
CREATE TABLE MyTable
(
  MyBigIntColumn bigint
 ,MyIntColumn  int
 ,MySmallIntColumn smallint
 ,MyTinyIntColumn tinyint
);

GO

INSERT INTO dbo.MyTable 
VALUES (9223372036854775807, 2147483647,32767,255);
 GO
SELECT MyBigIntColumn, MyIntColumn, MySmallIntColumn, MyTinyIntColumn
FROM dbo.MyTable;
GO
--Try Out
INSERT INTO dbo.MyTable 
VALUES (9223372036854775807, 2147483647,32767,256);
 GO

--Msg 220, Level 16, State 2, Line 25
--Arithmetic overflow error for data type tinyint, value = 256.
--The statement has been terminated.


-- En el ejemplo siguiente se crea una tabla con los tipos de datos decimal y numeric. 
-- Se insertan valores en cada columna y los resultados se devuelven con una instrucción 
-- SELECT.
-- Precision, Scale, and Length 

CREATE TABLE dbo.MiTabla  
(  
  MyDecimalColumn decimal(5,2)  
 ,MyNumericColumn numeric(10,5)  
  
);  
 
GO  
INSERT INTO dbo.MiTabla VALUES (123, 12345.12);  
GO  
SELECT MyDecimalColumn, MyNumericColumn  
FROM dbo.MiTabla  
 
 -- 
 -- Precision            , Scale,
 MyDecimalColumn		MyNumericColumn
  123.00	              12345.12000


En el ejemplo siguiente se convierten valores smallmoney y money a tipos de datos varchar y decimal, respectivamente.
DECLARE @mymoney_sm smallmoney = 3148.29,  
        @mymoney    money = 3148.29;  
SELECT  CAST(@mymoney_sm AS varchar) AS 'SM_MONEY varchar',  
        CAST(@mymoney AS decimal)    AS 'MONEY DECIMAL';  




-- UniqueIdentifier

-- En el ejemplo siguiente se convierte un valor uniqueidentifier a un tipo de datos char.

-- @myid Local variable

DECLARE @myid uniqueidentifier = NEWID();
SELECT CONVERT(char(255), @myid) AS 'UniqueIdentifier como Char';
GO

-- Timestamp DEPRECATED Synonym RowVersion
-- RowVersion

CREATE TABLE MyTest (myKey int PRIMARY KEY
    ,myValue int, RV rowversion);
GO 
INSERT INTO MyTest (myKey, myValue) VALUES (1, 0);
GO 
INSERT INTO MyTest (myKey, myValue) VALUES (2, 0);
GO
Select * from MyTest
GO

--myKey	myValue	RV
--1	0	0x00000000000007D1
--2	0	0x00000000000007D2


--Both cast and covert serves the same purpose i.e. convert a data type to another.

--Cast
--Cast is  ANSII Standard
--Cast cannot be used for Formatting Purposes.
--Cast cannot convert a datetime to specific format
--Convert
--Convert is Specific to SQL SERVER
--Convert can be used for Formatting Purposes.For example Select convert (varchar, datetime, 101)
--Convert can be used to convert a datetime to specific format


-- CAST


SELECT CAST(14.85 AS int);
-- Result: 14          (result is truncated)

SELECT CAST(14.85 AS float);
Result: 14.85       (result is not truncated)

SELECT CAST(15.6 AS varchar);
Result: '15.6'

SELECT CAST(15.6 AS varchar(4));
Result: '15.6'

SELECT CAST('15.6' AS float);
Result: 15.6

SELECT CAST('2014-05-02' AS datetime);
Result: '2014-05-02 00:00:00.000'

-- CONVERT


SELECT CONVERT(int, 14.85);
Result: 14          (result is truncated)

SELECT CONVERT(float, 14.85);
Result: 14.85       (result is not truncated)

SELECT CONVERT(varchar, 15.6);
Result: '15.6'

SELECT CONVERT(varchar(4), 15.6);
Result: '15.6'

SELECT CONVERT(float, '15.6');
Result: 15.6

SELECT CONVERT(datetime, '2014-05-02');
Result: '2014-05-02 00:00:00.000'

SELECT CONVERT(varchar, '05/02/2014', 101);
Result: '05/02/2014'



-- Ejemplo:
DECLARE @fecha AS datetime;
SELECT @fecha= GETDATE();
PRINT @fecha;  --              Nov 25 2016  5:10PM
PRINT CONVERT (CHAR(8),@fecha,3) -- 25/11/16
PRINT CONVERT (CHAR(10),@fecha,103) -- 25/11/2016
  

DECLARE @myval decimal (5, 2);  
SET @myval = 193.57;  
SELECT CAST(CAST(@myval AS varbinary(20)) AS decimal(10,5));  
-- Or, using CONVERT  
SELECT CONVERT(decimal(10,5), CONVERT(varbinary(20), @myval));  


SELECT   
   GETDATE() AS UnconvertedDateTime,  
   CAST(GETDATE() AS nvarchar(30)) AS UsingCast,  
   CONVERT(nvarchar(30), GETDATE(), 126) AS UsingConvertTo_ISO8601  ;  
GO

--UnconvertedDateTime	UsingCast	UsingConvertTo_ISO8601
--2016-11-25 17:11:42.553	Nov 25 2016  5:11PM	2016-11-25T17:11:42.553  

--------

--------

DECLARE @myVariable AS varchar(40);  
SET @myVariable = 'This string is longer than thirty characters';  
SELECT CAST(@myVariable AS varchar);  
SELECT DATALENGTH(CAST(@myVariable AS varchar)) AS 'VarcharDefaultLength';  
SELECT CONVERT(char, @myVariable);  
SELECT DATALENGTH(CONVERT(char, @myVariable)) AS 'VarcharDefaultLength';  

-----------------
USE AdventureWorks2014;  
GO  
SELECT  BusinessEntityID,   
   SalesYTD,   
   CONVERT (varchar(12),SalesYTD,1) AS MoneyDisplayStyle1,   
   GETDATE() AS CurrentDate,   
   CONVERT(varchar(12), GETDATE(), 3) AS DateDisplayStyle3  
FROM Sales.SalesPerson  
WHERE CAST(SalesYTD AS varchar(20) ) LIKE '1%';  
GO

