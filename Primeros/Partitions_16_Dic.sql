

-- PARTITIONS
-- Operations SPLIT-MERGE-SWITCH


--Create a database with multiple files and filegroups
--Create a Partition Function and a Partition Scheme based on date
--Create a Table on the Partition
--Insert Data into the Table
--Investigate how the data is stored according to partition
 
USE master
IF EXISTS(select name 
	from sys.databases where name='PartitionTest')
DROP DATABASE PartitionTest
GO
 
 
CREATE DATABASE [PartitionTest]
 ON  PRIMARY
( NAME = N'PartitionTest', FILENAME = N'c:\data\PartitionTest.mdf' , SIZE = 4096KB , FILEGROWTH = 1024KB ), 
FILEGROUP [FGARCHIVE] 
( NAME = N'FGARCHIVE', FILENAME = N'c:\data\FGARCHIVE.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
 FILEGROUP [FG2013] 
( NAME = N'FG2013', FILENAME = N'c:\data\FG2013.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
FILEGROUP [FG2014] 
( NAME = N'FG2014', FILENAME = N'c:\data\FG2014.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB ),
FILEGROUP [FG2015] 
( NAME = N'FG2015', FILENAME = N'c:\data\FG2015.ndf' , SIZE = 4096KB , FILEGROWTH = 1024KB )
 LOG ON
( NAME = N'PartitionTest_log', FILENAME = N'c:\data\PartitionTest_log.ldf' , SIZE = 1024KB , FILEGROWTH = 10%)
GO
 

USE PartitionTest
 GO
CREATE PARTITION FUNCTION partFn_Date (datetime)
AS RANGE RIGHT FOR VALUES ('2013-01-01','2014-01-01')
GO
 
CREATE PARTITION SCHEME part_Date
AS PARTITION partFn_Date --nombre de la funcion
TO (FGARCHIVE,FG2013,FG2014,FG2015)
GO

-- Partition scheme 'part_Date' has been created successfully. 'FG2015' is marked as the next 
-- used filegroup in partition scheme 'part_Date'.



Create Table Orders
(
OrderID Int Identity (10000,1),
OrderDesc Varchar(50),
OrderDate datetime,
OrderAmount money
)
ON part_Date (OrderDate)
GO
 
---Insert 2012 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2012-03-01',RAND()*100)
 
---Insert 2013 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2013-03-01',RAND()*100)
 
---Insert 2014 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2014-03-01',RAND()*100)
 
---Insert 2015 Orders
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-01-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-02-01',RAND()*100)
INSERT INTO Orders Values ('Order ' + Cast(SCOPE_IDENTITY() as varchar(10)),'2015-03-01',RAND()*100)
GO
---Find rows and corresponding partitions
SELECT *,$Partition.partFn_Date(ORDERDate)
FROM Orders
GO


OrderID	OrderDesc	OrderDate	OrderAmount	(No column name)
10000	NULL		2012-01-01 00:00:00.000	23,5079	1
10001	Order 10000	2012-02-01 00:00:00.000	26,5081	1
10002	Order 10001	2012-03-01 00:00:00.000	62,9929	1

10003	Order 10002	2013-01-01 00:00:00.000	91,9042	2
10004	Order 10003	2013-02-01 00:00:00.000	68,5466	2
10005	Order 10004	2013-03-01 00:00:00.000	90,4121	2

10006	Order 10005	2014-01-01 00:00:00.000	76,7857	3
10007	Order 10006	2014-02-01 00:00:00.000	5,5746	3
10008	Order 10007	2014-03-01 00:00:00.000	37,9908	3

10009	Order 10008	2015-01-01 00:00:00.000	26,4933	3
10010	Order 10009	2015-02-01 00:00:00.000	42,3028	3
10011	Order 10010	2015-03-01 00:00:00.000	2,7821	3

DECLARE @TableName NVARCHAR(200) = '[dbo].[Orders]'
SELECT SCHEMA_NAME(o.schema_id) + '.' + OBJECT_NAME(i.object_id) AS [object]
     , p.partition_number AS [p#]
     , fg.name AS [filegroup]
     , p.rows
     , au.total_pages AS pages
     , CASE boundary_value_on_right
       WHEN 1 THEN 'less than'
       ELSE 'less than or equal to' END as comparison
     , rv.value
     , CONVERT (VARCHAR(6), CONVERT (INT, SUBSTRING (au.first_page, 6, 1) +
       SUBSTRING (au.first_page, 5, 1))) + ':' + CONVERT (VARCHAR(20),
       CONVERT (INT, SUBSTRING (au.first_page, 4, 1) +
       SUBSTRING (au.first_page, 3, 1) + SUBSTRING (au.first_page, 2, 1) +
       SUBSTRING (au.first_page, 1, 1))) AS first_page
FROM sys.partitions p
INNER JOIN sys.indexes i
     ON p.object_id = i.object_id
AND p.index_id = i.index_id
INNER JOIN sys.objects o
     ON p.object_id = o.object_id
INNER JOIN sys.system_internals_allocation_units au
     ON p.partition_id = au.container_id
INNER JOIN sys.partition_schemes ps
     ON ps.data_space_id = i.data_space_id
INNER JOIN sys.partition_functions f
     ON f.function_id = ps.function_id
INNER JOIN sys.destination_data_spaces dds
     ON dds.partition_scheme_id = ps.data_space_id
     AND dds.destination_id = p.partition_number
INNER JOIN sys.filegroups fg
     ON dds.data_space_id = fg.data_space_id
LEFT OUTER JOIN sys.partition_range_values rv
     ON f.function_id = rv.function_id
     AND p.partition_number = rv.boundary_id
WHERE i.index_id < 2
     AND o.object_id = OBJECT_ID(@TableName);
GO

--object	p#	filegroup		rows	pages	comparison	value	first_page
--dbo.Orders	1	FGARCHIVE	3		2	less than	2013-01-01 00:00:00.000	3:8
--dbo.Orders	2	FG2013		3		2	less than	2014-01-01 00:00:00.000	4:8
--dbo.Orders	3	FG2014		6		2	less than	NULL	5:8

---find ranges for partitions
select *
from sys.partition_functions f
inner join sys.partition_range_values rv on f.function_id=rv.function_id
where f.name = 'partFn_Date'
GO
----Find number of rows per partition
select p.*
from sys.partitions p
inner join sys.tables t on p.object_id=t.object_id
and t.name = 'orders'
GO 

--partition_id	object_id	index_id	partition_number	hobt_id	rows	filestream_filegroup_id	data_compression	data_compression_desc
--72057594040549376	245575913	0	1	72057594040549376	3	0	0	NONE
--72057594040614912	245575913	0	2	72057594040614912	3	0	0	NONE
--72057594040680448	245575913	0	3	72057594040680448	6	0	0	NONE


----other useful system views with paritioning info
 
select *
from sys.partition_schemes
GO

--------------------------
-- SPLIT - MERGE - SWITCH

--Creating a new Range in our Partition for new data (SPLIT RANGE)
--Merging an older Range of data for archiving (MERGE RANGE)
--Reviewing detailed metadata on partitions
--Removing old Filegroups and Data files no longer needed after archiving
--Using the SWITCH PARTITION command to move data between partitions (Archiving, Moving data from Staging to 
--Production tables in DW environment)



--
--Let us put the 2015 data into a new partition
Use PartitionTest
GO 
-- SPLIT

ALTER PARTITION FUNCTION partFn_Date()
SPLIT RANGE ('2015-01-01');
GO

SELECT *,$Partition.partFn_Date(ORDERDate)
FROM Orders
GO

DECLARE @TableName NVARCHAR(200) = '[dbo].[Orders]'
SELECT SCHEMA_NAME(o.schema_id) + '.' + OBJECT_NAME(i.object_id) AS [object]
     , p.partition_number AS [p#]
     , fg.name AS [filegroup]
     , p.rows
     , au.total_pages AS pages
     , CASE boundary_value_on_right
       WHEN 1 THEN 'less than'
       ELSE 'less than or equal to' END as comparison
     , rv.value
     , CONVERT (VARCHAR(6), CONVERT (INT, SUBSTRING (au.first_page, 6, 1) +
       SUBSTRING (au.first_page, 5, 1))) + ':' + CONVERT (VARCHAR(20),
       CONVERT (INT, SUBSTRING (au.first_page, 4, 1) +
       SUBSTRING (au.first_page, 3, 1) + SUBSTRING (au.first_page, 2, 1) +
       SUBSTRING (au.first_page, 1, 1))) AS first_page
FROM sys.partitions p
INNER JOIN sys.indexes i
     ON p.object_id = i.object_id
AND p.index_id = i.index_id
INNER JOIN sys.objects o
     ON p.object_id = o.object_id
INNER JOIN sys.system_internals_allocation_units au
     ON p.partition_id = au.container_id
INNER JOIN sys.partition_schemes ps
     ON ps.data_space_id = i.data_space_id
INNER JOIN sys.partition_functions f
     ON f.function_id = ps.function_id
INNER JOIN sys.destination_data_spaces dds
     ON dds.partition_scheme_id = ps.data_space_id
     AND dds.destination_id = p.partition_number
INNER JOIN sys.filegroups fg
     ON dds.data_space_id = fg.data_space_id
LEFT OUTER JOIN sys.partition_range_values rv
     ON f.function_id = rv.function_id
     AND p.partition_number = rv.boundary_id
WHERE i.index_id < 2
     AND o.object_id = OBJECT_ID(@TableName);
GO

--object		p#	filegroup	rows	pages	comparison	value	first_page
--dbo.Orders	1	FGARCHIVE	3		2	less than	2013-01-01 00:00:00.000	3:8
--dbo.Orders	2	FG2013		3		2	less than	2014-01-01 00:00:00.000	4:8
--dbo.Orders	3	FG2014		3		2	less than	2015-01-01 00:00:00.000	5:8
--dbo.Orders	4	FG2015		3		2	less than	NULL	6:8

-- MERGE
--Remove 2013 Partition using a Merge
ALTER PARTITION FUNCTION partFn_Date ()
MERGE RANGE ('2013-01-01');
GO

DECLARE @TableName NVARCHAR(200) = '[dbo].[Orders]'
SELECT SCHEMA_NAME(o.schema_id) + '.' + OBJECT_NAME(i.object_id) AS [object]
     , p.partition_number AS [p#]
     , fg.name AS [filegroup]
     , p.rows
     , au.total_pages AS pages
     , CASE boundary_value_on_right
       WHEN 1 THEN 'less than'
       ELSE 'less than or equal to' END as comparison
     , rv.value
     , CONVERT (VARCHAR(6), CONVERT (INT, SUBSTRING (au.first_page, 6, 1) +
       SUBSTRING (au.first_page, 5, 1))) + ':' + CONVERT (VARCHAR(20),
       CONVERT (INT, SUBSTRING (au.first_page, 4, 1) +
       SUBSTRING (au.first_page, 3, 1) + SUBSTRING (au.first_page, 2, 1) +
       SUBSTRING (au.first_page, 1, 1))) AS first_page
FROM sys.partitions p
INNER JOIN sys.indexes i
     ON p.object_id = i.object_id
AND p.index_id = i.index_id
INNER JOIN sys.objects o
     ON p.object_id = o.object_id
INNER JOIN sys.system_internals_allocation_units au
     ON p.partition_id = au.container_id
INNER JOIN sys.partition_schemes ps
     ON ps.data_space_id = i.data_space_id
INNER JOIN sys.partition_functions f
     ON f.function_id = ps.function_id
INNER JOIN sys.destination_data_spaces dds
     ON dds.partition_scheme_id = ps.data_space_id
     AND dds.destination_id = p.partition_number
INNER JOIN sys.filegroups fg
     ON dds.data_space_id = fg.data_space_id
LEFT OUTER JOIN sys.partition_range_values rv
     ON f.function_id = rv.function_id
     AND p.partition_number = rv.boundary_id
WHERE i.index_id < 2
     AND o.object_id = OBJECT_ID(@TableName);
GO


--object		p#	filegroup	rows	pages	comparison	value	first_page
--dbo.Orders	1	FGARCHIVE	6		2	less than	2014-01-01 00:00:00.000	3:8
--dbo.Orders	2	FG2014		3		2	less than	2015-01-01 00:00:00.000	5:8
--dbo.Orders	3	FG2015		3		2	less than	NULL	6:8
--
--Query to determine table filegroup by index and partition
--Script provided by Jason Strate
--http://www.jasonstrate.com/2013/01/determining-file-group-for-a-table/
--
USE PartitionTest
Go
SELECT OBJECT_SCHEMA_NAME(t.object_id) AS schema_name
,t.name AS table_name
,i.index_id
,i.name AS index_name
,p.partition_number
,fg.name AS filegroup_name
,FORMAT(p.rows, '#,###') AS rows
FROM sys.tables t
INNER JOIN sys.indexes i ON t.object_id = i.object_id
INNER JOIN sys.partitions p ON i.object_id=p.object_id AND i.index_id=p.index_id
LEFT OUTER JOIN sys.partition_schemes ps ON i.data_space_id=ps.data_space_id
LEFT OUTER JOIN sys.destination_data_spaces dds ON ps.data_space_id=dds.partition_scheme_id AND 
p.partition_number=dds.destination_id
INNER JOIN sys.filegroups fg ON COALESCE(dds.data_space_id, i.data_space_id)=fg.data_space_id
 
---Remove the file and filegroup you no longer need
USE master
GO

--schema_name	table_name	index_id	index_name	partition_number	filegroup_name	rows
--dbo	Orders	0	NULL	1											FGARCHIVE		6
--dbo	Orders	0	NULL	2											FG2014			3
--dbo	Orders	0	NULL	3											FG2015			3	

ALTER DATABASE PartitionTest
REMOVE FILE FG2013
GO
 
-- The file 'FG2013' has been removed.

USE master
GO
ALTER DATABASE PartitionTest
REMOVE FILEGROUP FG2013
GO

-- The filegroup 'FG2013' has been removed.

-- SWITCH
USE PartitionTest
--Switch Data into another table then truncate
Create Table OrdersArchive
(
OrderID Int NOT NULL,
OrderDesc Varchar(50),
OrderDate datetime,
OrderAmount money
)
ON FGArchive
GO
 
ALTER TABLE Orders 
SWITCH Partition 1 to OrdersArchive
GO

SELECT *
FROM OrdersArchive
GO
SELECT *
FROM Orders
GO

DECLARE @TableName NVARCHAR(200) = '[dbo].[Orders]'
SELECT SCHEMA_NAME(o.schema_id) + '.' + OBJECT_NAME(i.object_id) AS [object]
     , p.partition_number AS [p#]
     , fg.name AS [filegroup]
     , p.rows
     , au.total_pages AS pages
     , CASE boundary_value_on_right
       WHEN 1 THEN 'less than'
       ELSE 'less than or equal to' END as comparison
     , rv.value
     , CONVERT (VARCHAR(6), CONVERT (INT, SUBSTRING (au.first_page, 6, 1) +
       SUBSTRING (au.first_page, 5, 1))) + ':' + CONVERT (VARCHAR(20),
       CONVERT (INT, SUBSTRING (au.first_page, 4, 1) +
       SUBSTRING (au.first_page, 3, 1) + SUBSTRING (au.first_page, 2, 1) +
       SUBSTRING (au.first_page, 1, 1))) AS first_page
FROM sys.partitions p
INNER JOIN sys.indexes i
     ON p.object_id = i.object_id
AND p.index_id = i.index_id
INNER JOIN sys.objects o
     ON p.object_id = o.object_id
INNER JOIN sys.system_internals_allocation_units au
     ON p.partition_id = au.container_id
INNER JOIN sys.partition_schemes ps
     ON ps.data_space_id = i.data_space_id
INNER JOIN sys.partition_functions f
     ON f.function_id = ps.function_id
INNER JOIN sys.destination_data_spaces dds
     ON dds.partition_scheme_id = ps.data_space_id
     AND dds.destination_id = p.partition_number
INNER JOIN sys.filegroups fg
     ON dds.data_space_id = fg.data_space_id
LEFT OUTER JOIN sys.partition_range_values rv
     ON f.function_id = rv.function_id
     AND p.partition_number = rv.boundary_id
WHERE i.index_id < 2
     AND o.object_id = OBJECT_ID(@TableName);
GO